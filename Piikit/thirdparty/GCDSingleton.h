//
//  GCDSingleton.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject; \

/*
 static dispatch_once_t pred = 0;
 __strong static id _sharedObject = nil;
 dispatch_once(&pred, ^{
 _sharedObject = [[self alloc] init]; // or some other init method
 });
 return _sharedObject;
 */

