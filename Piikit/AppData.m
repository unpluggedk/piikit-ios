//
//  AppData.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "AppData.h"
#import "GCDSingleton.h"

@implementation AppData

static sqlite3 *databaseHandle;

+(id)sharedAppData{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
    
    /*
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
    */
}

- (id)init{
    NSLog(@"init shared app data");
    [self initDatabase];
    _themeColor = [UIColor colorWithRed:238./256 green:46./256 blue:36./256 alpha:1.0];
    return self;
}

- (void)dealloc{
    sqlite3_close(databaseHandle);
}


#pragma mark - DATABASE

-(void)initDatabase
{
    // Create a string containing the full path to the sqlite.db inside the documents folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"sqlite.db"];
    
    // Check to see if the database file already exists
    bool databaseAlreadyExists = [[NSFileManager defaultManager] fileExistsAtPath:databasePath];
    
    // Open the database and store the handle as a data member
    if (sqlite3_open([databasePath UTF8String], &databaseHandle) == SQLITE_OK)
    {
        // Create the database if it doesn't yet exists in the file system
        if (!databaseAlreadyExists)
        {
            const char *sqlStatement;
            char *error;
            // Create the ENTRY table
            sqlStatement = "CREATE TABLE IF NOT EXISTS CATEGORIES (ID INTEGER PRIMARY KEY, CATEGORY TEXT, SELECTED INT)";
            if (sqlite3_exec(databaseHandle, sqlStatement, NULL, NULL, &error) != SQLITE_OK)
            {
                NSLog(@"Error: %s", error);
            }
        }
    }
}



-(BOOL)isIos7{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
            return YES;
    return NO;

    
    
}

@end
