//
//  Enums.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 27..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

// HomeViewCell.h
typedef enum{
    LEFT,
    RIGHT
} scoreSide;

// PostingItem.h
typedef NS_ENUM(NSInteger, VoteSide){
    NotVoted,
    VoteLeft,
    VoteRight,
    UnvoteLeft,
    UnvoteRight
};


