//
//  HomeViewCell_v2.h
//  PIIKIT
//
//  Created by Jason Kim on 13. 11. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Enums.h"

@interface HomeViewCell_v2 : UITableViewCell{
    UIView *titleView;
    UILabel *title;
    UILabel *username;
    
    UIImageView *profile;
    
    UILabel *date;
    
    UIImageView *leftImageView;
    UIImageView *rightImageView;
    
    UIView *footerView;
    UILabel *footer;
    
    UIImageView *scorebarView;
    UILabel *votebarScore;
    
    UIView *blackDivider;
    
    UIButton *reportBtn;
}


@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *username;

@property (nonatomic, retain) UIButton *reportBtn;
@property (nonatomic, retain) UIImageView *profile;
@property (nonatomic, retain) UILabel *date;

@property (nonatomic, retain) UILabel *votebarScore;

@property (nonatomic, retain) UIImageView *leftImageView;
@property (nonatomic, retain) UIImageView *rightImageView;

+ (CGFloat) cellHeight;

- (void)setTitleString:(NSString*)titleStr;
- (void)setUserNameString:(NSString*)usernameStr;
- (void)setDateStr:(NSString*)dateStr;
- (void)setFooter:(int)votes Comments:(int)comments Shared:(int)shared;

/*
 - (void)setLeftImage:(UIImage*)img;
- (void)setRightImage:(UIImage*)img;
- (void)setSingleImage:(UIImage*)img;
*/

- (void)setSingleImageWithURL:(NSString*)url placeholder:(UIImage*)placeholder;
- (void)setImagesWithUrlLeft:(NSString*)urlLeft right:(NSString*)urlRight placeholder:(UIImage*)placeholder;

- (void)hideVotebar;
- (void)setVotebar:(NSString*)score side:(scoreSide)side;


@end
