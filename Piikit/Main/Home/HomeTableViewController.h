//
//  HomeViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "DetailLoaderProtocol.h"

#import "DetailViewController.h"

#import "Vote.h"

@protocol HomeTableViewControllerDelegate <NSObject>

- (void)displayHUD;
- (void)hideHUD;
- (void)popToMain;

@end



@interface HomeTableViewController : UITableViewController<NetworkDelegate, UIGestureRecognizerDelegate, DetailViewControllerDelegate, UIAlertViewDelegate>{
    NSMutableArray *dataSource_current;
   
    NSMutableDictionary *myVoteDict;
    
    BOOL mEndOfPosting;
    
    NSNumber* lastLoadedCatId;

    
}

@property (nonatomic, retain) NSNumber *lastLoadedCatId;

@property (assign) id<HomeTableViewControllerDelegate> homeTableViewControllerDelegate;
@property (assign) id<DetailLoaderDelegate> detailLoaderDelegate;

- (void) loadPopular;
- (void) loadRecent;
- (void) loadFollowing;

- (void) loadCategoryId:(NSNumber*)catId;

@end
