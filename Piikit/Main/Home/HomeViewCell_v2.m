//
//  HomeViewCell_v2.m
//  PIIKIT
//
//  Created by Jason Kim on 13. 11. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "HomeViewCell_v2.h"
#import <CoreText/CoreText.h>
#import "AppData.h"

#import "UIImageView+WebCache.h"


@implementation HomeViewCell_v2

@synthesize title;
@synthesize username;
@synthesize profile;
@synthesize date;
@synthesize votebarScore;
@synthesize reportBtn;
@synthesize leftImageView, rightImageView;

+ (CGFloat) cellHeight{
    return 29+4+54+174+29;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        CGFloat relativeY = 0;
        
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
        titleView.backgroundColor = [UIColor blackColor];
        title = [[UILabel alloc] initWithFrame:CGRectMake(15, relativeY, 320-15-15, 29)];
        title.backgroundColor = [UIColor blackColor];
        relativeY += 29;
        [titleView addSubview:title];
        [self addSubview:titleView];
        
        relativeY +=4;
        profile = [[UIImageView alloc] initWithFrame:CGRectMake(15, relativeY, 53, 50)];
        [self addSubview:profile];
        
        username = [[UILabel alloc] initWithFrame:CGRectMake(75, relativeY, 320-15-80, 25)];
        [self addSubview:username];
        date = [[UILabel alloc] initWithFrame:CGRectMake(75, relativeY+22, 200, 25)];
        [self addSubview:date];
        
        relativeY += 54;
        
        leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, relativeY, 157, 174)];
        leftImageView.contentMode = UIViewContentModeScaleAspectFill;
        leftImageView.clipsToBounds = YES;
        
        rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(157+6, relativeY, 157, 174)];
        rightImageView.contentMode = UIViewContentModeScaleAspectFill;
        rightImageView.clipsToBounds = YES;
        
        rightImageView.hidden = true;
        
        [self addSubview:rightImageView];
        [self addSubview:leftImageView];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
        line.backgroundColor = [UIColor blackColor];
        [self addSubview:line];
        
        blackDivider = [[UIView alloc] initWithFrame:CGRectMake(157, relativeY, 6, 174)];
        blackDivider.backgroundColor = [UIColor blackColor];
        [self addSubview:blackDivider];
        
        scorebarView = [[UIImageView alloc] initWithFrame:CGRectMake(120, relativeY + 125, 320-120, 20)];
        scorebarView.image = [UIImage imageNamed:@"vote-bar-right.png"];
        
        votebarScore = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 20)];
        votebarScore.font = [UIFont fontWithName:@"BebasNeue" size:13.f];
        votebarScore.backgroundColor = [UIColor clearColor];
        votebarScore.textColor = [UIColor blackColor];
        
        [scorebarView addSubview:votebarScore];
        [self addSubview:scorebarView];
        
        scorebarView.hidden = YES;
        
        relativeY += 174;
        
        footerView = [[UIImageView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
        footerView.backgroundColor = [UIColor blackColor];
        footer = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 275, 29)];
        footer.textAlignment = NSTextAlignmentRight;
        footer.backgroundColor = [UIColor clearColor];
        [footerView addSubview:footer];
        
        [self addSubview:footerView];
        
        reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        reportBtn.frame = CGRectMake(320-18, footerView.frame.origin.y + 6, 15, 15);
        reportBtn.backgroundColor = [UIColor blackColor];
        UIFont *audimat = [UIFont fontWithName:@"AudimatMonoBold" size:13.f];
        NSMutableAttributedString *reportStr = [[NSMutableAttributedString alloc] initWithString:@"!"];
        [reportStr addAttribute:NSFontAttributeName value:audimat range:NSMakeRange(0, reportStr.string.length)];
        [reportBtn setAttributedTitle:reportStr forState:UIControlStateNormal];
        
        reportBtn.titleLabel.textColor = [UIColor redColor];
        reportBtn.layer.borderWidth = 1.f;
        reportBtn.layer.borderColor = [SharedAppData themeColor].CGColor;
        [self addSubview:reportBtn];
        
        
    }
    return self;
}

- (void)hideVotebar{
    scorebarView.hidden = YES;
}

- (void)setVotebar:(NSString*)score side:(scoreSide)side{
    scorebarView.hidden = NO;
    
    if(side == LEFT){
        scorebarView.image = [UIImage imageNamed:@"vote-bar-left.png"];
        scorebarView.frame = CGRectMake(0, 215, 423/2, 20);
        votebarScore.text = score;
        votebarScore.textAlignment = NSTextAlignmentLeft;
        votebarScore.frame = CGRectMake(35, 0, 423/2 - 40, 20);
    }
    else{
        scorebarView.image = [UIImage imageNamed:@"vote-bar-right.png"];
        scorebarView.frame = CGRectMake(320-423/2, 215, 423/2, 20);
        votebarScore.text = score;
        votebarScore.textAlignment = NSTextAlignmentRight;
        votebarScore.frame = CGRectMake(35, 0, 423/2 - 70, 20);
        
    }
    scorebarView.hidden = NO;
    
}

/*
- (void)setLeftImage:(UIImage*)img{
    leftImageView.frame = CGRectMake(0, 87, 157, 174);
    leftImageView.image = img;
    
    blackDivider.hidden = NO;
}

- (void)setRightImage:(UIImage*)img{
    rightImageView.image = img;
}

- (void)setSingleImage:(UIImage*)img{
    leftImageView.frame = CGRectMake(0, 87, 320, 174);
    leftImageView.image = img;
    blackDivider.hidden = YES;
    
}
*/
- (void)setSingleImageWithURL:(NSString*)url placeholder:(UIImage*)placeholder{
    rightImageView.hidden = true;
    leftImageView.frame = CGRectMake(0, 87, 320, 174);
    [leftImageView setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:placeholder];
    blackDivider.hidden = YES;
    
}

- (void)setImagesWithUrlLeft:(NSString*)urlLeft right:(NSString*)urlRight placeholder:(UIImage*)placeholder{
    rightImageView.hidden = false;
    leftImageView.frame = CGRectMake(0, 87, 157, 174);
    [leftImageView setImageWithURL:[NSURL URLWithString:urlLeft]
                  placeholderImage:placeholder];
    [rightImageView setImageWithURL:[NSURL URLWithString:urlRight]
                   placeholderImage:placeholder];
    
    blackDivider.hidden = NO;
}


- (void)setTitleString:(NSString*)titleStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:[titleStr uppercaseString]];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:17.f] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.f] range:NSMakeRange(0, titleStr.length)];
    
    title.attributedText = outstr;
}

- (void)setUserNameString:(NSString*)usernameStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@asks,",[usernameStr uppercaseString]]];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, outstr.string.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:.5f] range:NSMakeRange(0, usernameStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.f] range:NSMakeRange(usernameStr.length-1, 1)];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:15.f] range:NSMakeRange(0, usernameStr.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:10.f] range:NSMakeRange(usernameStr.length, 5)];
    
    username.attributedText = outstr;
}

- (void)setDateStr:(NSString*)dateStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:dateStr];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, dateStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-1.f] range:NSMakeRange(0, dateStr.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:10.f] range:NSMakeRange(0, dateStr.length)];
    
    date.attributedText = outstr;
    
    date.frame = CGRectMake(date.frame.origin.x, date.frame.origin.y, outstr.size.width, outstr.size.height);
    
    
}

- (void)setFooter:(int)votes Comments:(int)comments Shared:(int)shared{
    int votesDigit = [[NSString stringWithFormat:@"%d",votes] length];
    int commentsDigit = [[NSString stringWithFormat:@"%d",comments] length];
    int sharedDigit = [[NSString stringWithFormat:@"%d",shared] length];
    
    bool sharedDisplayed = false;
    NSMutableAttributedString *outstr;
    if(sharedDisplayed){
        outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dVOTES, %dCOMMENTS %dSHARED+MORE", votes, comments, shared]];
        int x = 0;
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, votesDigit)];
        x += votesDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 7)];
        x += 7;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, commentsDigit)];
        x += commentsDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 9)];
        x += 9;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, sharedDigit)];
        x += sharedDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 6)];
        x += 6;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, 5)];
        
        
    }
    else{
        outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dVOTES, %dCOMMENTS +MORE", votes, comments]];
        int x = 0;
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, votesDigit)];
        x += votesDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 7)];
        x += 7;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, commentsDigit)];
        x += commentsDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 9)];
        x += 9;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, 5)];
    }
    
    
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.f] range:NSMakeRange(0,  outstr.string.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:12.f] range:NSMakeRange(0, outstr.string.length)];
    
    footer.attributedText = outstr;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
