//
//  HomeViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 9..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "HomeViewController.h"
#import "AppData.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "IIViewDeckController.h"
#import "NetworkManager.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize tableView;
@synthesize detailLoaderDelegate;

#define POPULAR 101
#define RECENT 102
#define FOLLOWING 103

int mCurrent = POPULAR;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (NSAttributedString *)getTabbarString:(NSString*)str{
    UIFont *tabBarFont = [UIFont fontWithName:@"AudimatMono" size:12.f];
    NSNumber *linespace = [NSNumber numberWithFloat:0.7f];
    
    NSMutableAttributedString *out = [[NSMutableAttributedString alloc] initWithString:str];
    [out addAttribute:NSFontAttributeName value:tabBarFont range:NSMakeRange(0, out.length)];
    [out addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, out.length)];
    [out addAttribute:(NSString*)kCTKernAttributeName value:linespace range:NSMakeRange(0, out.length)];
    
    return out;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    /*
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320,30)];
    tabBar.backgroundColor = [UIColor blackColor];
    tabBar.layer.borderColor = [UIColor whiteColor].CGColor;
    tabBar.layer.borderWidth = 1.f;
    
    
    mPopularLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 107, 30)];
    mPopularLabel.backgroundColor = [UIColor blackColor];
    mPopularLabel.textAlignment = NSTextAlignmentCenter;
    mPopularLabel.attributedText = [self getTabbarString:@"POPULAR"];
    mPopularLabel.tag = POPULAR;
    mPopularLabel.textColor = [UIColor whiteColor];
    mPopularLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mPopularLabel];

    mRecentLabel = [[UILabel alloc] initWithFrame:CGRectMake(107, 0, 107, 30)];
    mRecentLabel.backgroundColor = [UIColor blackColor];
    mRecentLabel.textAlignment = NSTextAlignmentCenter;
    mRecentLabel.attributedText = [self getTabbarString:@"RECENT"];
    mRecentLabel.tag = RECENT;
    mRecentLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mRecentLabel];

    mFollowingLabel = [[UILabel alloc] initWithFrame:CGRectMake(213, 0, 107, 30)];
    mFollowingLabel.backgroundColor = [UIColor blackColor];
    mFollowingLabel.textAlignment = NSTextAlignmentCenter;
    mFollowingLabel.attributedText = [self getTabbarString:@"FOLLOWING"];
    mFollowingLabel.tag = FOLLOWING;
    mFollowingLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mFollowingLabel];

    UIView *line;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(107, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(213, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    
    [self.view addSubview:tabBar];
    */
    
    tableView = [[HomeTableViewController alloc] init];
    tableView.homeTableViewControllerDelegate = self;
    tableView.tableView.dataSource = tableView;
    tableView.tableView.delegate = tableView;
    
    tableView.detailLoaderDelegate = detailLoaderDelegate;
    
    int diffY = 0;
    if([SharedAppData isIos7])
        diffY = 20;

    tableView.tableView.frame = CGRectMake(0, diffY, 320, self.view.frame.size.height - diffY); //20 : status bar    49: tabbar    15: topbar
    [self.view addSubview:tableView.tableView];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Loading";
    HUD.userInteractionEnabled = NO;
    
    [self loadData:RECENT];

}


- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.tabBarController.viewDeckController.panningMode = IIViewDeckFullViewPanning;;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    int tag = [[[touches anyObject] view] tag];
    switch (tag) {
        case POPULAR:
            mPopularLabel.textColor = [UIColor whiteColor];
            mRecentLabel.textColor = [UIColor darkGrayColor];
            mFollowingLabel.textColor = [UIColor darkGrayColor];
            if(mCurrent != POPULAR){
                mCurrent = POPULAR;
                [self loadData:POPULAR];
            }
            break;
        case RECENT:
            mPopularLabel.textColor = [UIColor darkGrayColor];
            mRecentLabel.textColor = [UIColor whiteColor];
            mFollowingLabel.textColor = [UIColor darkGrayColor];
            if(mCurrent != RECENT){
                mCurrent = RECENT;
                [self loadData:RECENT];
            }
            break;
        case FOLLOWING:
            mPopularLabel.textColor = [UIColor darkGrayColor];
            mRecentLabel.textColor = [UIColor darkGrayColor];
            mFollowingLabel.textColor = [UIColor whiteColor];
            if(mCurrent != FOLLOWING){
                mCurrent = FOLLOWING;
                [self loadData:FOLLOWING];
            }
            break;
        default:
            break;
    }
    
}

- (void)loadData:(int)selection{
    // start HUD spinning
    
    // command for fetching
    if(selection == FOLLOWING)
        [tableView loadFollowing];
    else if(selection == RECENT)
        [tableView loadRecent];
    else if(selection == POPULAR)
        [tableView loadPopular];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)displayHUD{

    [HUD show:YES];
}

- (void)hideHUD{
    [HUD hide:YES];

}

-(void)popToMain{
    [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
}

@end
