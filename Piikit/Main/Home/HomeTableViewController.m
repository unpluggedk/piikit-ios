//
//  HomeViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "HomeTableViewController.h"
#import "AppData.h"

#import "HomeTableViewController.h"
#import "PostingItem.h"
//#import "HomeViewCell.h"
#import "HomeViewCell_v2.h"

#import "UIImageView+WebCache.h"

#import "DetailViewController.h"
#import "Profile.h"

@interface HomeTableViewController ()

@end

@implementation HomeTableViewController
@synthesize homeTableViewControllerDelegate;
@synthesize lastLoadedCatId;
@synthesize detailLoaderDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        /*
        PostingItem *item1 = [[PostingItem alloc] initWithID:123
                                                   Username:@"PIIK-LIKE-PSY"
                                                      Title:@"Better gangnam style?"
                                                Description:NULL
                                                  LabelLeft:@"" LabelRight:@""
                                                 ProfileURL:@"profile-1-placeholder.png"
                                                     PicLeft:NULL
                                                   PicRight:NULL
                                                  PicSingle:@"placeholder-left.png"
                                               NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:23
                                                NumComments:3
                                                   NumShared:0
                                            DateCreatedMilli:1234 DateUpdatedMilli:1234];

        
        
        item1.mDateStr = @"05/06/13 3:28pm";

        PostingItem *item2 = [[PostingItem alloc] initWithID:123
                                                    Username:@"PIIK-LIKE-PSY"
                                                       Title:@"Better gangnam style?"
                                                 Description:NULL
                                                   LabelLeft:@"" LabelRight:@""
                                                  ProfileURL:@"profile-1-placeholder.png"
                                                     PicLeft:NULL
                                                    PicRight:NULL
                                                   PicSingle:@"placeholder-left.png"
                                                NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:23
                                                 NumComments:3
                                                   NumShared:0
                                            DateCreatedMilli:1234 DateUpdatedMilli:1234];
        item2.mDateStr = @"05/05/13 9:28pm";
        */
        dataSource_current = [[NSMutableArray alloc] init];
        myVoteDict = [[NSMutableDictionary alloc] init];
             
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    mEndOfPosting = YES;
    
    self.tableView.separatorColor = [UIColor clearColor];
    // Uncomment the following line to preserve selection between presentations.
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to refresh"];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.tableView.contentOffset = CGPointMake(0, 30);
    
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)refreshView:(UIRefreshControl *)refresh {
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    // custom refresh logic would be placed here...
    
//    [SharedNetworkManager fetchNewPostings:Recent sender:self];
    [SharedNetworkManager fetchNewCategory:lastLoadedCatId sender:self];
    [homeTableViewControllerDelegate displayHUD];
      
}

/*
- (void)refresh:(id)sender {
    // Create URL
    NSURL *url = [NSURL URLWithString:@"http://search.twitter.com/search.json?q=ios%20development&rpp=100&include_entities=true&result_type=mixed/"];
    // Initialize URL Request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    // JSON Request Operation
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSArray *results = [(NSDictionary *)JSON objectForKey:@"results"];
        if ([results count]) {
            self.tweets = results;
            // Reload Table View
            [self.tableView reloadData];
            // End Refreshing
            [(UIRefreshControl *)sender endRefreshing];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        // End Refreshing
        [(UIRefreshControl *)sender endRefreshing];
    }];
    // Start Operation
    [operation start];
}

*/

- (void)updateTable
{
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    NSLog(@"NUMBER OF ROWS = %d", [self.tableView numberOfRowsInSection:0]);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!mEndOfPosting && indexPath.row == [dataSource_current count]*2 + 1) {
        NSLog(@"LOAD MORE!! t est");
        [homeTableViewControllerDelegate displayHUD];
        [SharedNetworkManager fetchMorePostings:Recent category:lastLoadedCatId sender:self];

    }
    
}

/*
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView.indexPathsForVisibleRows indexOfObject:indexPath] == NSNotFound)
    {
        // This indeed is an indexPath no longer visible
        // Do something to this non-visible cell...
        //[self.refreshControl endRefreshing];
        
        
    }
    if (!mEndOfPosting && indexPath.row == [dataSource_current count]*2 + 1) {
        NSLog(@"LOAD MORE!! t est");
    }
}
*/
/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"%f",[self.tableView contentOffset].y);
    if(refreshing){
        if([self.tableView contentOffset].y >= 0){
            refreshing = NO;
            [self.refreshControl endRefreshing];
            
        }
    }
}
*/

/*
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    NSIndexPath *firstVisibleIndexPath = [[self.tableView indexPathsForVisibleRows] objectAtIndex:0];
    //if(firstVisibleIndexPath.row != 0)
    //   [self.refreshControl endRefreshing];
    NSLog(@"first visible cell's section: %i, row: %i", firstVisibleIndexPath.section, firstVisibleIndexPath.row);
    NSLog(@"count = %d", [dataSource_current count] * 2 - 2);

    int lastRow = mEndOfPosting?[dataSource_current count]*2 + 1:[dataSource_current count]*2 + 2;
    
//    if(!mEndOfPosting && firstVisibleIndexPath.row >= lastRow - 1){
    if(!mEndOfPosting && firstVisibleIndexPath.row >= [dataSource_current count] * 2 - 2){
        [homeTableViewControllerDelegate displayHUD];
                [SharedNetworkManager fetchMorePostings:Recent category:lastLoadedCatId sender:self];
    }

    
}
*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

typedef enum{
    EMPTYCELL = 0,
    POSTCELL = 1,
    MORECELL = 2
} CellType;


- (CellType) getIdentifier:(int)row{
    int range = [dataSource_current count] * 2;
    
    if(row < range){
        if(row %2 == 0)
            return EMPTYCELL;
        else
            return POSTCELL;
    }
    else if(row == range){
        return EMPTYCELL;
    }
    return MORECELL;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(mEndOfPosting)
         return [dataSource_current count]*2 + 1;
    
    return [dataSource_current count]*2 + 2;  // empty cells in between + click for more posts
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self getIdentifier:indexPath.row] == POSTCELL)
        return [HomeViewCell_v2 cellHeight];
    else if ([self getIdentifier:indexPath.row] == MORECELL)
        return 40;

    return 29;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[3] = {@"emptycell", @"postcell", @"morecell"};
    CellType type = [self getIdentifier:indexPath.row];
    NSString *identifier = identifiers[type];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        switch (type) {
            case POSTCELL:
                cell = [[HomeViewCell_v2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                [((HomeViewCell_v2*)cell).reportBtn addTarget:self action:@selector(confirmReport:) forControlEvents:UIControlEventTouchUpInside];
                break;
            default:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
        }
        
    }

    // Configure the cell...
    if(type == MORECELL){
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"Bebas Neue" size:18.f];

//        cell.textLabel.text = @"\u2022";
        cell.textLabel.text = @"";//@"Click for more";
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
    }
    else if(type ==EMPTYCELL)
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    else{
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        HomeViewCell_v2 *currentCell = (HomeViewCell_v2*)cell;
        PostingItem *item = [dataSource_current objectAtIndex:(indexPath.row/2)];
        currentCell.reportBtn.tag = [item.mItemID intValue];
        [currentCell setTitleString:item.mTitle];
        [currentCell setUserNameString:item.mDisplayName];
        
        [currentCell.profile setImageWithURL:[NSURL URLWithString:item.mUserProfileURL]
                            placeholderImage:[UIImage imageNamed:@"Profile.png"]];
        
        [currentCell setDateStr:item.mDateStr];

        Vote *myVote = [myVoteDict objectForKey:item.mItemID];
        
        if(item.mPicSingle == NULL){
            [currentCell setImagesWithUrlLeft:item.mPicLeft right:item.mPicRight placeholder:[UIImage imageNamed:@"post_placeholder_small"]];

            if(myVote){
                NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
                NSTimeInterval votedTime = [myVote.time timeIntervalSince1970];
                
                if(now - votedTime < 50){
                    if(!myVote.isVote){
                        item.mAppVote = NotVoted;
                    }
                    else{
                        if(myVote.isLeft){
                            item.mAppVote = VoteLeft;
                        }
                        else{
                            item.mAppVote = VoteRight;
                        }
                        
                    }
                    

                    
                }
            }
            
              
            if(item.mAppVote == NotVoted){
                [currentCell hideVotebar];
            }
            
            else if(item.mAppVote == VoteLeft)
               [currentCell setVotebar:[NSString stringWithFormat:@"%d%%", (int)item.mVotesLeft*100/(item.mVotesLeft+item.mVotesRight)] side:LEFT];
            
            else if(item.mAppVote == VoteRight)
               [currentCell setVotebar:[NSString stringWithFormat:@"%d%%", (int)item.mVotesRight*100/(item.mVotesLeft+item.mVotesRight)] side:RIGHT];
            
            [currentCell setFooter:(item.mVotesLeft + item.mVotesRight) Comments:item.mNumComments Shared:item.mNumShared];
            
        }
    
        else{
            [currentCell setSingleImageWithURL:item.mPicSingle placeholder:[UIImage imageNamed:@"post_placeholder_large"]];

            if(myVote){
                NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
                NSTimeInterval votedTime = [myVote.time timeIntervalSince1970];
                
                if(now - votedTime < 50){
                    if(myVote.isVote){
                        item.mAppVote = VoteLeft;
                    }
                    else{
                        item.mAppVote = NotVoted;
                    }
                }
            }
            
            if(item.mAppVote == NotVoted)
                [currentCell hideVotebar];

            
            else{
                [currentCell setVotebar:[NSString stringWithFormat:@"%d LIKES", (int)item.mVotesSingle] side:RIGHT];
            }

            [currentCell setFooter:item.mVotesSingle Comments:item.mNumComments Shared:item.mNumShared];
         
        }
        
        
        
        UITapGestureRecognizer *leftrecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detectTouchImage:)];
        UITapGestureRecognizer *rightrecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detectTouchImage:)];
        
        [currentCell leftImageView].tag = indexPath.row/2 * 10;
        [currentCell leftImageView].userInteractionEnabled = YES;
        
        [currentCell rightImageView].tag = indexPath.row/2 * 10 + 1;
        [currentCell rightImageView].userInteractionEnabled = YES;
        
        [[currentCell leftImageView] addGestureRecognizer:leftrecognizer];
        [[currentCell rightImageView] addGestureRecognizer:rightrecognizer];
    }
    
    
    
    return cell;
}

#pragma mark - report & releated UIAlertViewDelegate

const int ALERT_REPORT = 1;
const int ALERT_LOGIN = 2;

int alertType = -1;

int reportNumber;
- (void)confirmReport:(UIButton*)sender{
    reportNumber = sender.tag;
    
    alertType = ALERT_REPORT;
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Report"
                          message:@"Do you want to report this post \nfor removal?"
                          delegate: self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Yes", @"No", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertType == ALERT_REPORT && buttonIndex == 0){
        [SharedNetworkManager reportPost:[NSNumber numberWithInt:reportNumber] memo:nil sender:self];

    }
    else if(alertType == ALERT_LOGIN && buttonIndex == 0){
        NSLog(@"goto login");
        [homeTableViewControllerDelegate popToMain];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}




-(void)detectTouchImage:(UIGestureRecognizer *)gestureRecognizer;
{
    UIImageView *myImageView = (UIImageView*)gestureRecognizer.view;
    int itemNum = myImageView.tag / 10;
    
    PostingItem *item = [dataSource_current objectAtIndex:itemNum];

    Vote *vote = [[Vote alloc] init];
    vote.time = [NSDate date];
    
    if([SharedProfile isLoginSkipped]){
        NSLog(@"login skipped!");
        alertType = ALERT_LOGIN;
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"PIIKIT"
                              message:@"PIIKIT needs you to login to vote.\nDo you want to Join or Login?"
                              delegate: self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"Yes", @"No", nil];
        [alert show];
        
        return;
    }
    
    
    
    if(myImageView.tag % 10 == 0){ // LEFT
        NSLog(@"image => %d left", itemNum);

        vote.isLeft = YES;
        if([item isPostingSingle]){
            if(item.mAppVote == VoteLeft){
                vote.isVote = NO;
                item.mVotesSingle--;
                [SharedNetworkManager unvotePostID:item.mItemID sender:NULL];
            }
            
            else{
                vote.isVote = YES;
                item.mVotesSingle++;
                [SharedNetworkManager votePostID:item.mItemID left:YES sender:NULL];
            }
        }
        else{
            if(item.mAppVote == VoteLeft){
                vote.isVote = NO;
                item.mVotesLeft--;
                [SharedNetworkManager unvotePostID:item.mItemID sender:NULL];
            }
            else if(item.mAppVote == VoteRight){
                vote.isVote = YES;
                item.mVotesLeft++;
                item.mVotesRight--;
                [SharedNetworkManager votePostID:item.mItemID left:YES sender:NULL];
                
            }
            else{
                vote.isVote = YES;
                item.mVotesLeft++;
                [SharedNetworkManager votePostID:item.mItemID left:YES sender:NULL];
            }
            
        }
    }
    else{   // RIGHT
        NSLog(@"image => %d right", itemNum);
        vote.isLeft = NO;
        if(item.mAppVote == VoteRight){
            vote.isVote = NO;
            item.mVotesRight--;
            [SharedNetworkManager unvotePostID:item.mItemID sender:NULL];
        }
        else if(item.mAppVote == VoteLeft){
            vote.isVote = YES;
            item.mVotesRight++;
            item.mVotesLeft--;
            [SharedNetworkManager votePostID:item.mItemID left:NO sender:NULL];
        }
        else{
            vote.isVote = YES;
            item.mVotesRight++;
            [SharedNetworkManager votePostID:item.mItemID left:NO sender:NULL];
        }
    }
    
    [myVoteDict setObject:vote forKey:item.mItemID];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:(itemNum*2)+1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationFade];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.refreshControl endRefreshing];
    NSLog(@"row selected %d", indexPath.row/2);
    
    if ([self getIdentifier:indexPath.row] == MORECELL) {
        NSLog(@"MORE CELL !");
        [homeTableViewControllerDelegate displayHUD];
        [SharedNetworkManager fetchMorePostings:Recent category:lastLoadedCatId sender:self];
        
        return;
    }
    else if ([self getIdentifier:indexPath.row] == EMPTYCELL) {
        NSLog(@"EMPTY CELL !");
        NSLog(@"view size = %f", self.view.frame.size.height);
        return;
    }
    else{
       // load detail
        if(detailLoaderDelegate){
            PostingItem *item = [dataSource_current objectAtIndex:indexPath.row/2];
            /*
            NSLog(@"change vote");
            Vote *vote = [[Vote alloc] init];
            vote.time = [NSDate date];
            vote.isVote = YES;
            item.mMyVote = VoteLeft;
            [myVoteDict setObject:vote forKey:item.mItemID];
            */
            
            [detailLoaderDelegate loadDetailWithID:item.mItemID atRow:indexPath.row sender:self];
            
            //NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationFade];
            
            
        }
        else
            NSLog(@"detailLoaderDelegate not set");
    }
    
    
}


- (void) loadPopular{
    NSLog(@"load popular");
    [homeTableViewControllerDelegate displayHUD];
    [SharedNetworkManager fetchNewPostings:Popular sender:self];
}

- (void) loadRecent{
    NSLog(@"load Recent");
    [homeTableViewControllerDelegate displayHUD];
    [SharedNetworkManager fetchNewPostings:Recent sender:self];
   
}

- (void) loadFollowing;{
    NSLog(@"load Following");
    [homeTableViewControllerDelegate displayHUD];
    [SharedNetworkManager fetchNewPostings:Following sender:self];
}

- (void) loadCategoryId:(NSNumber*)catId{
    NSLog(@"load Category");
    
    [homeTableViewControllerDelegate displayHUD];
    [SharedNetworkManager fetchNewCategory:catId sender:self];
}

#pragma mark - network delegate
- (void)networkManagerError{
    NSLog(@"Network error");
    [homeTableViewControllerDelegate hideHUD];    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)returnReportPost:(BOOL)success{
    UIAlertView *alert;
    if(success)
        alert = [[UIAlertView alloc] initWithTitle:@"Report"
                                           message:@"Report has been filed to PIIKIT" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    else
        alert = [[UIAlertView alloc] initWithTitle:@"Report"
                                           message:@"Reporting failed. Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

int itemsNeedCounter = 10;

- (void)returnFetchedItems:(NSArray*)items newItems:(BOOL)isNew isEnd:(BOOL)isEnd{
  NSLog(@"hometableviewcontroller; items returned : %d items", [items count]);

    if(isNew){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",
                                 [formatter stringFromDate:[NSDate date]]];
        self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
        [self.refreshControl endRefreshing];

        
        [dataSource_current removeAllObjects];
    }
 
    int newItemCount = 0;
    for(PostingItem* item in items){
        if(item.mMyVote == NotVoted){
            [dataSource_current addObject:item];
            newItemCount++;
            itemsNeedCounter--;
        }
    }
//    [dataSource_current addObjectsFromArray:items];
    mEndOfPosting = isEnd;
    
    if(!mEndOfPosting && itemsNeedCounter > 0){
        [SharedNetworkManager fetchMorePostings:Recent category:lastLoadedCatId sender:self];
        
    }
    else{
        itemsNeedCounter = 10;
        [self.tableView reloadData];
        [homeTableViewControllerDelegate hideHUD];
    }
    
    if(isNew){
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }

}



#pragma mark - detailViewControllerDelegate
- (void)returnVote:(Vote *)vote atRow:(int)row{
    NSLog(@"returned vote %d", row);

    PostingItem *item = [dataSource_current objectAtIndex:(row/2)];
    if([item isPostingSingle]){
        if(vote.isVote)
            item.mVotesSingle++;
        else
            item.mVotesSingle--;
    }
    else{  // item double posting
        if(vote.isVote && vote.isLeft)
            item.mVotesLeft++;
        else if(vote.isVote && !vote.isLeft)
            item.mVotesRight++;
        else if(!vote.isVote && vote.isLeft)
            item.mVotesLeft--;
        else if(!vote.isVote && !vote.isLeft)
            item.mVotesRight--;
    }
    
    [myVoteDict setObject:vote forKey:vote.itemId];
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationFade];

}

@end
