//
//  HomeViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 9..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewController.h"
#import "MBProgressHUD.h"

#import "DetailLoaderProtocol.h"

@interface HomeViewController : UIViewController <HomeTableViewControllerDelegate>{
    UILabel *mPopularLabel;
    UILabel *mRecentLabel;
    UILabel *mFollowingLabel;
    
    HomeTableViewController *tableView;
    
    MBProgressHUD *HUD;   
}

@property (nonatomic, retain) HomeTableViewController *tableView;
@property (assign) id<DetailLoaderDelegate> detailLoaderDelegate;

@end
