//
//  DetailLoaderProtocol.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//
#import "DetailViewController.h"

@protocol DetailLoaderDelegate <NSObject>
- (void) loadDetailWithID:(NSNumber*)detailId atRow:(int)row sender:(id<DetailViewControllerDelegate>)delegate;

@end
