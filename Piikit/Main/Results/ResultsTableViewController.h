//
//  ResultsTableViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"

#import "DetailLoaderProtocol.h"

@protocol ResultTableViewControllerDelegate <NSObject>

- (void)displayHUD;
- (void)hideHUD;

@end

/*
typedef NS_ENUM(NSInteger, ResultRequestType){
    Voted,
    Posted,
    Bookmarks
};*/

@interface ResultsTableViewController : UITableViewController<NetworkDelegate>{
  /*  NSMutableArray *dataSource_updated;
    NSMutableArray *dataSource_voted;
    NSMutableArray *dataSource_created;
    */
    NSMutableArray *dataSource_current;
    
    BOOL endOfPosting;
    
    ResultRequestType resultType;
    
}

@property (nonatomic, assign) BOOL endOfPosting;
@property (assign) id<ResultTableViewControllerDelegate> resultTableViewControllerDelegate;
@property (assign) id<DetailLoaderDelegate> detailLoaderDelegate;

- (void) loadVoted_isNew:(BOOL)isNew; //voted + commented
- (void) loadPosted_isNew:(BOOL)isNew;
- (void) loadBookmarks_isNew:(BOOL)isNew;

- (void) setResultType:(ResultRequestType)type;
@end
