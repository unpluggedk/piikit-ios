//
//  Results2TableViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Results2TableViewController : UITableViewController{
    NSMutableArray *dataSource_updated;
    NSMutableArray *dataSource_voted;
    NSMutableArray *dataSource_created;
    
    NSMutableArray *dataSource_current;
    
    Boolean endReached;
    
    int numRows;
}

@property (nonatomic, assign) Boolean endReached;



@end

