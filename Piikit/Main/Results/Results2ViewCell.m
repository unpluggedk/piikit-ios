//
//  Results2ViewCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 8. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "Results2ViewCell.h"

@implementation Results2ViewCell

@synthesize leftCell;
@synthesize rightCell;

#define LEFTCELL 101;
#define RIGHTCELL 102;

+ (CGFloat) cellHeight{
    return 115;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        leftCell = [[SingleResult alloc] initWithFrame:CGRectMake(15,0,140,51)];
        leftCell.userInteractionEnabled = YES;
        leftCell.tag = LEFTCELL;
        rightCell = [[SingleResult alloc] initWithFrame:CGRectMake(165,0,140,51)];
        rightCell.userInteractionEnabled = YES;
        rightCell.tag = RIGHTCELL;
        
        [self.contentView addSubview:leftCell];
        [self.contentView addSubview:rightCell];
        
        leftCell.hidden = YES;
        rightCell.hidden = YES;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
