//
//  ResultsTableViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ResultsTableViewController.h"
#import "PostingItem.h"
#import "ResultsViewCell.h"
#import "SingleResult.h"
#import "Profile.h"

@interface ResultsTableViewController ()

@end

@implementation ResultsTableViewController
@synthesize endOfPosting;
@synthesize resultTableViewControllerDelegate;
@synthesize detailLoaderDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        endOfPosting = NO;
        resultType = -1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
/*
    PostingItem *item1 = [[PostingItem alloc] initWithID:NULL
                                                Username:@"PIIK-LIKE-PSY"
                                                   Title:@"Better gangnam style?"
                                             Description:NULL
                                               LabelLeft:@"" LabelRight:@""
                                              ProfileURL:@"profile-1-placeholder.png"
                                                 PicLeft:NULL
                                                PicRight:NULL
                                               PicSingle:@"http://test.piikit.com:8080/static/uploads/2-1373008633768.png"
                                            NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:23
                                             NumComments:3
                                               NumShared:0
                                        DateCreatedMilli:1234 DateUpdatedMilli:1234];

    PostingItem *item2 = [[PostingItem alloc] initWithID:NULL
                                                Username:@"PIIK-LIKE-PSY"
                                                   Title:@"Better gangnam style?"
                                             Description:NULL
                                               LabelLeft:@"" LabelRight:@""
                                              ProfileURL:@"profile-1-placeholder.png"
                                                 PicLeft:@"http://test.piikit.com:8080/static/uploads/2-1373008633768.png"
                                                PicRight:@"http://test.piikit.com:8080/static/uploads/20-1372998959617.png"
                                               PicSingle:NULL
                                            NumVotesLeft:2 NumVotesRight:3 NumVotesSingle:0
                                             NumComments:3
                                               NumShared:0
                                        DateCreatedMilli:1234 DateUpdatedMilli:1234];
    */
   // dataSource_created = [[NSMutableArray alloc] initWithObjects:item1, item2, nil];
   // dataSource_updated= [[NSMutableArray alloc] initWithObjects:@"popular1", @"popular2", @"popular3", nil];
   // dataSource_voted = [[NSMutableArray alloc] initWithObjects:@"recent1", @"recent2", @"recent3", nil];
    
    dataSource_current = [[NSMutableArray alloc] init];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to refresh"];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.tableView.contentOffset = CGPointMake(0, 30);
    

}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!endOfPosting && indexPath.row == [dataSource_current count]*2 + 1) {
        NSLog(@"load");
        switch (resultType) {
            case Voted_New:
                NSLog(@"0");
                [self loadVoted_isNew:YES];
                break;
                
            case Voted:
                NSLog(@"1");
                [self loadVoted_isNew:NO];
                break;
                
            case Bookmarks:
                [self loadBookmarks_isNew:NO];
                break;
                
            case Posted:
                [self loadPosted_isNew:NO];
                break;
                
            default:
                break;
                
        }
    }
    
}

-(void)refreshView:(UIRefreshControl *)refresh {
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    [resultTableViewControllerDelegate displayHUD];
    
    switch (resultType) {
        case Voted:
            [self loadVoted_isNew:YES];
            break;
           
        case Bookmarks:
            [self loadBookmarks_isNew:YES];
            break;
        
        case Posted:
            [self loadPosted_isNew:YES];
            break;
            
        default:
            break;
    }
    // custom refresh logic would be placed here...
    
    //    [SharedNetworkManager fetchNewPostings:Recent sender:self];
    //[SharedNetworkManager fetchNewCategory:lastLoadedCatId sender:self];
    //[homeTableViewControllerDelegate displayHUD];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

typedef enum{
    EMPTYCELL = 0,
    POSTCELL = 1,
    MORECELL = 2
} CellType;

- (CellType) getIdentifier:(int)row{
    int range = [dataSource_current count] * 2;
    
    if(row < range){
        if(row %2 == 0)
            return EMPTYCELL;
        else
            return POSTCELL;
    }
    else if(row == range){
        return EMPTYCELL;
    }
    return MORECELL;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self getIdentifier:indexPath.row] == POSTCELL)
        return [ResultsViewCell cellHeight];
    else if ([self getIdentifier:indexPath.row] == MORECELL)
        return 40;
    return 29;
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(endOfPosting && [dataSource_current count] == 0)
        return 2;
    
    if(endOfPosting)
        return [dataSource_current count] * 2;
    
    return [dataSource_current count] * 2 + 2;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[3] = {@"emptycell", @"postcell", @"morecell"};
    CellType type = [self getIdentifier:indexPath.row];
    NSString *identifier = identifiers[type];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        switch (type) {
            case POSTCELL:
                cell = [[ResultsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            default:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
        }
        
    }

    if(type == MORECELL){
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"Bebas Neue" size:18.f];
        
        //        cell.textLabel.text = @"\u2022";
        cell.textLabel.text = endOfPosting?@"No Activities Found":@"";//@"Click for more";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    else if(type ==EMPTYCELL)
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    else{
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ResultsViewCell *currentCell = (ResultsViewCell*)cell;
        PostingItem *item = [dataSource_current objectAtIndex:(indexPath.row/2)];
        
        [currentCell setTitleString:item.mTitle];
        if([item isPostingSingle]){
            [currentCell setSingleImageWithURL:item.mPicSingle placeholder:[UIImage imageNamed:@"post_placeholder_large"]];
            [currentCell setFooter:item.mVotesSingle Comments:item.mNumComments Shared:item.mNumShared];
            [currentCell setScoreSingle:item.mVotesSingle];
        }
        else{
            [currentCell setImagesWithUrlLeft:item.mPicLeft right:item.mPicRight
                                  placeholder:[UIImage imageNamed:@"post_placeholder_small"]];
            [currentCell setFooter:item.mVotesLeft+item.mVotesRight Comments:item.mNumComments Shared:item.mNumShared];
            [currentCell setScoreLeft:item.mVotesLeft Right:item.mVotesRight];
            //[currentCell setScoreLeft:0 Right:1];
        }

    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"row selected = %d", indexPath.row);
    NSLog(@"number of rows = %d", [tableView numberOfRowsInSection:0]);
    
    if([tableView numberOfRowsInSection:0] == indexPath.row + 1){
        NSLog(@"more?");
        switch (resultType) {
            case Voted:
                [self loadVoted_isNew:NO];
                break;
                
            case Bookmarks:
                [self loadBookmarks_isNew:NO];
                break;
                
            case Posted:
                [self loadPosted_isNew:NO];
                break;
                
            default:
                break;
        
        }
        return;
    }
    if(indexPath.row % 2 == 0){
        NSLog(@"empty cell");
        return;
    }
    if([dataSource_current count] > 0){
        PostingItem *item = [dataSource_current objectAtIndex:(indexPath.row/2)];
        [detailLoaderDelegate loadDetailWithID:item.mItemID atRow:indexPath.row sender:NULL];
    }
    
   
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    // [resultTableViewControllerDelegate displayHUD];
    
}


- (void) loadVoted_isNew:(BOOL)isNew{
    [resultTableViewControllerDelegate displayHUD];
    resultType = Voted;
    [SharedNetworkManager fetchResultsWithType:Voted isNew:isNew userId:[SharedProfile userId] sender:self];
}

- (void) loadPosted_isNew:(BOOL)isNew{
    [resultTableViewControllerDelegate displayHUD];
    resultType = Posted;
    [SharedNetworkManager fetchResultsWithType:Posted isNew:isNew userId:[SharedProfile userId] sender:self];
}

- (void) loadBookmarks_isNew:(BOOL)isNew{
    [resultTableViewControllerDelegate displayHUD];
    resultType = Bookmarks;
    [SharedNetworkManager fetchResultsWithType:Bookmarks isNew:isNew userId:[SharedProfile userId] sender:self];
}


#pragma mark - network delegate
- (void)networkManagerError{
    NSLog(@"Network error 1 ");
    [resultTableViewControllerDelegate hideHUD];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}


- (void)returnFetchedItems:(NSArray *)items newItems:(BOOL)isNew isEnd:(BOOL)isEnd{
    NSLog(@"items returned : %d items", [items count]);
    
    if(isNew){
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",
                                 [formatter stringFromDate:[NSDate date]]];
        self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
        [self.refreshControl endRefreshing];
        
        [dataSource_current removeAllObjects];
    }
    [dataSource_current addObjectsFromArray:items];
    
    endOfPosting = isEnd;
    /*
    if([items count] < 10){
        endOfPosting = YES;
        NSLog(@"END OF POSTING!!");
    }
    else
        endOfPosting = NO;
    */
    [self.tableView reloadData];
    
    [resultTableViewControllerDelegate hideHUD];
}



- (void) setResultType:(ResultRequestType)type{
    resultType = type;
}




@end
