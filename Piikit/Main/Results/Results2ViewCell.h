//
//  Results2ViewCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleResult.h"

@interface Results2ViewCell : UITableViewCell {
    SingleResult *leftCell;
    SingleResult *rightCell;
}

@property (nonatomic, retain) SingleResult *leftCell;
@property (nonatomic, retain) SingleResult *rightCell;

+ (CGFloat) cellHeight;


@end
