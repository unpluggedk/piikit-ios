//
//  ResultsViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ResultsViewController.h"

#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "AppData.h"
#import "IIViewDeckController.h"

#import "Profile.h"

#import "NeedToLoginView.h"

@interface ResultsViewController ()

@end

@implementation ResultsViewController

@synthesize detailLoaderDelegate;

#define VOTED 101
#define POSTED 102
#define BOOKMARKS 103

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

/*
- (void)viewDidLayoutSubviews{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        CGRect viewBounds = self.view.bounds;
        CGFloat topBarOffset = self.topLayoutGuide.length;
        viewBounds.origin.y = topBarOffset * -1;
        self.view.bounds = viewBounds;
        
        //float heightPadding = statusBarViewRect.size.height+self.navigationController.navigationBar.frame.size.height;
        
        //myContentView.contentInset = UIEdgeInsetsMake(heightPadding, 0.0, 0.0, 0.0);

    }

}
*/

- (NSAttributedString *)getTabbarString:(NSString*)str{
    UIFont *tabBarFont = [UIFont fontWithName:@"AudimatMono" size:12.f];
    NSNumber *linespace = [NSNumber numberWithFloat:0.7f];
    
    NSMutableAttributedString *out = [[NSMutableAttributedString alloc] initWithString:str];
    [out addAttribute:NSFontAttributeName value:tabBarFont range:NSMakeRange(0, out.length)];
    [out addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, out.length)];
    [out addAttribute:(NSString*)kCTKernAttributeName value:linespace range:NSMakeRange(0, out.length)];
    
    return out;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // ios 7
    int y = 0;
    if([SharedAppData isIos7])
        y = 20;
        
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(0, y, 320,30)];
    tabBar.backgroundColor = [UIColor blackColor];
    tabBar.layer.borderColor = [UIColor whiteColor].CGColor;
    tabBar.layer.borderWidth = 1.f;
    
    
    mLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 107, 30)];
    mLeftLabel.backgroundColor = [UIColor blackColor];
    mLeftLabel.textAlignment = NSTextAlignmentCenter;
    mLeftLabel.attributedText = [self getTabbarString:@"VOTED"];
    mLeftLabel.tag = VOTED;
    mLeftLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mLeftLabel];
    
    mCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(107, 0, 107, 30)];
    mCenterLabel.backgroundColor = [UIColor blackColor];
    mCenterLabel.textAlignment = NSTextAlignmentCenter;
    mCenterLabel.attributedText = [self getTabbarString:@"POSTED"];
    mCenterLabel.tag = POSTED;
    mCenterLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mCenterLabel];
    
    mRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(213, 0, 107, 30)];
    mRightLabel.backgroundColor = [UIColor blackColor];
    mRightLabel.textAlignment = NSTextAlignmentCenter;
    mRightLabel.attributedText = [self getTabbarString:@"BOOKMARKS"];
    mRightLabel.tag = BOOKMARKS;
    mRightLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mRightLabel];
    
    
    UIView *line;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(107, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(213, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    
    [self.view addSubview:tabBar];

    if([SharedProfile isLoginSkipped]){
        mLeftLabel.userInteractionEnabled = NO;
        mRightLabel.userInteractionEnabled = NO;
        mCenterLabel.userInteractionEnabled = NO;
        
        NeedToLoginView *loginView = [[NeedToLoginView alloc] initWithFrame:CGRectMake(0, 30 + y, 320, self.view.frame.size.height - 30 - y)];
        [loginView.loginBtn addTarget:self action:@selector(popToMain) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:loginView];
        
        
        return;
    }
    
    tableView = [[ResultsTableViewController alloc] init];
    tableView.tableView.dataSource = tableView;
    tableView.tableView.delegate = tableView;
    tableView.detailLoaderDelegate = detailLoaderDelegate;
    tableView.resultTableViewControllerDelegate = self;
    tableView.tableView.frame = CGRectMake(0, 30 + y, 320, self.view.frame.size.height - 30 - y); //20 : status bar    49: tabbar    15: topbar
    
    [tableView setResultType:Voted_New];
    
    //tableView.tableView.contentInset = UIEdgeInsetsMake(29, 0, 0, 0);
    [self.view addSubview:tableView.tableView];

    tableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Loading";
    HUD.userInteractionEnabled = NO;
    
    mLeftLabel.textColor = [UIColor whiteColor];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    int tag = [[[touches anyObject] view] tag];
    switch (tag) {
        case VOTED:
            mLeftLabel.textColor = [UIColor whiteColor];
            mCenterLabel.textColor = [UIColor darkGrayColor];
            mRightLabel.textColor = [UIColor darkGrayColor];
            [tableView loadVoted_isNew:YES];
            break;
        case POSTED:
            mLeftLabel.textColor = [UIColor darkGrayColor];
            mCenterLabel.textColor = [UIColor whiteColor];
            mRightLabel.textColor = [UIColor darkGrayColor];
            [tableView loadPosted_isNew:YES];
            break;
        case BOOKMARKS:
            mLeftLabel.textColor = [UIColor darkGrayColor];
            mCenterLabel.textColor = [UIColor darkGrayColor];
            mRightLabel.textColor = [UIColor whiteColor];
            [tableView loadBookmarks_isNew:YES];
            break;
        default:
            break;
    }
    
}


- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    self.tabBarController.viewDeckController.panningMode = IIViewDeckFullViewPanning;
    self.tabBarController.viewDeckController.panningMode = IIViewDeckNoPanning;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)displayHUD{
    
    [HUD show:YES];
}

- (void)hideHUD{
    [HUD hide:YES];
    
}

- (void)popToMain{
    [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
}


@end
