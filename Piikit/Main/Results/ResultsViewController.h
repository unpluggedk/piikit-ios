//
//  ResultsViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultsTableViewController.h"
#import "MBProgressHUD.h"

#import "DetailLoaderProtocol.h"

@interface ResultsViewController : UIViewController<ResultTableViewControllerDelegate>{
    UILabel *mLeftLabel;
    UILabel *mCenterLabel;
    UILabel *mRightLabel;
    
    ResultsTableViewController *tableView;
    
    MBProgressHUD *HUD;  
}

@property (assign) id<DetailLoaderDelegate> detailLoaderDelegate;


@end
