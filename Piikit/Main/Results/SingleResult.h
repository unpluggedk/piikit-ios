//
//  SingleResult.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleResult : UIView{
    UIView *titleView;
    UILabel *titleLabel;
    
    UIImageView *leftImageView;
    UIImageView *rightImageView;
    
    UIView *footerView;
    
    UILabel *leftLabel;
    UILabel *rightLabel;
    
}

@property (nonatomic, retain) UIView *titleView;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UIImageView *leftImageView;
@property (nonatomic, retain) UIImageView *rightImageView;
@property (nonatomic, retain) UIView *footerView;

- (void)setTitleString:(NSString*)titleStr;

- (void)setLeftImage:(UIImage*)img;
- (void)setRightImage:(UIImage*)img;
- (void)setSingleImage:(UIImage*)img;

- (void)setVotesSingle:(int)single;
- (void)setVotesLeft:(int)left Right:(int)right;

@end
