//
//  Results2TableViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 8. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "Results2TableViewController.h"
#import "PostingItem.h"
#import "Results2ViewCell.h"
#import "SingleResult.h"

@interface Results2TableViewController ()

@end

@implementation Results2TableViewController
@synthesize endReached;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        endReached = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    /*
     PostingItem *item1 = [[PostingItem alloc] initWithID:123
     Username:@"PIIK-LIKE-PSY"
     Question:@"Better gangnam style?"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:@"profile-1-placeholder.png"
     PicLeft:@"placeholder-image-left.png"
     PicRight:@"placeholder-right.png"
     PicSingle:NULL
     NumVotesLeft:65 NumVotesRight:23 NumVotesSingle:0 NumComments:22 NumShared:12];
     */
    PostingItem *item1 = [[PostingItem alloc] initWithID:NULL
                                                DisplayName:@"PIIK-LIKE-PSY"
                                                   Title:@"Better gangnam style?"
                                             Description:NULL
                                               LabelLeft:@"" LabelRight:@""
                                              ProfileURL:@"profile-1-placeholder.png"
                                                 PicLeft:NULL
                                                PicRight:NULL
                                               PicSingle:@"placeholder-left.png"
                                            NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:23
                                             NumComments:3
                                               NumShared:0
                                        DateCreatedMilli:1234 DateUpdatedMilli:1234];
    
    
    item1.mDateStr = @"05/06/13 3:28pm";
    /*
     PostingItem *item2 = [[PostingItem alloc] initWithID:123
     Username:@"CONTEMPORARYBIYER"
     Question:@"ALEXANDER WANG? OR PHILLIP LIM?"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:@"profile-2-placeholder.png"
     PicLeft:@"placeholder-2-left.png"
     PicRight:@"placeholder-2-right.png"
     PicSingle:NULL//@"placeholder-2-right.png"
     NumVotesLeft:11 NumVotesRight:0 NumVotesSingle:13 NumComments:1 NumShared:0];
     
     item2.mDateStr = @"05/05/13 9:28pm";
     
     PostingItem *item3 = [[PostingItem alloc] initWithID:123
     Username:@"CONTEMPORARYBIYER"
     Question:@"ALEXANDER WANG? OR PHILLIP LIM?"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:@"profile-2-placeholder.png"
     PicLeft:NULL//@"placeholder-2-left.png"
     PicRight:NULL//@"placeholder-2-right.png"
     PicSingle:@"placeholder-2-right.png"
     NumVotesLeft:11 NumVotesRight:44 NumVotesSingle:13 NumComments:1 NumShared:0];
     
     item3.mDateStr = @"05/05/13 9:28pm";
     
     PostingItem *item4 = [[PostingItem alloc] initWithID:123
     Username:@"CONTEMPORARYBIYER"
     Question:@"ALEXANDER WANG? OR PHILLIP LIM?"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:NULL//@"profile-2-placeholder.png"
     PicLeft:@"placeholder-2-left.png"
     PicRight:@"placeholder-2-right.png"
     PicSingle:@"placeholder-2-right.png"
     NumVotesLeft:11 NumVotesRight:44 NumVotesSingle:13 NumComments:1 NumShared:0];
     
     item4.mDateStr = @"05/05/13 9:28pm";
     
     PostingItem *item5 = [[PostingItem alloc] initWithID:123
     Username:@"CONTEMPORARYBIYER"
     Question:@"ALEXANDER WANG? OR PHILLIP LIM?"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:@"profile-2-placeholder.png"
     PicLeft:NULL//@"placeholder-2-left.png"
     PicRight:NULL//@"placeholder-2-right.png"
     PicSingle:@"placeholder-2-right.png"
     NumVotesLeft:11 NumVotesRight:44 NumVotesSingle:13 NumComments:1 NumShared:0];
     
     item5.mDateStr = @"05/05/13 9:28pm";
     
     PostingItem *item6 = [[PostingItem alloc] initWithID:123
     Username:@"CONTEMPORARYBIYER"
     Question:@"ALEXANDER WANG? OR PHILLIP LIM?QUESTION IS VERY VERY LONG"
     LabelLeft:@"" LabelRight:@""
     ProfileURL:@"profile-2-placeholder.png"
     PicLeft:@"placeholder-2-left.png"
     PicRight:@"placeholder-2-right.png"
     PicSingle:NULL//@"placeholder-2-right.png"
     NumVotesLeft:11 NumVotesRight:44 NumVotesSingle:13 NumComments:19 NumShared:0];
     
     item6.mDateStr = @"05/05/13 9:28pm";
     */
    //dataSource_created = [[NSMutableArray alloc] initWithObjects:item1, item2, item3, item4, item5, item6, item1, nil];
    dataSource_created = [[NSMutableArray alloc] initWithObjects:item1, item1, nil];
    dataSource_updated= [[NSMutableArray alloc] initWithObjects:@"popular1", @"popular2", @"popular3", nil];
    dataSource_voted = [[NSMutableArray alloc] initWithObjects:@"recent1", @"recent2", @"recent3", nil];
    
    dataSource_current = dataSource_created;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == numRows)
        return 20;
    return [Results2ViewCell cellHeight];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if([dataSource_current count] == 0)
        return 0;
    
    numRows = (1+[dataSource_current count])/2;
    return numRows + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *LastCellIdentifier = @"More";
    
    NSString *identifier;
    
    if(indexPath.row == [dataSource_current count])
        identifier = LastCellIdentifier;
    else
        identifier = CellIdentifier;
    
    UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if(cell == nil){
        if(indexPath.row == numRows){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:@"Bebas Neue" size:15.f];
        }
        else
            cell = [[Results2ViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    if(indexPath.row == numRows){
        cell.textLabel.text = endReached?@"":@"Click for more";
        return cell;
    }
    
    PostingItem *leftItem = [dataSource_current objectAtIndex:(indexPath.row*2)];
    SingleResult *left = ((Results2ViewCell*)cell).leftCell;
    [left setTitleString:leftItem.mTitle];
    if(leftItem.mPicSingle == NULL){
        [left setLeftImage:[UIImage imageNamed:leftItem.mPicLeft]];
        [left setRightImage:[UIImage imageNamed:leftItem.mPicRight]];
        [left setVotesLeft:leftItem.mVotesLeft Right:leftItem.mVotesRight];
    }
    else{
        [left setSingleImage:[UIImage imageNamed:leftItem.mPicSingle]];
        [left setVotesSingle:leftItem.mVotesSingle];
    }
    left.hidden = NO;
    
    if(indexPath.row*2 + 1 >= [dataSource_current count])
        return cell;
    
    
    PostingItem *rightItem = [dataSource_current objectAtIndex:(indexPath.row*2)+1];
    
    SingleResult *right = ((Results2ViewCell*)cell).rightCell;
    
    [right setTitleString:rightItem.mTitle];
    if(rightItem.mPicSingle == NULL){
        [right setLeftImage:[UIImage imageNamed:rightItem.mPicLeft]];
        [right setRightImage:[UIImage imageNamed:rightItem.mPicRight]];
        [right setVotesLeft:rightItem.mVotesLeft Right:rightItem.mVotesRight];
    }
    else{
        [right setSingleImage:[UIImage imageNamed:rightItem.mPicSingle]];
        [right setVotesSingle:rightItem.mVotesSingle];
        
    }
    right.hidden = NO;
    
    // cell.textLabel.text = @"QWE";
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
