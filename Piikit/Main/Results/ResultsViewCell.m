//
//  ResultsViewCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ResultsViewCell.h"
#import <CoreText/CoreText.h>
#import <UIKit/UIStringDrawing.h>

#import "UIImageView+WebCache.h"

@implementation ResultsViewCell

+ (CGFloat) cellHeight{
    return 20+160+20;
}

UIFont *scoreFont;
CGFloat offsetX = 15;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        scoreFont = [UIFont fontWithName:@"Bebas Neue" size:20.f];
        
        CGFloat relativeY = 0;
        
        
        titleView = [[UIView alloc] initWithFrame:CGRectMake(offsetX, relativeY, 290, 20)];
        titleView.backgroundColor = [UIColor blackColor];
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 290-10-10, 20)];
        title.backgroundColor = [UIColor blackColor];
        [titleView addSubview:title];
        [self addSubview:titleView];

        relativeY += 20;
        
        leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(offsetX, relativeY, 145, 160)];
        leftImageView.contentMode = UIViewContentModeScaleAspectFill;
        leftImageView.clipsToBounds = YES;
        
        rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(offsetX + 140, relativeY, 145, 160)];
        rightImageView.contentMode = UIViewContentModeScaleAspectFill;
        rightImageView.clipsToBounds = YES;
        
        rightImageView.hidden = true;
        
        [self addSubview:rightImageView];
        [self addSubview:leftImageView];

        leftBarView = [[UIView alloc] initWithFrame:CGRectMake(offsetX, relativeY + 110, 145, 25)];
        leftBarView.backgroundColor = [UIColor redColor];
        rightBarView = [[UIView alloc] initWithFrame:CGRectMake(offsetX + 145, relativeY + 110, 145, 25)];
        rightBarView.backgroundColor = [UIColor whiteColor];
        
        leftBarLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 25)];
        leftBarLabel.font = scoreFont;
        leftBarLabel.backgroundColor = [UIColor clearColor];
        leftBarLabel.textAlignment = NSTextAlignmentLeft;
        [leftBarView addSubview:leftBarLabel];
        
        rightBarLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 25)];
        rightBarLabel.font = scoreFont;
        rightBarLabel.backgroundColor = [UIColor clearColor];
        rightBarLabel.textAlignment = NSTextAlignmentRight;
        [rightBarView addSubview:rightBarLabel];
                
        [self addSubview:leftBarView];
        [self addSubview:rightBarView];
        
        relativeY += 160;
        footerView = [[UIImageView alloc] initWithFrame:CGRectMake(offsetX, relativeY, 290, 20)];
        footerView.backgroundColor = [UIColor blackColor];
        footer = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 270, 20)];
        footer.textAlignment = NSTextAlignmentRight;
        footer.backgroundColor = [UIColor clearColor];
        [footerView addSubview:footer];
        
        [self addSubview:footerView];


    }
    return self;
}

- (void)setScoreSingle:(CGFloat)likes{
    CGRect frame = leftBarView.frame;
    
    leftBarView.backgroundColor = [UIColor redColor];
    rightBarView.backgroundColor = [UIColor clearColor];
    
    rightBarLabel.text = @"";
    leftBarLabel.textColor = [UIColor whiteColor];
    leftBarLabel.text = [NSString stringWithFormat:@"%d likes", (int)likes];
    
    CGFloat labelSize = [leftBarLabel.text sizeWithFont:scoreFont].width + 20;
    CGFloat leftSize = 290 * (likes==0?1:likes)/10.;
    
    if(leftSize < labelSize)
        leftSize = labelSize;
    
    if(leftSize > 290)
        leftSize = 290;
    
    frame.size.width = leftSize;
    
    leftBarView.frame = frame;
    rightBarView.frame = CGRectMake(offsetX + leftSize, frame.origin.y, 290 - leftSize, 25);
}

- (void)setScoreLeft:(CGFloat)left Right:(CGFloat)right{
    CGRect leftFrame = leftBarView.frame;
    CGRect rightFrame = rightBarView.frame;
    
    if(left >= right){
        leftBarView.backgroundColor = [UIColor redColor];
        leftBarLabel.textColor = [UIColor whiteColor];
        rightBarView.backgroundColor = [UIColor whiteColor];
        rightBarLabel.textColor = [UIColor redColor];
    }
    else{
        leftBarView.backgroundColor = [UIColor whiteColor];
        leftBarLabel.textColor = [UIColor redColor];
        rightBarView.backgroundColor = [UIColor redColor];
        rightBarLabel.textColor = [UIColor whiteColor];
    }
    
    leftBarLabel.text = [NSString stringWithFormat:@"%d", (int)left];
    rightBarLabel.text = [NSString stringWithFormat:@"%d", (int)right];
    
    CGFloat minLeftSize = [leftBarLabel.text sizeWithFont:scoreFont].width + 20;
    CGFloat minRightSize = [rightBarLabel.text sizeWithFont:scoreFont].width + 20;
    
    if(left == 0 & right == 0){
        left = 1;
        right = 1;
    }/*
    else if(left == 0){
        left = 1;
    }
    else if(right == 0){
        right = 1;
    }
    */
    
    CGFloat leftSize = 290.f * (left / (left+right));
    CGFloat rightSize = 290.f - leftSize;
    
    if(leftSize < minLeftSize){
        leftSize = minLeftSize;
        rightSize = 290.f - leftSize;
    }
    
    if(rightSize < minRightSize){
        rightSize = minRightSize;
        leftSize = 290.f - rightSize;
    }
    
    
    
    leftFrame.size.width = leftSize;
    rightFrame.origin.x = offsetX + leftSize;
    rightFrame.size.width = rightSize;
    
    leftBarView.frame = leftFrame;
    rightBarView.frame = rightFrame;
    
    leftBarLabel.frame = CGRectMake(10, 0, leftSize-20, 25);
    rightBarLabel.frame = CGRectMake(10, 0, rightSize-20, 25);

}

- (void)setTitleString:(NSString*)titleStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:[titleStr uppercaseString]];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:12.f] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.f] range:NSMakeRange(0, titleStr.length)];
    
    title.attributedText = outstr;
}


- (void)setSingleImageWithURL:(NSString*)url placeholder:(UIImage*)placeholder{
    rightImageView.hidden = true;
    
    leftImageView.frame = CGRectMake(offsetX, 20, 290, 160);
    [leftImageView setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:placeholder];
    blackDivider.hidden = YES;
    
}

- (void)setImagesWithUrlLeft:(NSString*)urlLeft right:(NSString*)urlRight placeholder:(UIImage*)placeholder{
    rightImageView.hidden = false;
    
    leftImageView.frame = CGRectMake(offsetX, 20, 145, 160);
    [leftImageView setImageWithURL:[NSURL URLWithString:urlLeft]
                  placeholderImage:placeholder];
    [rightImageView setImageWithURL:[NSURL URLWithString:urlRight]
                   placeholderImage:placeholder];
    
    blackDivider.hidden = NO;
}


- (void)setFooter:(int)votes Comments:(int)comments Shared:(int)shared{
    int votesDigit = [[NSString stringWithFormat:@"%d",votes] length];
    int commentsDigit = [[NSString stringWithFormat:@"%d",comments] length];
    int sharedDigit = [[NSString stringWithFormat:@"%d",shared] length];
    
    bool sharedDisplayed = false;
    NSMutableAttributedString *outstr;
    if(sharedDisplayed){
        outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dVOTES, %dCOMMENTS %dSHARED", votes, comments, shared]];
        int x = 0;
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, votesDigit)];
        x += votesDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 7)];
        x += 7;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, commentsDigit)];
        x += commentsDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 9)];
        x += 9;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, sharedDigit)];
        x += sharedDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 6)];
        x += 6;
        
        //[outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, 5)];
        
        
    }
    else{
        outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%dVOTES, %dCOMMENTS ", votes, comments]];
        int x = 0;
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, votesDigit)];
        x += votesDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 7)];
        x += 7;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, commentsDigit)];
        x += commentsDigit;
        
        [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(x, 9)];
        x += 9;
        
        //[outstr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(x, 5)];
    }
    
    
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.f] range:NSMakeRange(0,  outstr.string.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:12.f] range:NSMakeRange(0, outstr.string.length)];
    
    footer.attributedText = outstr;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
