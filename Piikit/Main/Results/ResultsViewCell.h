//
//  ResultsViewCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleResult.h"

@interface ResultsViewCell : UITableViewCell {
    UIView *titleView;
    UILabel *title;
    
    UIImageView *leftImageView;
    UIImageView *rightImageView;
    
    UIView *footerView;
    UILabel *footer;

    UIView *leftBarView;
    UIView *rightBarView;
    
    UILabel *leftBarLabel;
    UILabel *rightBarLabel;
    
    UIView *blackDivider;
}

+ (CGFloat) cellHeight;

- (void)setTitleString:(NSString*)titleStr;
- (void)setSingleImageWithURL:(NSString*)url placeholder:(UIImage*)placeholder;
- (void)setImagesWithUrlLeft:(NSString*)urlLeft right:(NSString*)urlRight placeholder:(UIImage*)placeholder;
- (void)setFooter:(int)votes Comments:(int)comments Shared:(int)shared;

- (void)setScoreSingle:(CGFloat)likes;
- (void)setScoreLeft:(CGFloat)left Right:(CGFloat)right;


@end
