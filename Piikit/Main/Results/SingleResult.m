//
//  SingleResult.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "SingleResult.h"
#import <CoreText/CoreText.h>
#import "AppData.h"

@implementation SingleResult

@synthesize titleView;
@synthesize titleLabel;
@synthesize leftImageView;
@synthesize rightImageView;
@synthesize footerView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 13)];
        titleView.backgroundColor = [UIColor blackColor];
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 130, 13)];
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleView addSubview:titleLabel];
        [self addSubview:titleView];
        
        leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 13, 70, 80)];
        rightImageView =[[UIImageView alloc] initWithFrame:CGRectMake(70, 13, 70, 80)];
        leftImageView.clipsToBounds = YES;
        rightImageView.clipsToBounds = YES;
        [self addSubview:leftImageView];
        [self addSubview:rightImageView];
        
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 83, 140, 13)];
        footerView.backgroundColor = [UIColor blackColor];
        [self addSubview:footerView];
        
        UIFont *font = [UIFont fontWithName:@"BebasNeue" size:12.f];
        leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 70, 13)];
        rightLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 30, 70, 13)];
        leftLabel.font = font;
        rightLabel.font = font;
        leftLabel.textAlignment = NSTextAlignmentLeft;
        rightLabel.textAlignment = NSTextAlignmentRight;
    
        [self addSubview:leftLabel];
        [self addSubview:rightLabel];
        
    }
    return self;
}

- (void)setVotesSingle:(int)single{
    leftLabel.hidden = NO;
    rightLabel.hidden = YES;
    
    leftLabel.text = [NSString stringWithFormat:@"  %d", single];
    leftLabel.frame = CGRectMake(0, 60, 90, 13);
    leftLabel.backgroundColor = [SharedAppData themeColor];
    leftLabel.textColor = [UIColor whiteColor];
    
}
- (void)setVotesLeft:(int)left Right:(int)right{
    leftLabel.hidden = NO;
    rightLabel.hidden = NO;
    
    leftLabel.text = [NSString stringWithFormat:@"  %d", left];
    rightLabel.text = [NSString stringWithFormat:@"%d  ", right];
    
    if(left >= right){
        leftLabel.textColor = [UIColor whiteColor];
        leftLabel.backgroundColor = [SharedAppData themeColor];
        rightLabel.textColor = [SharedAppData themeColor];
        rightLabel.backgroundColor = [UIColor whiteColor];
    }
    else{
        leftLabel.textColor = [SharedAppData themeColor];
        leftLabel.backgroundColor = [UIColor whiteColor];
        rightLabel.textColor = [UIColor whiteColor];
        rightLabel.backgroundColor = [SharedAppData themeColor];
    }

    CGFloat sizeLeft = left*140./(left+right);
    leftLabel.frame = CGRectMake(0, 60, sizeLeft, 13);
    rightLabel.frame = CGRectMake(sizeLeft, 60, 140-sizeLeft, 13);

}



- (void)setTitleString:(NSString*)titleStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:[titleStr uppercaseString]];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:10.f] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, titleStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:.7f] range:NSMakeRange(0, titleStr.length)];
    
    titleLabel.attributedText = outstr;
}


- (void)setLeftImage:(UIImage*)img{
    leftImageView.frame = CGRectMake(0, 13, 70, 80);
    leftImageView.image = img;

}

- (void)setRightImage:(UIImage*)img{
    rightImageView.image = img;
}

- (void)setSingleImage:(UIImage*)img{
    leftImageView.frame = CGRectMake(0, 13, 140, 80);
    leftImageView.image = img;

    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
