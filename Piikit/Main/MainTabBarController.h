//
//  MainTabBarController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "CategoryViewController.h"
#import "HomeViewController.h"

#import "DetailLoaderProtocol.h"

@interface MainTabBarController : UITabBarController<IIViewDeckControllerDelegate, CategoryViewControllerDelegate, DetailLoaderDelegate>{
    HomeViewController *homeVC ;    
}
@end
