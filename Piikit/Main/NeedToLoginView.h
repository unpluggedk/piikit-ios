//
//  NeedToLoginView.h
//  PIIKIT
//
//  Created by Jason Kim on 13. 12. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedToLoginView : UIView{
    UIImageView *logo;
    UIButton *loginBtn;
}

@property (nonatomic, retain) UIImageView *logo;
@property (nonatomic, retain) UIButton *loginBtn;

@end
