//
//  NeedToLoginView.m
//  PIIKIT
//
//  Created by Jason Kim on 13. 12. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "NeedToLoginView.h"

@implementation NeedToLoginView

@synthesize logo, loginBtn;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIFont *font = [UIFont fontWithName:@"Bebas Neue" size:18.f];
        logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
        logo.contentMode = UIViewContentModeCenter;
        logo.image = [UIImage imageNamed:@"piikit-logo.png"];
        
        [self addSubview:logo];
        
        loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        loginBtn.frame = CGRectMake(107.5, 160, 105, 29);
        loginBtn.backgroundColor = [UIColor blackColor];
        loginBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"JOIN / LOGIN"];
        [str addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, str.string.length)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, str.string.length)];
      //  [str addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.5f] range:NSMakeRange(0, str.string.length)];
        
        [loginBtn setAttributedTitle:str forState:UIControlStateNormal];
        
        [self addSubview:loginBtn];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
