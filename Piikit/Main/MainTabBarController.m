//
//  MainTabBarController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "MainTabBarController.h"
#import "PostViewController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "ResultsViewController.h"

#import "DetailViewController.h"

#import "AppData.h"
@interface MainTabBarController ()

@end

@implementation MainTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.tabBar.barStyle = UIBarStyleBlackOpaque;
        
        // ios7
        // to fix last row covered by tabbar
        //if([self.tabBar respondsToSelector:@selector(setTranslucent:)])
        if([SharedAppData isIos7])
            self.tabBar.translucent = NO;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    homeVC = [[HomeViewController alloc] init];
    homeVC.detailLoaderDelegate = self;
    
    PostViewController *postVC = [[PostViewController alloc] init];
    ProfileViewController *profileVC = [[ProfileViewController alloc] init];
    ResultsViewController *resultsVC = [[ResultsViewController alloc] init];
    resultsVC.detailLoaderDelegate = self;
    
    UINavigationController *homeNavBar = [[UINavigationController alloc] initWithRootViewController:homeVC];
    homeNavBar.tabBarItem.title = @"Home";
    homeNavBar.tabBarItem.image = [UIImage imageNamed:@"icon_home"];
    
    UINavigationController *resultNavBar = [[UINavigationController alloc] initWithRootViewController:resultsVC];
    resultNavBar.tabBarItem.title = @"Results";
    resultNavBar.tabBarItem.image = [UIImage imageNamed:@"icon_results.png"];
    
    UINavigationController *postNavBar = [[UINavigationController alloc] initWithRootViewController:postVC];
    postNavBar.tabBarItem.title = @"Post";
    postNavBar.tabBarItem.image = [UIImage imageNamed:@"icon_post.png"];
    
    UINavigationController *profileNavBar = [[UINavigationController alloc] initWithRootViewController:profileVC];
    profileNavBar.tabBarItem.title = @"Profile";
    profileNavBar.tabBarItem.image = [UIImage imageNamed:@"icon_profile.png"];
    
    NSArray *localViewControllersArray = [[NSArray alloc] initWithObjects:homeNavBar, resultNavBar, postNavBar, profileNavBar, nil];
    
    self.viewControllers = localViewControllersArray;
    //self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    //self.selectedIndex = 3;
  
}


- (void)viewDeckController:(IIViewDeckController *)viewDeckController didShowCenterViewFromSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated{
    NSLog(@"show center");


}

- (void)loadCategory:(NSNumber *)categoryId name:(NSString *)str{
    [self.viewDeckController toggleLeftViewAnimated:YES
                                         completion:^(IIViewDeckController *controller, BOOL success) {
                                             [homeVC.tableView setLastLoadedCatId:categoryId];
                                             [homeVC.tableView loadCategoryId:categoryId];
                                         }];
    
   
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadDetailWithID:(NSNumber*)detailId atRow:(int)row sender:(id<DetailViewControllerDelegate>)delegate{
    DetailViewController *detailVC = [[DetailViewController alloc] initWithPostid:detailId atRow:row];
    if(delegate)
        detailVC.detailViewControllerDelegate = delegate;
    
    [self.navigationController pushViewController:detailVC animated:YES];
    
}


@end
