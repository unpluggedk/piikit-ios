//
//  CommentsTableViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 8. 24..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "CommentsTableViewController.h"
#import "Comment.h"
#import "UIImageView+WebCache.h"

#import <QuartzCore/QuartzCore.h>

#import "AppData.h"

@interface CommentsTableViewController ()

@end

@implementation CommentsTableViewController

@synthesize commentsTableViewControllerDelegate;

- (id)initWithPostId:(NSNumber*)postId{
    mPostId = postId;
    self.title = @"Comments";
    mDataSource = [[NSMutableArray alloc] init];
    mCellHeight = [[NSMutableArray alloc] init];
    
    return [super init];
}

/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Comments";
        mDataSource = [[NSMutableArray alloc] init];
        mCellHeight = [[NSMutableArray alloc] init];
        
    }
    return self;
}
*/

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [commentsTableViewControllerDelegate reportNumComments:[mDataSource count]];

}
/*
- (void)viewDidAppear:(BOOL)animated{
    self.tableView.frame = CGRectMake(0, 0, 320, self.tableView.frame.size.height-20);

}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    freshlyLoaded = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    mTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height - 35)];
    
    // ios 7
    if([SharedAppData isIos7])
        mTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    else
        mTableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
    mTableView.dataSource = self;
    mTableView.delegate = self;
    
    tableViewSizeOriginal = mTableView.frame;
    
    [self.view addSubview:mTableView];

    mCommentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-35, 320, 35)];
    mCommentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
    
    mUserComment = [[UITextView alloc] initWithFrame:CGRectMake(10, 5, 220, 25)];
    mUserComment.font = [UIFont fontWithName:@"AudimatMonoShort" size:13.f];
    mUserComment.layer.cornerRadius = 3.0f;
    
    mUserComment.delegate = self;
    [mCommentView addSubview:mUserComment];
    
    [self.view addSubview:mCommentView];

    mSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [mSendBtn setImage:[UIImage imageNamed:@"send.png"] forState:UIControlStateNormal];
    mSendBtn.frame = CGRectMake(250, self.view.frame.size.height -30, 60, 25);
    [mSendBtn addTarget:self action:@selector(sendBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mSendBtn];
    
    /*
    UILabel *sendLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, self.view.frame.size.height -30, 60, 25)];
    sendLabel.font = [UIFont fontWithName:@"AudimatMono" size:13.f];
    sendLabel.text = @"send";
    sendLabel.textColor = [UIColor whiteColor];
    sendLabel.textAlignment = UITextAlignmentCenter;
    sendLabel.backgroundColor = [UIColor blackColor];
    sendLabel.layer.cornerRadius = 3.0f;
    sendLabel.tag = SEND;
    sendLabel.userInteractionEnabled = YES;
    [self.view addSubview:sendLabel];
*/
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [SharedNetworkManager fetchNewComments:mPostId sender:self];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(keyboardWillAnimate:)
                                                  name:UIKeyboardWillShowNotification
                                                object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(keyboardWillAnimate:)
                                                  name:UIKeyboardWillHideNotification
                                                object:nil];
    [commentsTableViewControllerDelegate displayHUD];
    
}

- (void)sendBtnPressed{
    NSLog(@"send");
    //mSendBtn.userInteractionEnabled = NO;
    if([mUserComment.text isEqualToString:@""])
        return;
    
    [commentsTableViewControllerDelegate displayHUD];
    [SharedNetworkManager postComment:mPostId comment:mUserComment.text sender:self];
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    //[mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([mDataSource count] > 0)?[mDataSource count]-1:0 inSection:0]
    //                  atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    [UIView animateWithDuration:[duration doubleValue]
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         if([notification name] == UIKeyboardWillShowNotification)
                         {
                             mTableView.frame = CGRectMake(mTableView.frame.origin.x,
                                                           keyboardBounds.size.height,
                                                           mTableView.frame.size.width,
                                                           mTableView.frame.size.height - keyboardBounds.size.height);
                             
                             
                             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                             
                             
                         }
                         else if([notification name] == UIKeyboardWillHideNotification)
                         {
                             
                             mTableView.frame = tableViewSizeOriginal;
                             
                             
                             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                             
                             
                         }
                         if([mDataSource count] > 0)
                             //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, [duration doubleValue] * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                             [mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[mDataSource count]-1 inSection:0]
                                               atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                         
                         //});
                         
                     } completion:^(BOOL finished) {
                         
 

                     }];
    /*
    if([notification name] == UIKeyboardWillShowNotification)
    {
        mTableView.frame = CGRectMake(mTableView.frame.origin.x,
                                      keyboardBounds.size.height,
                                      mTableView.frame.size.width,
                                      mTableView.frame.size.height - keyboardBounds.size.height);
        
  
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];


    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {

        mTableView.frame = tableViewSizeOriginal;

     
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];


    }
    [UIView commitAnimations];
    
    if([mDataSource count] > 0)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, [duration doubleValue] * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[mDataSource count]-1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];

        });
     */
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [mDataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CommentsViewCell calcCellHeight:((Comment*)[mDataSource objectAtIndex:indexPath.row]).mComment];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    static const id identifiers[3] = {@"emptycell", @"postcell", @"morecell"};
    CellType type = [self getIdentifier:indexPath.row];
    NSString *identifier = identifiers[type];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        switch (type) {
            case POSTCELL:
                cell = [[HomeViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            default:
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
        }
        
    }
    */

    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[CommentsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }

    // Configure the cell...
    Comment *comment = [mDataSource objectAtIndex:indexPath.row];
    NSLog(@"num likes %@   num dislikes %@", [comment mNumLikes], [comment mNumDisikes]);
    
    //cell.textLabel.text =  comment.mComment;
    CommentsViewCell *commentCell = (CommentsViewCell*)cell;
    commentCell.commentsViewCellDelegate = self;
    commentCell.commentId = comment.mCommentId;
    [commentCell setUsername:comment.mUsername];
    [commentCell.imageView setImageWithURL:[NSURL URLWithString:comment.mProfileURLString]
                   placeholderImage:[UIImage imageNamed:@"Profile.png"]];
    [commentCell setComment:comment.mComment];
    [commentCell setTimeDiff:comment.mTimeDiff];
    
    [commentCell setLikes:[comment.mNumLikes intValue] Dislikes:[comment.mNumDisikes intValue]];
    
    commentCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */

 //   NSLog(@"selected");
    if ([mUserComment isFirstResponder]) {
        [mUserComment resignFirstResponder];
    }
}



#pragma mark - textview delegate
- (void)textViewDidChange:(UITextView *)textView{
    CGFloat newContentHeight = textView.contentSize.height;

//    mTableView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - (10 + newContentHeight));
    
    mCommentView.frame = CGRectMake(0, self.view.frame.size.height - (10 + newContentHeight), 320, 10 + newContentHeight);
    mUserComment.frame = CGRectMake(5, 5, 220, newContentHeight);
//    CGRect frame = textView.frame;
  //  frame.size.height = textView.contentSize.height;
    //textView.frame = frame;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
   }
  
   return YES;
}


#pragma mark - network delegate
- (void)networkManagerError{
    [commentsTableViewControllerDelegate hideHUD];
    mSendBtn.userInteractionEnabled = YES;

    // add alert for retry
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)returnComments:(NSArray *)comments newItems:(BOOL)isNew endOfComments:(BOOL)isEnd{

    [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Comment *comment = (Comment*)obj;
        [mDataSource insertObject:comment atIndex:0];
//        [mDataSource addObject:comment];
    }];

    if(!isEnd){
        [SharedNetworkManager fetchMoreComments:mPostId sender:self];
    }
    else{
        mSendBtn.userInteractionEnabled = YES;
        [commentsTableViewControllerDelegate hideHUD];
        [mTableView reloadData];
        
        if([mDataSource count] > 0 && freshlyLoaded){
            [mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[mDataSource count]-1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            freshlyLoaded = NO;
        }
    }
    

}



- (void)returnCommentComplete:(BOOL)success Comment:(Comment *)comment{
    [commentsTableViewControllerDelegate hideHUD];
    if(success){
        mSendBtn.userInteractionEnabled = YES;
        mUserComment.text = @"";
        
        if ([mUserComment isFirstResponder])
            [mUserComment resignFirstResponder];
        
        [mDataSource addObject:comment];
        [mTableView reloadData];

        mTableView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - 35);
        mCommentView.frame = CGRectMake(0, self.view.frame.size.height-35, 320, 35);
        mUserComment.frame = CGRectMake(10, 5, 220, 25);

        [mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[mDataSource count]-1 inSection:0]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
    }
    else{
        //warning posting failed
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Comments Failed"
                              message:@"Please retry later after reload"
                              delegate: nil
                              cancelButtonTitle:@"Continue"
                              otherButtonTitles:nil];
        [alert show];
    }
}


- (void) returnCommentLikeDislikeComplete:(BOOL)success CommentId:(NSNumber *)commentId{
    [mDataSource removeAllObjects];
    [SharedNetworkManager fetchNewComments:mPostId sender:self];
}


#pragma mark - CommentsViewCellDelegate
- (void) likeOrDislikeComment:(NSNumber*)commentId isLike:(BOOL)isLike{
    [commentsTableViewControllerDelegate displayHUD];
    [SharedNetworkManager likeOrDislikeComment:commentId
                                        isLike:isLike
                                        sender:self];
}







@end
