//
//  CommentsViewCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 9. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "CommentsViewCell.h"

@implementation CommentsViewCell

@synthesize commentsViewCellDelegate;
@synthesize commentId;

#define LIKE 101
#define DISLIKE 102

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        username = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, 150, 15)];
        username.font = [UIFont fontWithName:@"AudimatMonoShort" size:13.f];
        username.textColor = [UIColor darkGrayColor];
        //username.backgroundColor = [UIColor greenColor];
        
        comment = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 245, 20)];
        comment.font = [UIFont fontWithName:@"AudimatMonoShort" size:13.f];
        comment.textColor = [UIColor lightGrayColor];
        //comment.backgroundColor = [UIColor blueColor];
        comment.lineBreakMode = UILineBreakModeWordWrap;
        comment.numberOfLines = 0;
        //comment.contentInset = UIEdgeInsetsMake(-4,-8,0,0);
        comment.textAlignment = UITextAlignmentLeft;
        
        [self addSubview:username];
        [self addSubview:comment];
        
       
        timeDiff = [[UILabel alloc] initWithFrame:CGRectMake(180, 5, 125, 15)];
        timeDiff.font = [UIFont fontWithName:@"AudimatMonoShort" size:10.f];
        timeDiff.textColor = [UIColor grayColor];
        timeDiff.textAlignment = UITextAlignmentRight;
        
        [self addSubview:timeDiff];
        

    }
    return self;
}

- (void) layoutSubviews{
    [super layoutSubviews];
    self.textLabel.hidden = YES;
    self.imageView.frame = CGRectMake(15, 5, 40, 40);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) setUsername:(NSString*)inputUsername{
    username.text = inputUsername;
}

- (void) setComment:(NSString*)inputComment{
    CGSize newSize = [inputComment sizeWithFont:comment.font
                              constrainedToSize:CGSizeMake(comment.frame.size.width,400)
                                  lineBreakMode:NSLineBreakByWordWrapping];
    

    comment.text = inputComment;
    comment.frame = CGRectMake(60, 20, 245, newSize.height + 5);

}

- (void) setTimeDiff:(NSString*)inputTimeDiff{
    timeDiff.text = inputTimeDiff;
}

+ (CGFloat) calcCellHeight:(NSString*)comment{
    CGSize newSize = [comment sizeWithFont:[UIFont fontWithName:@"AudimatMonoShort" size:13.f]
                              constrainedToSize:CGSizeMake(245,400)
                                  lineBreakMode:NSLineBreakByWordWrapping];
    
    return MAX(50, 20 + newSize.height + 10 + 15);
}

- (void) setLikes:(int)numLikes Dislikes:(int)numDislikes{
    
    int cellHeight = [CommentsViewCell calcCellHeight:comment.text];
    
    if(!likes){
        likes = [[UILabel alloc] initWithFrame:CGRectZero];
        likes.font = [UIFont fontWithName:@"AudimatMonoShort" size:10.f];
        likes.textColor = [UIColor colorWithRed:0.3 green:0.6 blue:0.9 alpha:1.0f];
        likes.tag = LIKE;
        likes.userInteractionEnabled = YES;
        [self addSubview:likes];
    }
    
    if(!dislikes){
        dislikes = [[UILabel alloc] initWithFrame:CGRectZero];
        dislikes.font = [UIFont fontWithName:@"AudimatMonoShort" size:10.f];
        dislikes.textColor = [UIColor colorWithRed:0.9 green:0.3 blue:0.3 alpha:1.0f];
        dislikes.tag = DISLIKE;
        dislikes.userInteractionEnabled = YES;
        [self addSubview:dislikes];
    }
    
    likes.text = [NSString stringWithFormat:@"%d likes", numLikes];
    dislikes.text = [NSString stringWithFormat:@"%d dislikes", numDislikes];
    
    CGSize size = [dislikes.text sizeWithFont:dislikes.font];
    dislikes.frame = CGRectMake(305-size.width, cellHeight - 15, size.width, 15);
    
    size = [likes.text sizeWithFont:likes.font];
    likes.frame = CGRectMake(240-size.width, cellHeight - 15, size.width, 15);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    int tag = [[[touches anyObject] view] tag];
    switch (tag) {
            
        case LIKE:
            [commentsViewCellDelegate likeOrDislikeComment:commentId isLike:YES];
            break;
        
        case DISLIKE:
            [commentsViewCellDelegate likeOrDislikeComment:commentId isLike:NO];
            break;
            
        default:
            break;
    }
}

@end
/*
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.textLabel.font = [UIFont fontWithName:@"AudimatMonoLight" size:19.f];
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        
        self.imageView.image = [UIImage imageNamed:@"category_not_selected.png"];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(20, 0, 200, 29);
    self.imageView.frame = CGRectMake(20, 8, 13, 13);
}
*/

