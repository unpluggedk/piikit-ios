//
//  DetailBarView.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 24..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBarView : UIView{
    NSString *mLabelText;
    UIView *mFillbar;
    
    UIView *mDisablebar;
    
    CGFloat minFillSize;
    int side;
    
    UILabel *mLeftScore;
    UILabel *mRightScore;
    BOOL isSingle;
}

@property (nonatomic, assign) BOOL isSingle;

- (id) initWithFrame:(CGRect)frame label:(NSString*)label;

- (void) setBarInitOnLeft;
- (void) setBarInitOnRight;

- (void) setBarVisibleScoreLeft:(int)leftScore right:(int)rightScore;
- (void) setEnable:(BOOL)enable;

@end
