//
//  DetailBarView.m
//  Piikit
//
//  Created by Jason Kim on 13. 8. 24..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "DetailBarView.h"


@implementation DetailBarView

@synthesize isSingle;
- (id) initWithFrame:(CGRect)frame label:(NSString*)label{
    mLabelText = label;
    return [self initWithFrame:frame];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];

        minFillSize = frame.size.width / 20.;
        
        mFillbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, minFillSize, frame.size.height)];
        mFillbar.backgroundColor = [UIColor redColor];
        [self addSubview:mFillbar];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        label.text = mLabelText;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"AudimatMonoBold" size:12.f];
        label.backgroundColor = [UIColor clearColor];
        [label sizeToFit];
        label.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        [self addSubview:label];
        
        mLeftScore = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width/2, frame.size.height)];
        mLeftScore.text = @"";
        mLeftScore.font = [UIFont fontWithName:@"AudimatMonoBold" size:12.f];
        mLeftScore.backgroundColor = [UIColor clearColor];
        mLeftScore.textAlignment = NSTextAlignmentLeft;
        [self addSubview:mLeftScore];
        
        mRightScore = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width/2, 0, frame.size.width/2, frame.size.height)];
        mRightScore.text = @"";
        mRightScore.font = [UIFont fontWithName:@"AudimatMonoBold" size:12.f];
        mRightScore.backgroundColor = [UIColor clearColor];
        mRightScore.textAlignment = NSTextAlignmentRight;
        [self addSubview:mRightScore];
        
        mFillbar.hidden = YES;
        side = 0;

        mDisablebar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [mDisablebar setAlpha:0.7];
        [mDisablebar setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:mDisablebar];
        
        mDisablebar.hidden = YES;
    }
    return self;
}

- (void) setBarInitOnLeft{
    mFillbar.frame = CGRectMake(0, 0, minFillSize, self.frame.size.height);
    side = 0;
    mFillbar.hidden = NO;
}
- (void) setBarInitOnRight{
    mFillbar.frame = CGRectMake(self.frame.size.width - minFillSize, 0, minFillSize, self.frame.size.height);
    side = 1;
    mFillbar.hidden = NO;
}


- (void) setBarVisibleScoreLeft:(int)leftScore right:(int)rightScore{
    // side
    //     0 : left
    //     1 : right

    mLeftScore.text = [NSString stringWithFormat:@"%d", leftScore];
    if(!isSingle)
        mRightScore.text = [NSString stringWithFormat:@"%d", rightScore];
    
    if(side == 0){
        if(leftScore == rightScore)
            mFillbar.frame = CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height);
        else{
            CGFloat dividePercent = (CGFloat)leftScore / (CGFloat)(leftScore + rightScore);
            dividePercent = MIN(dividePercent, 0.95);
            dividePercent = MAX(dividePercent, 0.05);
            CGFloat dividePoint = self.frame.size.width * dividePercent;
            mFillbar.frame = CGRectMake(0, 0, dividePoint, self.frame.size.height);
        }

    }
    else{
        if(leftScore == rightScore)
            mFillbar.frame = CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height);

        else{
            CGFloat dividePercent = (CGFloat)rightScore / (CGFloat)(leftScore + rightScore);
            dividePercent = MIN(dividePercent, 0.95);
            dividePercent = MAX(dividePercent, 0.05);            
            CGFloat dividePoint = self.frame.size.width * (1. - dividePercent);
            mFillbar.frame = CGRectMake(dividePoint, 0, self.frame.size.width * dividePercent, self.frame.size.height);
        }

    }
    

    
    
}

- (void) setEnable:(BOOL)enable{
    if(enable)
        mDisablebar.hidden = YES;
    else
        mDisablebar.hidden = NO;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
