//
//  CommentsViewCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 9. 3..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CommentsViewCellDelegate <NSObject>
- (void) likeOrDislikeComment:(NSNumber*)commentId isLike:(BOOL)isLike;

@end


@interface CommentsViewCell : UITableViewCell{
    UILabel *username;
    UILabel *comment;
    
    UILabel *timeDiff;
    
    UILabel *likes;
    UILabel *dislikes;
    
    NSNumber *commentId;
}

+ (CGFloat) calcCellHeight:(NSString*)comment;

- (void) setUsername:(NSString*)inputUsername;
- (void) setComment:(NSString*)inputComment;
- (void) setTimeDiff:(NSString*)inputTimeDiff;

- (void) setLikes:(int)numLikes Dislikes:(int)numDislikes;

@property (nonatomic, retain) NSNumber *commentId;
@property (assign) id<CommentsViewCellDelegate> commentsViewCellDelegate;



@end
