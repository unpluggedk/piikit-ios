//
//  DetailViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 7. 28..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailBarView.h"
#import "UICountingLabel.h"
#import "PostingItem.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"

#import "CommentsTableViewController.h"

#import "Vote.h"

#import "Enums.h"
#import "AppScrollView.h"

@protocol DetailViewControllerDelegate <NSObject>
- (void) returnVote:(Vote*)vote atRow:(int)row;
@end




@interface DetailViewController : UIViewController <UITextViewDelegate, NetworkDelegate, CommentsTableViewControllerDelegate, UIAlertViewDelegate>{
    BOOL isImageFullScreen;
    
    MBProgressHUD *HUD;
    BOOL isFirstLoading;
    
    UILabel *mLeftLabel;
    UILabel *mCenterLabel;
    UILabel *mRightLabel;
    
    // view related
    UILabel *username;
    UIImageView *profile;
    UILabel *date;
    
    UIView *productView;
    UIView *botline; //line below images
    UILabel *leftProductLabel;
    UILabel *rightProductLabel;
    
    UIButton *leftImageButton;   
    UIButton *rightImageButton;
    
    UIImageView *leftVoteImageView;
    UIImageView *rightVoteImageView;
    
    UITextView *title;
    UITextView *description;
    
    UIView *blackDivider;
    
    DetailBarView *barMale;
    DetailBarView *barFemale;
    DetailBarView *barUnder21;
    DetailBarView *bar21to30;
    DetailBarView *barOver30;
    
    UICountingLabel *leftCountingLabel;
    UICountingLabel *rightCountingLabel;
    
    NSNumber *postId;
    PostingItem *item;
    
    CGRect leftFull;
    CGRect leftHalf;
    
    BOOL isBookmarked;
    int row;
    
    VoteSide voteSide;
    UILabel *commentsLabel;
    UILabel *shareLabel;
    UIView *resultsView;
    
    UILabel *leftLikeLabel;
    UILabel *rightLikeLabel;
    
    AppScrollView *scrollView;

    // temporary holder for expand label
    UILabel *expandLabel;
    
    UIButton *fullScreenButton;

    UIImage *maximizeImage;
    UIImage *minimizeImage;
    
    BOOL isDetailAdded;
    BOOL isDetailExpanded;
    
    NSAttributedString *showString;
    NSAttributedString *hideString;
}

@property (assign) id<DetailViewControllerDelegate> detailViewControllerDelegate;

//@property (nonatomic, retain) UIImageView *leftImageView;
//@property (nonatomic, retain) UIImageView *rightImageView;

@property (nonatomic, retain) UIButton *leftImageButton;
@property (nonatomic, retain) UIButton *rightImageButton;

- (id)initWithPostid:(NSNumber*)loadPostId atRow:(int)indexPathRow;

@end
