//
//  CommentsTableViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 24..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "CommentsViewCell.h"

@protocol CommentsTableViewControllerDelegate <NSObject>
- (void)reportNumComments:(int)numComments;
- (void)displayHUD;
- (void)hideHUD;

@end



@interface CommentsTableViewController: UIViewController <UITableViewDelegate, UITableViewDataSource, NetworkDelegate, UITextViewDelegate, CommentsViewCellDelegate>{
//@interface CommentsTableViewController : UITableViewController<NetworkDelegate>{
    NSNumber *mPostId;
    NSMutableArray *mDataSource;
    NSMutableArray *mCellHeight;
    
    UITableView *mTableView;
    
    UIView *mCommentView;
    UITextView *mUserComment;
    UIButton *mSendBtn;
    
    BOOL freshlyLoaded;
    
    CGRect tableViewSizeOriginal;
}

@property (assign) id<CommentsTableViewControllerDelegate> commentsTableViewControllerDelegate;

- (id)initWithPostId:(NSNumber*)postId;

@end
