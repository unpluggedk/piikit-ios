//
//  DetailViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 7. 28..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "DetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>

#import "UIImageView+WebCache.h"

#import "Profile.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize leftImageButton, rightImageButton;
@synthesize detailViewControllerDelegate;

#define LEFT 201
#define CENTER 202
#define RIGHT 203

#define COMMENTS 204
#define SHARE 205

#define LEFTIMAGE 206
#define RIGHTIMAGE 207

#define FULLSCREEN 208

#define EXPAND 209

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPostid:(NSNumber*)loadPostId  atRow:(int)indexPathRow{
    postId = loadPostId;
    row = indexPathRow;
    isImageFullScreen = NO;
    isFirstLoading = YES;
    
    return [super init];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
	// Do any additional setup after loading the view.
    maximizeImage = [UIImage imageNamed:@"fullscreen-icon.png"];
    minimizeImage = [UIImage imageNamed:@"fullscreen-close-icon.png"];
    
    
    // ios7
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0){
        scrollView = [[AppScrollView alloc] initWithFrame:CGRectMake(0, 30, 320, self.view.frame.size.height - 30)];
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height - 20 - 30);
        
        
    }
    else{
        scrollView = [[AppScrollView alloc] initWithFrame:CGRectMake(0, 30, 320, self.view.frame.size.height - 30)];
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height - 30);
    }
    
    [self.view addSubview:scrollView];
    
    int tabBarY = 0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        tabBarY = 20;
    
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(0, tabBarY, 320,30)];
    tabBar.backgroundColor = [UIColor blackColor];
    tabBar.layer.borderColor = [UIColor whiteColor].CGColor;
    tabBar.layer.borderWidth = 1.f;
    
    mLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 107, 30)];
    mLeftLabel.backgroundColor = [UIColor blackColor];
    mLeftLabel.textAlignment = NSTextAlignmentCenter;
    mLeftLabel.attributedText = [self getTabbarString:@"BACK"];
    mLeftLabel.tag = LEFT;
    mLeftLabel.textColor = [UIColor whiteColor];
    mLeftLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mLeftLabel];
    
    mCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(107, 0, 107, 30)];
    mCenterLabel.backgroundColor = [UIColor blackColor];
    mCenterLabel.textAlignment = NSTextAlignmentCenter;
    mCenterLabel.attributedText = [self getTabbarString:@"BOOKMARK"];
    mCenterLabel.tag = CENTER;
    mCenterLabel.userInteractionEnabled = YES;
    mCenterLabel.textColor = [UIColor grayColor];
    [tabBar addSubview:mCenterLabel];
    
    mRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(213, 0, 107, 30)];
    mRightLabel.backgroundColor = [UIColor blackColor];
    mRightLabel.textAlignment = NSTextAlignmentCenter;
    mRightLabel.attributedText = [self getTabbarString:@""];
    mRightLabel.tag = RIGHT;
    mRightLabel.userInteractionEnabled = YES;
    [tabBar addSubview:mRightLabel];
    
    UIView *line;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(107, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(213, 0, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [tabBar addSubview:line];
    
    isBookmarked = NO;
    
    [self.view addSubview:tabBar];
    
    scrollView.backgroundColor = [UIColor whiteColor];
    
    [self setupView];
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Loading";
    HUD.userInteractionEnabled = NO;
    
    [HUD show:YES];
    [SharedNetworkManager fetchSinglePosting:postId sender:self];
    

    
}

- (NSAttributedString *)getTabbarString:(NSString*)str{
    UIFont *tabBarFont = [UIFont fontWithName:@"AudimatMono" size:12.f];
    NSNumber *linespace = [NSNumber numberWithFloat:0.7f];
    
    NSMutableAttributedString *out = [[NSMutableAttributedString alloc] initWithString:str];
    [out addAttribute:NSFontAttributeName value:tabBarFont range:NSMakeRange(0, out.length)];
    [out addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, out.length)];
    [out addAttribute:(NSString*)kCTKernAttributeName value:linespace range:NSMakeRange(0, out.length)];
    
    return out;
}

- (void) setupView{
    isDetailAdded = NO;
    isDetailExpanded = NO;
    
    CGFloat relativeY = 10.0;
    
    profile = [[UIImageView alloc] initWithFrame:CGRectMake(15, relativeY, 53, 50)];
    [scrollView addSubview:profile];
    
    username = [[UILabel alloc] initWithFrame:CGRectMake(72, relativeY, 320-15-80, 17)];
    username.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:username];
    
    date = [[UILabel alloc] initWithFrame:CGRectMake(235, relativeY, 70, 20)];
    [scrollView addSubview:date];
    
//    int titleStart = 15 + 53 + 15;
    
    title = [[UITextView alloc] initWithFrame:CGRectMake(65, relativeY + 12, 320-65, 40)];
    title.font = [UIFont fontWithName:@"BebasNeueSmallLinespacing" size:17.f];
    title.text = @"";
    title.userInteractionEnabled = NO;
    //title.delegate = self;
    title.textColor = [UIColor blackColor];
    title.backgroundColor = [UIColor clearColor];
  
    [scrollView addSubview:title];
    
    relativeY += 54;
    
    // temporary holder for expand label
    /*
    UILabel *expandLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, relativeY, 150, 30)];
    expandLabel.textColor = [UIColor blueColor];
    NSMutableAttributedString *expandString = [[NSMutableAttributedString alloc] initWithString:@"expand"];
    [expandString addAttribute:NSFontAttributeName
                         value:[UIFont fontWithName:@"AudimatMonoShort" size:15.f]
                         range:NSMakeRange(0, 5)];
    [expandString addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blueColor]
                         range:NSMakeRange(0, 6)];
    [expandString addAttribute:NSUnderlineStyleAttributeName
                         value:[NSNumber numberWithInt:1] range:NSMakeRange(0, 6)];
    expandLabel.attributedText = expandString;
    [scrollView addSubview:expandLabel];
    relativeY += 30;
    */
    productView = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 174+20)];
    
    leftImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftImageButton.frame = CGRectMake(0, 0, 157, 174);
    leftImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    leftImageButton.clipsToBounds = YES;
    leftImageButton.layer.borderWidth = 0.7f;
    
    [leftImageButton addTarget:self action:@selector(voteLeft) forControlEvents:UIControlEventTouchUpInside];
    
    leftHalf = leftImageButton.frame;
    leftFull = CGRectMake(0, 0, 320, 174);
    
    rightImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightImageButton.frame = CGRectMake(157+6, 0, 157, 174);
    rightImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    rightImageButton.clipsToBounds = YES;
    rightImageButton.layer.borderWidth = 0.7f;

    [rightImageButton addTarget:self action:@selector(voteRight) forControlEvents:UIControlEventTouchUpInside];
    
    leftVoteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 50)];
    leftVoteImageView.image = [UIImage imageNamed:@"likebutton_p_red.png"];
    [leftImageButton addSubview:leftVoteImageView];
    leftVoteImageView.hidden = YES;

    rightVoteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 50)];
    rightVoteImageView.image = [UIImage imageNamed:@"likebutton_p_red.png"];
    [rightImageButton addSubview:rightVoteImageView];
    rightVoteImageView.hidden = YES;
    
    blackDivider = [[UIView alloc] initWithFrame:CGRectMake(157, 0, 6, 174)];
    blackDivider.backgroundColor = [UIColor blackColor];
    [productView addSubview:blackDivider];
    
    
    relativeY += 174;
    
    //botline = [[UIView alloc] initWithFrame:CGRectMake(0, 174, 320, 1)];
    //botline.backgroundColor = [UIColor blackColor];
    relativeY += 1;
    
    leftProductLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 175, 140, 20)];
    leftProductLabel.textColor = [UIColor grayColor];
    leftProductLabel.backgroundColor = [UIColor clearColor];
    leftProductLabel.text = @"product name a";
    leftProductLabel.font = [UIFont fontWithName:@"AudimatMonoShort" size:12.f];
    leftProductLabel.numberOfLines = 0;
    leftProductLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    rightProductLabel = [[UILabel alloc] initWithFrame:CGRectMake(160+10, 175, 140, 20)];
    rightProductLabel.textColor = [UIColor grayColor];
    rightProductLabel.backgroundColor = [UIColor clearColor];
    rightProductLabel.text = @"product name b";
    rightProductLabel.font = [UIFont fontWithName:@"AudimatMonoShort" size:12.f];
    rightProductLabel.numberOfLines = 0;
    rightProductLabel.lineBreakMode = UILineBreakModeWordWrap;

    
    [productView addSubview:leftProductLabel];
    [productView addSubview:rightProductLabel];
    
    //[productView addSubview:botline];
    
    [productView addSubview:rightImageButton];
    [productView addSubview:leftImageButton];
    //[productView addSubview:line];
    //productView.layer.borderWidth = 1.f;
    
    
    relativeY += 27;
    
    resultsView = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 116)];
    resultsView.backgroundColor = [UIColor blackColor];

    int barY = 14;
    barMale = [[DetailBarView alloc] initWithFrame:CGRectMake(85, barY, 150, 12) label:@"MALE:"];

    barY += 12 + 7;
    barFemale = [[DetailBarView alloc] initWithFrame:CGRectMake(85, barY, 150, 12) label:@"FEMALE:"];

    barY += 12 + 7;
    barUnder21 = [[DetailBarView alloc] initWithFrame:CGRectMake(85, barY, 150, 12) label:@"UNDER 21:"];

    barY += 12 + 7;
    bar21to30 = [[DetailBarView alloc] initWithFrame:CGRectMake(85, barY, 150, 12) label:@"21-30's:"];

    barY += 12 + 7;
    barOver30 = [[DetailBarView alloc] initWithFrame:CGRectMake(85, barY, 150, 12) label:@"OVER 30:"];

    [resultsView addSubview:barMale];
    [resultsView addSubview:barFemale];
    [resultsView addSubview:barUnder21];
    [resultsView addSubview:bar21to30];
    [resultsView addSubview:barOver30];
    
    [scrollView addSubview:resultsView];
    
    
    leftCountingLabel = [[UICountingLabel alloc] initWithFrame:CGRectMake(0, 0, 85, 116)];
    leftCountingLabel.textAlignment = NSTextAlignmentCenter;
    
    leftCountingLabel.font = [UIFont fontWithName:@"BebasNeue" size:40.f];
    leftCountingLabel.text = @"0";
    leftCountingLabel.textColor = [UIColor whiteColor];
    leftCountingLabel.backgroundColor = [UIColor clearColor];
    leftCountingLabel.format = @"%d";
//    leftCountingLabel.method = UILabelCountingMethodLinear;
    
    leftLikeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, 85, 30)];
    leftLikeLabel.font = [UIFont fontWithName:@"BebasNeue" size:25.f];
    leftLikeLabel.text = @"LIKES";
    leftLikeLabel.textColor = [UIColor redColor];
    leftLikeLabel.backgroundColor = [UIColor clearColor];
    leftLikeLabel.textAlignment = NSTextAlignmentCenter;
    [resultsView addSubview:leftLikeLabel];
    

    rightLikeLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-80, 80, 85, 30)];
    rightLikeLabel.font = [UIFont fontWithName:@"BebasNeue" size:25.f];
    rightLikeLabel.text = @"LIKES";
    rightLikeLabel.textColor = [UIColor redColor];
    rightLikeLabel.backgroundColor = [UIColor clearColor];
    rightLikeLabel.textAlignment = NSTextAlignmentCenter;
    [resultsView addSubview:rightLikeLabel];
    
    rightCountingLabel = [[UICountingLabel alloc] initWithFrame:CGRectMake(320-85, 0, 85, 116)];
    rightCountingLabel.textAlignment = NSTextAlignmentCenter;
    
    rightCountingLabel.font = [UIFont fontWithName:@"BebasNeue" size:40.f];
    rightCountingLabel.text = @"0";
    rightCountingLabel.textColor = [UIColor redColor];
    rightCountingLabel.backgroundColor = [UIColor clearColor];
    rightCountingLabel.format = @"%d";

    [resultsView addSubview:leftCountingLabel];
    [resultsView addSubview:rightCountingLabel];
    
    CGFloat bottom = scrollView.contentSize.height;//.frame.size.height;
    
    commentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, bottom - 30, 150, 30)];
    commentsLabel.font = [UIFont fontWithName:@"AudimatMono" size:15.f];
    commentsLabel.text = @"Comments";
    commentsLabel.textAlignment = NSTextAlignmentLeft;
    commentsLabel.textColor = [UIColor grayColor];
    commentsLabel.tag = COMMENTS;
    commentsLabel.userInteractionEnabled = YES;
    [scrollView addSubview:commentsLabel];
    
    shareLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-15-150, bottom - 30, 150, 30)];
    shareLabel.font = [UIFont fontWithName:@"AudimatMono" size:15.f];
    shareLabel.text = @"Share";
    shareLabel.textAlignment = NSTextAlignmentRight;
    shareLabel.textColor = [UIColor grayColor];
    shareLabel.tag = SHARE;
    shareLabel.userInteractionEnabled = YES;
    shareLabel.hidden = YES;
    
    [scrollView addSubview:shareLabel];
    
    voteSide = NotVoted;
    
    [scrollView addSubview:productView];
    
    fullScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fullScreenButton.frame = CGRectMake(320-40, 0, 40, 40);
    [fullScreenButton setImage:maximizeImage forState:UIControlStateNormal];
    fullScreenButton.alpha = 0.7;
    [fullScreenButton addTarget:self action:@selector(fullScreenPressed) forControlEvents:UIControlEventTouchUpInside];
    fullScreenButton.hidden = YES;
    [productView addSubview:fullScreenButton];
    
    [self loadPlaceholder];
    
}

- (void) loadPlaceholder{
    profile.image = [UIImage imageNamed:@"Profile.png"];
    [self setUserNameString:@"PIIKIT"];
    
    NSDate *curDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy h:mma"];
        
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:curDate];
    
    [self setDateStr:formattedDateString];
    
    //leftImageButton.image = [UIImage imageNamed:@"post_placeholder_small.png"];
    //rightImageButton.image = [UIImage imageNamed:@"post_placeholder_small.png"];
    
    [leftImageButton setImage:[UIImage imageNamed:@"post_placeholder_small.png"] forState:UIControlStateNormal];
    [rightImageButton setImage:[UIImage imageNamed:@"post_placeholder_small.png"] forState:UIControlStateNormal];
}

- (void)setLeftProductLabel:(NSString*)productLabel{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:productLabel];
    
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-0.7f] range:NSMakeRange(0, productLabel.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoShort" size:12.f] range:NSMakeRange(0, productLabel.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, productLabel.length)];
    
    leftProductLabel.attributedText = outstr;
}

- (void)setRightProductLabel:(NSString*)productLabel{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:productLabel];
    
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-0.7f] range:NSMakeRange(0, productLabel.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoShort" size:12.f] range:NSMakeRange(0, productLabel.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, productLabel.length)];
    
    rightProductLabel.attributedText = outstr;
}



- (void)setUserNameString:(NSString*)usernameStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@asks,",[usernameStr uppercaseString]]];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, outstr.string.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:.5f] range:NSMakeRange(0, usernameStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.f] range:NSMakeRange(usernameStr.length-1, 1)];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BebasNeue" size:15.f] range:NSMakeRange(0, usernameStr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(usernameStr.length, 5)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:10.f] range:NSMakeRange(usernameStr.length, 5)];

    username.attributedText = outstr;
}

- (void)setDateStr:(NSString*)dateStr{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:dateStr];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, dateStr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-1.f] range:NSMakeRange(0, dateStr.length)];
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:10.f] range:NSMakeRange(0, dateStr.length)];
    
    date.textAlignment = NSTextAlignmentRight;
    date.attributedText = outstr;
    
    //date.frame = CGRectMake(date.frame.origin.x, date.frame.origin.y, outstr.size.width, outstr.size.height);
    
    
}

- (BOOL) checkLogin{
    if([SharedProfile isLoginSkipped]){
        NSLog(@"login skipped!");
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"PIIKIT"
                              message:@"PIIKIT needs you to login.\nDo you want to Join or Login?"
                              delegate: self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"Yes", @"No", nil];
        [alert show];
        return NO;
    }
    return YES;
}


- (void) voteLeft{
    if(![self checkLogin]){
        return;
    }
    Vote *vote = [[Vote alloc] init];
    vote.time = [NSDate date];
    vote.itemId = postId;
    
    if(voteSide == NotVoted){
        voteSide = VoteLeft;
        vote.isVote = YES;
        vote.isLeft = YES;
        leftVoteImageView.hidden = NO;
        [SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
    }
    else if(voteSide == VoteLeft){
        voteSide = NotVoted;
        vote.isVote = NO;
        vote.isLeft = YES;
        leftVoteImageView.hidden = YES;
        [SharedNetworkManager unvotePostID:item.mItemID sender:self];
    }
    else if(voteSide == VoteRight){
        voteSide = VoteLeft;
        vote.isVote = YES;
        vote.isLeft = YES;
        leftVoteImageView.hidden = NO;
        rightVoteImageView.hidden = YES;
        [SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
    }
    
    //[SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
    if(detailViewControllerDelegate)
        [detailViewControllerDelegate returnVote:vote atRow:row];
    else
        NSLog(@"no delegate");

}


- (void) voteRight{
    if(![self checkLogin]){
        return;
    }
    
    Vote *vote = [[Vote alloc] init];
    vote.time = [NSDate date];
    vote.itemId = postId;
    
    if(voteSide == NotVoted){
        voteSide = VoteRight;
        vote.isVote = YES;
        vote.isLeft = NO;
        rightVoteImageView.hidden = NO;
        [SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
    }
    else if(voteSide == VoteRight){
        voteSide = NotVoted;
        vote.isLeft = NO;
        vote.isVote = NO;
        rightVoteImageView.hidden = YES;
        [SharedNetworkManager unvotePostID:item.mItemID  sender:self];
    }
    else if(voteSide == VoteLeft){
        voteSide = VoteRight;
        vote.isVote = YES;
        vote.isLeft = NO;
        leftVoteImageView.hidden = YES;
        rightVoteImageView.hidden = NO;
        [SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
    }
    //[SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
    
    if(detailViewControllerDelegate)
        [detailViewControllerDelegate returnVote:vote atRow:row];
    else
        NSLog(@"no delegate");
    
}

- (void)fullScreenPressed{
    // original: 174
    [UIView animateWithDuration:0.5
                     animations:^{
                         int offset = 365 - 174;
                         productView.frame = CGRectMake(productView.frame.origin.x, productView.frame.origin.y,
                                                        productView.frame.size.width, productView.frame.size.height + offset);
                         leftImageButton.frame = CGRectMake(leftImageButton.frame.origin.x, leftImageButton.frame.origin.y,
                                                            leftImageButton.frame.size.width, 365);
                         leftProductLabel.frame = CGRectOffset(leftProductLabel.frame, 0, offset);
                         
                         botline.frame = CGRectOffset(botline.frame, 0, offset);
                         
                         resultsView.frame = CGRectOffset(resultsView.frame, 0, offset);
                         commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, offset);
                         shareLabel.frame = CGRectOffset(shareLabel.frame, 0, offset);
                         
                         
                         scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                                             scrollView.contentSize.height + offset);
                         
                         
                         ;
                     } completion:^(BOOL finished) {
                         [fullScreenButton setImage:minimizeImage forState:UIControlStateNormal];
                         [fullScreenButton removeTarget:self action:@selector(fullScreenPressed) forControlEvents:UIControlEventTouchUpInside];
                         [fullScreenButton addTarget:self action:@selector(minimizeScreenPressed) forControlEvents:UIControlEventTouchUpInside];

                     }];
    
    
    
}

- (void)minimizeScreenPressed{
    [UIView animateWithDuration:0.5
                     animations:^{
                         int offset = 365 - 174;
                         productView.frame = CGRectMake(productView.frame.origin.x, productView.frame.origin.y,
                                                        productView.frame.size.width, productView.frame.size.height - offset);
                         leftImageButton.frame = CGRectMake(leftImageButton.frame.origin.x, leftImageButton.frame.origin.y,
                                                            leftImageButton.frame.size.width, 174);
                         leftProductLabel.frame = CGRectOffset(leftProductLabel.frame, 0, -offset);
                         
                         botline.frame = CGRectOffset(botline.frame, 0, -offset);
                         
                         resultsView.frame = CGRectOffset(resultsView.frame, 0, -offset);
                         commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, -offset);
                         shareLabel.frame = CGRectOffset(shareLabel.frame, 0, -offset);
                         
                         
                         scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                                             scrollView.contentSize.height - offset);
                         
                         
                         ;
                     } completion:^(BOOL finished) {
                         [fullScreenButton setImage:maximizeImage forState:UIControlStateNormal];
                         [fullScreenButton removeTarget:self action:@selector(minimizeScreenPressed) forControlEvents:UIControlEventTouchUpInside];
                         [fullScreenButton addTarget:self action:@selector(fullScreenPressed) forControlEvents:UIControlEventTouchUpInside];
                         
                     }];
    
    
    
    
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (![HUD isUserInteractionEnabled]) {
        return;
    }
    
    int tag = [[[touches anyObject] view] tag];
    switch (tag) {
       /*
        case FULLSCREEN:{
            filter.hidden = NO;
            
            if(!isImageFullScreen){
                fullScreenIcon.hidden = YES;
                
                [UIView animateWithDuration:0.3
                                 animations:^{
                                     productView.frame = productLargeFrame;// CGRectMake(20, 40, 280, self.view.frame.size.height - 40);
                                     leftImageButton.frame = leftImageLargeFrame;// CGRectMake(0, 0, 280, self.view.frame.size.height - 40);
                                 } completion:^(BOOL finished) {
                                     isImageFullScreen = YES;
                                     leftImageButton.contentMode = UIViewContentModeScaleAspectFit;
                                 }];
            }
            break;
        }
        */
        case LEFT:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case CENTER:{ // Bookmark
            [HUD show:YES];
            if(isBookmarked){
                isBookmarked = NO;
                [SharedNetworkManager removeBookmarkPostID:item.mItemID sender:self];
                //mCenterLabel.textColor = [UIColor grayColor];
            }
            else{
                isBookmarked = YES;
                [SharedNetworkManager bookmarkPostID:item.mItemID sender:self];
                //mCenterLabel.textColor = [UIColor whiteColor];
            }
            
            
            break;
        }
        case RIGHT:
            break;
            
            /*
        case LEFTIMAGE:{
            Vote *vote = [[Vote alloc] init];
            vote.time = [NSDate date];
            vote.itemId = postId;
            
            if(voteSide == NotVoted){
                voteSide = VoteLeft;
                vote.isVote = YES;
                vote.isLeft = YES;
                leftVoteImageView.hidden = NO;
                [SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
            }
            else if(voteSide == VoteLeft){
                voteSide = NotVoted;
                vote.isVote = NO;
                vote.isLeft = YES;
                leftVoteImageView.hidden = YES;
                [SharedNetworkManager unvotePostID:item.mItemID sender:self];
            }
            else if(voteSide == VoteRight){
                voteSide = VoteLeft;
                vote.isVote = YES;
                vote.isLeft = YES;
                leftVoteImageView.hidden = NO;
                rightVoteImageView.hidden = YES;
                [SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
            }
            
            //[SharedNetworkManager votePostID:item.mItemID left:YES sender:self];
            if(detailViewControllerDelegate)
                [detailViewControllerDelegate returnVote:vote atRow:row];
            else
                NSLog(@"no delegate");
            break;
        }
        
        case RIGHTIMAGE:{
            Vote *vote = [[Vote alloc] init];
            vote.time = [NSDate date];
            vote.itemId = postId;
            
            if(voteSide == NotVoted){
                voteSide = VoteRight;
                vote.isVote = YES;
                vote.isLeft = NO;
                rightVoteImageView.hidden = NO;
                [SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
            }
            else if(voteSide == VoteRight){
                voteSide = NotVoted;
                vote.isLeft = NO;
                vote.isVote = NO;
                rightVoteImageView.hidden = YES;
                [SharedNetworkManager unvotePostID:item.mItemID  sender:self];
            }
            else if(voteSide == VoteLeft){
                voteSide = VoteRight;
                vote.isVote = YES;
                vote.isLeft = NO;
                leftVoteImageView.hidden = YES;
                rightVoteImageView.hidden = NO;
                [SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
            }
            //[SharedNetworkManager votePostID:item.mItemID left:NO sender:self];
            
            if(detailViewControllerDelegate)
                [detailViewControllerDelegate returnVote:vote atRow:row];
            else
                NSLog(@"no delegate");
            break;
        }
            */
        case COMMENTS:{
            if(![self checkLogin]){
                break;
            }
            CommentsTableViewController *commentsTableVC = [[CommentsTableViewController alloc] initWithPostId:item.mItemID];
            commentsTableVC.commentsTableViewControllerDelegate = self;
            [self.navigationController pushViewController:commentsTableVC animated:YES];
            
            break;
        }
        case SHARE:
            break;
            
        case EXPAND:
            if(isDetailExpanded)
                [self descriptionShow:NO];
            else
                [self descriptionShow:YES];
            
            isDetailExpanded = !isDetailExpanded;
            break;
            
        default:{

            break;
            
            
            
        }
    }
    
}

/*
- (void)maximizeImage{
    [UIView animateWithDuration:0.3
                     animations:^{
                         productView.frame = CGRectMake(20, 40, 280, self.view.frame.size.height - 40);
                         leftImageButton.frame = CGRectMake(0, 0, 280, self.view.frame.size.height - 40);
                         fullScreenIcon.frame = CGRectMake(280-40, self.view.frame.size.height - 40, 40, 40);
                     } completion:^(BOOL finished) {
                         fullScreenIcon.hidden = YES;
                         isImageFullScreen = YES;
                         leftImageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
//                         leftImageButton.contentMode = UIViewContentModeScaleAspectFit;
                         fullScreenIcon.frame = CGRectMake(280-40, self.view.frame.size.height - 40, 40, 40);
                     }];
}

- (void)minimizeImage{
    
}
*/


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - network delegate
- (void)returnVotingComplete:(BOOL)success{
    if(success){
        [HUD show:YES];
        HUD.userInteractionEnabled = NO;
        [SharedNetworkManager fetchSinglePosting:postId sender:self];
        
     //   Vote *vote = NULL;
        
     //   [detailViewControllerDelegate returnVote:vote];
    }
}


- (void)returnSinglePost:(PostingItem *)returnedItem itemExist:(BOOL)itemExist{
    HUD.userInteractionEnabled = YES;
    [HUD hide:YES];
    
    if(!itemExist){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"This posting no longer exists" delegate:NULL cancelButtonTitle:@"Back" otherButtonTitles:nil];
        [alert show];
        return;
    }

    item = returnedItem;
   
    [profile setImageWithURL:[NSURL URLWithString:item.mUserProfileURL]
            placeholderImage:[UIImage imageNamed:@"Profile.png"]];

    if (item.mIsBookmarked){
        mCenterLabel.textColor = [UIColor whiteColor];
        isBookmarked = YES;
    }
    else{
        mCenterLabel.textColor = [UIColor grayColor];
        isBookmarked = NO;
    }
    title.text = item.mTitle;
    [self setUserNameString:item.mDisplayName];
    [self setDateStr:item.mDateStr];
    
    
    if(![item.mDescription isEqual:[NSNull null]] && !isDetailAdded){
        //title.backgroundColor = [UIColor blueColor];
        
        isDetailAdded = YES;
        NSLog(@"%@", item.mDescription);
        
        CGSize titleSize = [title.text sizeWithFont:title.font
                                  constrainedToSize:CGSizeMake(title.frame.size.width, 200)
                                      lineBreakMode:NSLineBreakByWordWrapping];
        
        expandLabel = [[UILabel alloc] initWithFrame:CGRectMake(title.frame.origin.x + 5, title.frame.origin.y + titleSize.height + 10,
                                                                         title.frame.size.width - 10, 16)];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"show detail"];
        NSRange strRange = NSMakeRange(0, str.string.length);
        [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:strRange];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoShort" size:13.f] range:strRange];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:strRange];
        [str addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-1.f] range:strRange];
        showString = [[NSAttributedString alloc] initWithAttributedString:str];
 
        str =[[NSMutableAttributedString alloc] initWithString:@"hide detail"];
        strRange = NSMakeRange(0, str.string.length);
        [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:strRange];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoShort" size:13.f] range:strRange];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:strRange];
        [str addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:-1.f] range:strRange];
        hideString = str;
        
        expandLabel.tag = EXPAND;
        expandLabel.attributedText = showString;
        expandLabel.userInteractionEnabled = YES;
        [scrollView addSubview:expandLabel];
        
        int offset = 5;
        
        productView.frame = CGRectOffset(productView.frame, 0, offset);
        resultsView.frame = CGRectOffset(resultsView.frame, 0, offset);
        commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, offset);
        shareLabel.frame = CGRectOffset(shareLabel.frame, 0, offset);
        
        scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                            scrollView.contentSize.height + offset);
        
        
        
       /*
        CGSize titleSize = [title.text sizeWithFont:title.font
                            constrainedToSize:CGSizeMake(title.frame.size.width, 200)
                                      lineBreakMode:NSLineBreakByWordWrapping];
        
        description = [[UITextView alloc] initWithFrame:CGRectZero];
        description.font = [UIFont fontWithName:@"AudimatMono" size:12.f];
        
        NSString *string = item.mDescription;
        NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        description.text = trimmedString;
        
        description.editable = NO;
        description.selectable = YES;
        description.dataDetectorTypes = UIDataDetectorTypeLink;
        description.scrollEnabled = NO;
        
        //CGSize newSize = CGSizeMake(title.frame.size.width, description.contentSize.height);

        CGSize newSize = [description sizeThatFits:CGSizeMake(title.frame.size.width, FLT_MAX)];


        description.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y + titleSize.height + 5, title.frame.size.width, newSize.height);
        description.backgroundColor = [UIColor clearColor];
        
        [scrollView addSubview:description];

        int offset = (description.frame.origin.y + description.frame.size.height) - productView.frame.origin.y;
        productView.frame = CGRectOffset(productView.frame, 0, offset);
        resultsView.frame = CGRectOffset(resultsView.frame, 0, offset);
        commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, offset);
        shareLabel.frame = CGRectOffset(shareLabel.frame, 0, offset);
        
        scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                            scrollView.contentSize.height + offset);
         */
        
    }
   
    
    [barMale setEnable:YES];
    [barFemale setEnable:YES];
    [barUnder21 setEnable:YES];
    [bar21to30 setEnable:YES];
    [barOver30 setEnable:YES];
    
    if(item.isPostingSingle){
        bar21to30.isSingle = YES;
        barFemale.isSingle = YES;
        barMale.isSingle = YES;
        barOver30.isSingle = YES;
        barUnder21.isSingle = YES;
        
        blackDivider.hidden = YES;
        
        if(isFirstLoading){
            isFirstLoading = NO;
        
            leftImageButton.frame = leftFull;
            /*
             [leftImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicSingle]
                          placeholderImage:[UIImage imageNamed:@"post_placeholder_large"]];
            */
            __unsafe_unretained typeof(self) weakSelf = self;
            [leftImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicSingle]
                                    placeholderImage:[UIImage imageNamed:@"post_placeholder_large"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                               [weakSelf.leftImageButton setImage:image forState:UIControlStateNormal];
                                           }];
       
            
            
            //leftImageButton.contentMode = UIViewContentModeScaleAspectFit;
            rightImageButton.hidden = YES;
            rightProductLabel.hidden = YES;

            leftProductLabel.textAlignment = NSTextAlignmentCenter;
            leftProductLabel.center = CGPointMake(160, leftProductLabel.center.y);
            if([item.mLabelLeft isEqual:[NSNull null]])
                leftProductLabel.hidden = YES;
            else
                [self setLeftProductLabel:item.mLabelLeft];
            
            [barMale setBarInitOnLeft];
            [barFemale setBarInitOnLeft];
            [barUnder21 setBarInitOnLeft];
            [bar21to30 setBarInitOnLeft];
            [barOver30 setBarInitOnLeft];
           
            /*
            barMale.frame = CGRectMake(barMale.frame.origin.x, barMale.frame.origin.y, 200, 12);
            barFemale.frame = CGRectMake(barFemale.frame.origin.x, barFemale.frame.origin.y, 200, 12);
            barUnder21.frame = CGRectMake(barUnder21.frame.origin.x, barUnder21.frame.origin.y, 200, 12);
            bar21to30.frame = CGRectMake(bar21to30.frame.origin.x, bar21to30.frame.origin.y, 200, 12);
            barOver30.frame = CGRectMake(barOver30.frame.origin.x, barOver30.frame.origin.y, 200, 12);
            */
            
            leftCountingLabel.textColor = [UIColor redColor];
            leftLikeLabel.textColor = [UIColor redColor];
            rightLikeLabel.hidden = YES;
            rightCountingLabel.text = @"-";
            rightCountingLabel.textColor = [UIColor grayColor];
        }
        
        [leftCountingLabel countFrom:0 to:item.mVotesSingle withDuration:0.5];
        [UIView animateWithDuration:0.7
                         animations:^{
                             [barMale setBarVisibleScoreLeft:item.mVotesLeftMale right:10];
                             [barFemale setBarVisibleScoreLeft:item.mVotesLeftFemale right:10];
                             [barUnder21 setBarVisibleScoreLeft:item.mVotesLeftUnder21 right:10];
                             [bar21to30 setBarVisibleScoreLeft:item.mVotesLeft21to30 right:10];
                             [barOver30 setBarVisibleScoreLeft:item.mVotesLeftOver30 right:10];
                         }];
        
        
        if(item.mVotesLeftMale == 0)
            [barMale setEnable:NO];
        if(item.mVotesLeftFemale == 0 )
            [barFemale setEnable:NO];
        
        if(item.mVotesLeftUnder21 == 0)
            [barUnder21 setEnable:NO];
        
        if(item.mVotesLeft21to30 == 0)
            [bar21to30 setEnable:NO];
        
        if(item.mVotesLeftOver30 == 0)
            [barOver30 setEnable:NO];

        
/*
        fullScreenIcon = [[UIImageView alloc] initWithFrame:CGRectMake(320-40, leftImageButton.frame.size.height-40, 40, 40)];
        fullScreenIcon.alpha = 0.7f;
        fullScreenIcon.image = [UIImage imageNamed:@"fullscreen-icon.png"];
        fullScreenIcon.tag = FULLSCREEN;
        fullScreenIcon.userInteractionEnabled = YES;
        
        [leftImageButton addSubview:fullScreenIcon];
  */

        fullScreenButton.hidden = NO;
    }
    else{
        bar21to30.isSingle = NO;
        barFemale.isSingle = NO;
        barMale.isSingle = NO;
        barOver30.isSingle = NO;
        barUnder21.isSingle = NO;
        
        if(isFirstLoading){
            isFirstLoading = NO;
            leftImageButton.frame = leftHalf;
            
            __unsafe_unretained typeof(self) weakSelf = self;
            [leftImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicLeft]
                                    placeholderImage:[UIImage imageNamed:@"post_placeholder_small"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                               [weakSelf.leftImageButton setImage:image forState:UIControlStateNormal];
                                           }];
            [rightImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicRight]
                                    placeholderImage:[UIImage imageNamed:@"post_placeholder_small"]
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                               [weakSelf.rightImageButton setImage:image forState:UIControlStateNormal];
                                           }];
            
           // [leftImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicLeft]
           //               placeholderImage:[UIImage imageNamed:@"post_placeholder_small"]];
            //[rightImageButton.imageView setImageWithURL:[NSURL URLWithString:item.mPicRight]
              //            placeholderImage:[UIImage imageNamed:@"post_placeholder_small"]];
         
            
            if([item.mLabelLeft isEqual:[NSNull null]])
                leftProductLabel.hidden = YES;
            else
                [self setLeftProductLabel:item.mLabelLeft];
            
            if([item.mLabelRight isEqual:[NSNull null]])
                rightProductLabel.hidden = YES;
            else
                [self setRightProductLabel:item.mLabelRight];
        
            if(item.mVotesLeft > item.mVotesRight){
                leftCountingLabel.textColor = [UIColor redColor];
                leftLikeLabel.textColor = [UIColor redColor];
                rightCountingLabel.textColor = [UIColor whiteColor];
                rightLikeLabel.textColor = [UIColor whiteColor];
                [barMale setBarInitOnLeft];
                [barFemale setBarInitOnLeft];
                [barUnder21 setBarInitOnLeft];
                [bar21to30 setBarInitOnLeft];
                [barOver30 setBarInitOnLeft];
                
                
            }
            else{
                leftCountingLabel.textColor = [UIColor whiteColor];
                leftLikeLabel.textColor = [UIColor whiteColor];
                rightCountingLabel.textColor = [UIColor redColor];
                rightLikeLabel.textColor = [UIColor redColor];
                [barMale setBarInitOnRight];
                [barFemale setBarInitOnRight];
                [barUnder21 setBarInitOnRight];
                [bar21to30 setBarInitOnRight];
                [barOver30 setBarInitOnRight];
            }
        }
        
        [leftCountingLabel countFrom:0 to:item.mVotesLeft withDuration:0.5];
        [rightCountingLabel countFrom:0 to:item.mVotesRight withDuration:0.5];
        [UIView animateWithDuration:0.7
                         animations:^{
                             [barMale setBarVisibleScoreLeft:item.mVotesLeftMale right:item.mVotesRightMale];
                             [barFemale setBarVisibleScoreLeft:item.mVotesLeftFemale right:item.mVotesRightFemale];
                             [barUnder21 setBarVisibleScoreLeft:item.mVotesLeftUnder21 right:item.mVotesRightUnder21];
                             [bar21to30 setBarVisibleScoreLeft:item.mVotesLeft21to30 right:item.mVotesRight21to30];
                             [barOver30 setBarVisibleScoreLeft:item.mVotesLeftOver30 right:item.mVotesRightOver30];
                         }];
            
        
        if(item.mVotesLeftMale == 0 && item.mVotesRightMale == 0)
            [barMale setEnable:NO];
        if(item.mVotesLeftFemale == 0 && item.mVotesRightFemale == 0)
            [barFemale setEnable:NO];
        
        if(item.mVotesLeftUnder21 == 0 && item.mVotesRightUnder21 == 0)
            [barUnder21 setEnable:NO];
        
        if(item.mVotesLeft21to30 == 0 && item.mVotesRight21to30 == 0)
            [bar21to30 setEnable:NO];
        
        if(item.mVotesLeftOver30 == 0 && item.mVotesRightOver30 == 0)
            [barOver30 setEnable:NO];
        
            
        /*
        [barMale setBarInitOnLeft];
        [barFemale setBarInitOnRight];
        
        [leftCountingLabel countFrom:0 to:21 withDuration:0.7];
        [UIView animateWithDuration:0.7
                         animations:^{
                             [barMale setBarVisibleScoreLeft:15 right:0];
                             [barFemale setBarVisibleScoreLeft:2 right:22];
                         }];
        
        
        */
        
        
        
        
        
    }
    
    if(item.mMyVote == VoteLeft){
        leftVoteImageView.hidden = NO;
        voteSide = VoteLeft;
    }
    else if(item.mMyVote == VoteRight){
        rightVoteImageView.hidden = NO;
        voteSide = VoteRight;
    }
 
    if(item.mNumComments == 0){
        //commentsLabel.userInteractionEnabled = NO;
        commentsLabel.text = @"0comments";
        commentsLabel.textColor = [UIColor lightGrayColor];
        
    }
    else if(item.mNumComments == 1){
        commentsLabel.text = @"1comment";
    }
    else{
        commentsLabel.text = [NSString stringWithFormat:@"%dcomments", item.mNumComments];
    }

    /*
    if((item.isPostingSingle &&[item.mLabelLeft isEqual:@""]) ||
       (!item.isPostingSingle && [item.mLabelLeft isEqual:@""] && [item.mLabelRight isEqual:@""]))
        
       resultsView.frame = CGRectOffset(resultsView.frame, 0, -25);
    */
    
    
    //NSLog(@"%f  %f",     title.contentSize.height, title.frame.size.height);
    float fontSize = 17;
    while(title.contentSize.height > 50){
        fontSize -= 0.3;
        title.font = [UIFont fontWithName:@"BebasNeueSmallLinespacing" size:fontSize];
            NSLog(@"%f  %f",     title.contentSize.height, title.frame.size.height);
    }
    NSLog(@"%f  %f",     title.contentSize.height, title.frame.size.height);
    
}

- (void)returnBookmarkCommit:(BOOL)success isBookmark:(BOOL)isBookmark{
    if(success){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bookmark Successful" message:@"Added to Bookmark"
                                                       delegate:NULL cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];

        [SharedNetworkManager fetchSinglePosting:postId sender:self];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bookmark Unsuccessful" message:@"Please try again in few minutes"
                                                       delegate:NULL cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (void)descriptionShow:(BOOL)show{
    if(!description){
        description = [[UITextView alloc] initWithFrame:CGRectZero];
        description.font = [UIFont fontWithName:@"AudimatMono" size:12.f];
        
        NSString *string = item.mDescription;
        NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        description.text = trimmedString;
        
        description.editable = NO;
        //description.selectable = YES;
        description.dataDetectorTypes = UIDataDetectorTypeLink;
        description.scrollEnabled = NO;
        
        
        CGSize newSize = [description sizeThatFits:CGSizeMake(title.frame.size.width, FLT_MAX)];
        
        description.frame = CGRectMake(title.frame.origin.x, expandLabel.frame.origin.y + expandLabel.frame.size.height,
                                       title.frame.size.width, newSize.height);
        description.backgroundColor = [UIColor clearColor];
        
        [scrollView addSubview:description];
        
        
    }
    
    if(show){
        description.hidden = NO;
        description.alpha = 0.0f;
       [UIView animateWithDuration:0.3
                        animations:^{
                            int offset = (description.frame.origin.y + description.frame.size.height) - productView.frame.origin.y;
                            productView.frame = CGRectOffset(productView.frame, 0, offset);
                            resultsView.frame = CGRectOffset(resultsView.frame, 0, offset);
                            commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, offset);
                            shareLabel.frame = CGRectOffset(shareLabel.frame, 0, offset);
                            scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                                                scrollView.contentSize.height + offset);
                            description.alpha = 1.0f;
                        } completion:^(BOOL finished) {
                            expandLabel.attributedText = hideString;
                            
                        }];
        /*
        
        int offset = (description.frame.origin.y + description.frame.size.height) - productView.frame.origin.y;
        productView.frame = CGRectOffset(productView.frame, 0, offset);
        resultsView.frame = CGRectOffset(resultsView.frame, 0, offset);
        commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, offset);
        shareLabel.frame = CGRectOffset(shareLabel.frame, 0, offset);
        
        scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                            scrollView.contentSize.height + offset);
         */
    }
    else{
        [UIView animateWithDuration:0.3
                         animations:^{
                             
                             int offset = description.frame.size.height - 5;
                             productView.frame = CGRectOffset(productView.frame, 0, -offset);
                             resultsView.frame = CGRectOffset(resultsView.frame, 0, -offset);
                             commentsLabel.frame = CGRectOffset(commentsLabel.frame, 0, -offset);
                             shareLabel.frame = CGRectOffset(shareLabel.frame, 0, -offset);
                             
                             scrollView.contentSize = CGSizeMake(scrollView.contentSize.width,
                                                                 scrollView.contentSize.height - offset);
                             description.alpha = 0.0f;
                             
                         } completion:^(BOOL finished) {
                             expandLabel.attributedText = showString;
                             description.hidden = YES;
                         }];
    }
  
}

-(void)networkManagerError{
    HUD.userInteractionEnabled = YES;
    [HUD hide:YES];

    NSLog(@"Network Error");
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}


#pragma mark - CommentsTableViewControllerDelegate
- (void)reportNumComments:(int)numComments{
    if(numComments == 1){
        commentsLabel.text = @"1comment";
    }
    else{
        commentsLabel.text = [NSString stringWithFormat:@"%dcomments", numComments];
    }
}

- (void)displayHUD{
    NSLog(@"display hud");
    [HUD show:YES];
}
- (void)hideHUD{
    NSLog(@"hide hud");
    [HUD hide:YES];
}

#pragma mark - AlertViewDelegate for login check
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}



@end
