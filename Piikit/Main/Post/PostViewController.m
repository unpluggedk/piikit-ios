//
//  PostViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "PostViewController.h"
#import "NetworkManager.h"
#import "PostingItem.h"
#import "AppData.h"
#import <QuartzCore/QuartzCore.h>

#import "IIViewDeckController.h"

#import "Profile.h"
#import "UIImageView+WebCache.h"

#import "NeedToLoginView.h"

//#import "UIImage+Resize.h"

#define QUESTION 101
#define PRODUCTNAME_LEFT 102
#define PRODUCTNAME_RIGHT 103
#define DESCRIPTION 104

#define LEFTPIC 105
#define RIGHTPIC 106


@interface PostViewController ()

@end

@implementation PostViewController

//UIImagePickerController *picker;
GKImagePicker *imagePicker;
UIPopoverController *popoverController;
//UIImagePickerController *ctr;

UIFont *bebas_30;// = [UIFont fontWithName:@"Bebas Neue" size:17.f];
UIFont *audimat_21;// = [UIFont fontWithName:@"AudimatMono" size:12.f];
UIFont *audimat_question;// = [UIFont fontWithName:@"AudimatMono" size:15.f];
UIFont *audimat_short;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        bebas_30 = [UIFont fontWithName:@"Bebas Neue" size:17.f];
        audimat_21 = [UIFont fontWithName:@"AudimatMono" size:15.f];
        audimat_question = [UIFont fontWithName:@"AudimatMonoShort" size:14.f];
        audimat_short = [UIFont fontWithName:@"AudimatMonoShort" size:15.f];

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
    
    self.tabBarController.viewDeckController.panningMode = IIViewDeckNoPanning;
    
    if([SharedProfile isLoginSkipped]){
        if(mUserProfile)
            [mUserProfile setImage:[UIImage imageNamed:@"PROFILE-ICON"]];
    
        if(mUserName){
            NSString *displayname = @"PIIKIT";
            NSMutableAttributedString *usernameStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ asks,", displayname]];
            [usernameStr addAttribute:NSFontAttributeName value:bebas_30 range:NSMakeRange(0, displayname.length)];
            [usernameStr addAttribute:NSFontAttributeName value:audimat_21 range:NSMakeRange(displayname.length, 6)];
            [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, displayname.length)];
            [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(displayname.length, 6)];
            mUserName.attributedText = usernameStr;
        }
        return;
    }
    
    // reload profile
    if(mUserProfile)
        [mUserProfile setImageWithURL:[NSURL URLWithString:[SharedProfile profileURLString]] placeholderImage:[UIImage imageNamed:@"PROFILE-ICON"]];
    
    if(mUserName){
        NSString *displayname = [SharedProfile displayname];
        NSMutableAttributedString *usernameStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ asks,", displayname]];
        [usernameStr addAttribute:NSFontAttributeName value:bebas_30 range:NSMakeRange(0, displayname.length)];
        [usernameStr addAttribute:NSFontAttributeName value:audimat_21 range:NSMakeRange(displayname.length, 6)];
        [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, displayname.length)];
        [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(displayname.length, 6)];
        mUserName.attributedText = usernameStr;
    }
}




- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self setupLayout];
    
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Uploading";
    HUD.userInteractionEnabled = NO;
    
  //  imageProcessHelperView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 315, 360)];
  //  imageProcessHelperView.contentMode = UIViewContentModeScaleAspectFit;
  //  imageProcessHelperView.backgroundColor = [UIColor blackColor];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(keyboardWillAnimate:)
                                                  name:UIKeyboardWillShowNotification
                                                object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(keyboardWillAnimate:)
                                                  name:UIKeyboardWillHideNotification
                                                object:nil];
    
 
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

CGPoint restorePoint;

- (void)keyboardWillAnimate:(NSNotification *)notification
{
    if([mUserQuestion isFirstResponder])
        return;
    
    if([mDescription isFirstResponder])
        return;
    
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    //[mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([mDataSource count] > 0)?[mDataSource count]-1:0 inSection:0]
    //                  atScrollPosition:UITableViewScrollPositionBottom animated:YES];
   
    if([notification name] == UIKeyboardWillShowNotification)
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height + keyboardBounds.size.height - 49);
        
    [UIView animateWithDuration:[duration doubleValue]
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                            if([notification name] == UIKeyboardWillShowNotification)
                            {
                                restorePoint = scrollView.contentOffset;
                                
                                
                                //CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
                                CGPoint bottomOffset = CGPointMake(0, keyboardBounds.size.height - 49 - 25);
                                [scrollView setContentOffset:bottomOffset animated:NO];
                            }
                            else if([notification name] == UIKeyboardWillHideNotification)
                            {
                                [scrollView setContentOffset:restorePoint animated:NO];
                                //[scrollView scrollRectToVisible:restoreVisibleRect animated:NO];
                                
                            }
                        } completion:^(BOOL finished) {
                            if([notification name] == UIKeyboardWillHideNotification)
                                scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height);

                        }];
    /*
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    if([notification name] == UIKeyboardWillShowNotification)
    {

        restorePoint = scrollView.contentOffset;
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height + keyboardBounds.size.height - 49);
        CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height);
        [scrollView setContentOffset:restorePoint animated:YES];
        
        
    }
    [UIView commitAnimations];
    */
}


- (void)popToMain{
    [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
}


- (void)setupLayout{
    
    CGRect viewFrame;
    if([SharedAppData isIos7])
        viewFrame = CGRectMake(0, 20, 320, self.view.frame.size.height - 49 - 20);
    else
        viewFrame = CGRectMake(0, 0, 320, self.view.frame.size.height - 49);
    
    scrollView = [[AppScrollView alloc] initWithFrame:viewFrame];
    [self.view addSubview:scrollView];

 
    CGFloat relativeY = 35;
    
   // if([SharedAppData screenHeight] < 568){
        relativeY -= 25;
    //}
   
    relativeY = 0;
    //NSLog(@"postviewcontroller self.view size %f", self.view.frame.size.height);
    //NSLog(@"postviewcontroller self.view.bounds size %f", self.view.bounds.size.height);
    UIView *topbar = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    topbar.backgroundColor = [UIColor blackColor];
    UILabel *topbarLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 270, 29)];
    topbarLabel.font = bebas_30;
    topbarLabel.text = @"WHAT DO YOU WANT TO ASK?";
    topbarLabel.backgroundColor = [UIColor clearColor];
    topbarLabel.textColor = [UIColor lightGrayColor];
    [topbar addSubview:topbarLabel];
    
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearBtn setImage:[UIImage imageNamed:@"clearBtn.png"] forState:UIControlStateNormal];
    clearBtn.contentMode = UIViewContentModeScaleAspectFit;
    clearBtn.frame = CGRectMake(320 - 50, 5, 40, 20);
    [clearBtn addTarget:self action:@selector(clearLayout) forControlEvents:UIControlEventTouchUpInside];
    [topbar addSubview:clearBtn];
    
    
    
    [scrollView addSubview:topbar];
    relativeY += 29;
    
    if([SharedProfile isLoginSkipped]){
        NeedToLoginView *view = [[NeedToLoginView alloc] initWithFrame:CGRectMake(0, relativeY, 320, viewFrame.size.height - relativeY)];
        [view.loginBtn addTarget:self action:@selector(popToMain) forControlEvents:UIControlEventTouchUpInside];
        // if layered with a view, button won't work.
        [scrollView addSubview:view.loginBtn];
        [scrollView addSubview:view.logo];
        
        return;
    }
    
    

    mUserProfile = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PROFILE-ICON"]];
    [mUserProfile setImageWithURL:[NSURL URLWithString:[SharedProfile profileURLString]] placeholderImage:[UIImage imageNamed:@"PROFILE-ICON"]];
    [mUserProfile setBackgroundColor:[UIColor lightGrayColor]];
    [mUserProfile setFrame:CGRectMake(15, relativeY+4, 50, 50)];
    //[mUserProfile setTag:USERPROFILE];
    [scrollView addSubview:mUserProfile];
    
    CGFloat x = 15+50+7;
    mUserName = [[UILabel alloc] initWithFrame:CGRectMake(x, relativeY, 320-15-x, 29)];
    NSString *displayname = [SharedProfile displayname];// @"ANONYMOUS_USER"; // get from data
    NSMutableAttributedString *usernameStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ asks,", displayname]];
    [usernameStr addAttribute:NSFontAttributeName value:bebas_30 range:NSMakeRange(0, displayname.length)];
    [usernameStr addAttribute:NSFontAttributeName value:audimat_21 range:NSMakeRange(displayname.length, 6)];
    [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, displayname.length)];
    [usernameStr addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(displayname.length, 6)];
    mUserName.attributedText = usernameStr;
    [scrollView addSubview:mUserName];
    
    mUserQuestion = [[UITextView alloc] initWithFrame:CGRectMake(x, relativeY+20, 320-15-x, 37)];
    mUserQuestion.font = audimat_question;
    mUserQuestion.tag = QUESTION;
    mUserQuestion.text = @" ";
    mUserQuestion.delegate = self;
    mUserQuestion.textColor = [UIColor blackColor];
    
    mUserQuestionPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 215, 37)];
    mUserQuestionPlaceholder.font = audimat_question;
    mUserQuestionPlaceholder.textColor = [UIColor lightGrayColor];
    mUserQuestionPlaceholder.text = @"Enter your question";
    mUserQuestionPlaceholder.userInteractionEnabled = NO;
    [mUserQuestion addSubview:mUserQuestionPlaceholder];
    
    relativeY += 58;
    
    [scrollView addSubview:mUserQuestion];
    
    
    mDescription = [[UITextView alloc] initWithFrame:CGRectMake(x, relativeY, 320-15-x, 27.5)];
    mDescription.font = audimat_question;
    mDescription.tag = DESCRIPTION;
    mDescription.text = @" ";
    mDescription.delegate = self;
    mDescription.textColor = [UIColor blackColor];
 
    mDescriptionPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 37)];
    mDescriptionPlaceholder.font = audimat_question;
    mDescriptionPlaceholder.textColor = [UIColor lightGrayColor];
    mDescriptionPlaceholder.text = @"Description (Optional)";
    mDescriptionPlaceholder.userInteractionEnabled = NO;
    [mDescription addSubview:mDescriptionPlaceholder];
    
    [scrollView addSubview:mDescription];
    
    relativeY += 38;
     
    mPictureView = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 200)];
    relativeY += 200;
    
    //mPictureView.backgroundColor = [UIColor redColor];
    mPictureView.layer.borderWidth = 1.f;
    [scrollView addSubview:mPictureView];
    
    mLeftImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
    mLeftImage.backgroundColor = [UIColor whiteColor];
    mLeftImage.tag = LEFTPIC;
    mLeftImage.layer.borderWidth = 1.0f;
    mLeftImage.contentMode = UIViewContentModeScaleAspectFill;
    mLeftImage.userInteractionEnabled = YES; // enable buttons
    
    //mLeftImage.image = [UIImage imageNamed:@"sample_3.jpg"];
    
    [mPictureView addSubview:mLeftImage];
    
    mLeftText = [[UITextField alloc] initWithFrame:CGRectMake(10, 180, 300, 20)];
    mLeftText.font = audimat_question;
    mLeftText.placeholder = @"Add Name";
    mLeftText.tag = PRODUCTNAME_LEFT;
    mLeftText.delegate = self;
    [mPictureView addSubview:mLeftText];
    
    mLeftImageDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    mLeftImageDelete.frame = CGRectMake(5, 5, 30, 30);
    [mLeftImageDelete setImage:[UIImage imageNamed:@"delete1"] forState:UIControlStateNormal];
    [mLeftImageDelete addTarget:self action:@selector(leftImageDeletePressed) forControlEvents:UIControlEventTouchUpInside];

    [mLeftImage addSubview:mLeftImageDelete];
    
    mLeftImagePost = [UIButton buttonWithType:UIButtonTypeCustom];
    mLeftImagePost.frame = CGRectMake(0, 0, 104, 45);
    [mLeftImagePost setImage:[UIImage imageNamed:@"IMAGE1POST.png"] forState:UIControlStateNormal];
    [mLeftImagePost addTarget:self action:@selector(leftImagePostPressed) forControlEvents:UIControlEventTouchUpInside];
    mLeftImagePost.center = mLeftImage.center;
    
    [mLeftImage addSubview:mLeftImagePost];
    
    mMaximizeSinglePic = [UIButton buttonWithType:UIButtonTypeCustom];
    mMaximizeSinglePic.frame = CGRectMake(mLeftImage.frame.size.width - 40, 0, 40, 40);
    [mMaximizeSinglePic setImage:[UIImage imageNamed:@"fullscreen-icon.png"] forState:UIControlStateNormal];
    [mMaximizeSinglePic addTarget:self action:@selector(maximizeImage) forControlEvents:UIControlEventTouchUpInside];
    mMaximizeSinglePic.alpha = 0.5;
    [mLeftImage addSubview:mMaximizeSinglePic];
    
    mMinimizeSinglePic = [UIButton buttonWithType:UIButtonTypeCustom];
    mMinimizeSinglePic.frame = CGRectMake(mLeftImage.frame.size.width - 40, 0, 40, 40);
    [mMinimizeSinglePic setImage:[UIImage imageNamed:@"fullscreen-close-icon.png"] forState:UIControlStateNormal];
    [mMinimizeSinglePic addTarget:self action:@selector(minimizeImage) forControlEvents:UIControlEventTouchUpInside];
    mMinimizeSinglePic.alpha = 0.5;
    [mLeftImage addSubview:mMinimizeSinglePic];
    
    mLeftImageDelete.hidden = YES;
    mMaximizeSinglePic.hidden = YES;
    mMinimizeSinglePic.hidden = YES;
    
    

    mRightImagePost = [UIButton buttonWithType:UIButtonTypeCustom];
    mRightImagePost.frame = CGRectMake(320 - 104, 180 - 45, 104, 45);
    mRightImagePost.alpha = 0.5;
    [mRightImagePost setImage:[UIImage imageNamed:@"IMAGE2POST.png"] forState:UIControlStateNormal];
    [mRightImagePost addTarget:self action:@selector(rightImagePostPressed) forControlEvents:UIControlEventTouchUpInside];
    mRightImagePost.hidden = YES;
    
    [mPictureView addSubview:mRightImagePost];
    
    
    
    mRightImage = [[UIImageView alloc] initWithFrame:CGRectMake(160, 0, 160, 180)];
    mRightImage.backgroundColor = [UIColor whiteColor];
    mRightImage.tag = RIGHTPIC;
    mRightImage.layer.borderWidth = 1.0f;
    mRightImage.contentMode = UIViewContentModeScaleAspectFill;
    mRightImage.userInteractionEnabled = YES; // enable buttons
    //mLeftImage.image = [UIImage imageNamed:@"sample_3.jpg"];
    [mPictureView addSubview:mRightImage];
    
    
    mRightText = [[UITextField alloc] initWithFrame:CGRectMake(10 + 160, 180, 140, 20)];
    mRightText.font = audimat_question;
    mRightText.placeholder = @"Add Name";
    mRightText.tag = PRODUCTNAME_RIGHT;
    mRightText.delegate = self;
    mRightText.hidden = YES;
    [mPictureView addSubview:mRightText];
    
    mLeftText.text = @"";
    mRightText.text = @"";
    
    mRightImageDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    mRightImageDelete.frame = CGRectMake(5, 5, 30, 30);
    [mRightImageDelete setImage:[UIImage imageNamed:@"delete1"] forState:UIControlStateNormal];
    [mRightImageDelete addTarget:self action:@selector(rightImageDeletePressed) forControlEvents:UIControlEventTouchUpInside];
    
    [mRightImage addSubview:mRightImageDelete];
    
    [mPictureView addSubview:mRightImage];
    
    mRightImage.hidden = YES;
    mRightImageDelete.hidden = YES;
    
    
    mLeftImage.clipsToBounds = YES;
    mRightImage.clipsToBounds = YES;
    
    relativeY += 15;
    
    mCategoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    mCategoryBtn.frame = CGRectMake(0, relativeY, 320, 29);
    relativeY += 29;
    
    mCategoryBtn.backgroundColor = [UIColor blackColor];
    [mCategoryBtn addTarget:self action:@selector(categoryPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    mCategory = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 29)];
    mCategory.text = @"Select Category";
    mCategory.font = [UIFont fontWithName:@"AudimatMonoBold" size:20.f];
    mCategory.textAlignment = NSTextAlignmentCenter;
    mCategory.textColor = [UIColor whiteColor];
    mCategory.backgroundColor = [UIColor clearColor];
    [mCategoryBtn addSubview:mCategory];
    
    [scrollView addSubview:mCategoryBtn];
    
    catId = NULL;
    
    relativeY += 15;
    mSubmitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    mSubmitBtn.frame = CGRectMake(0, relativeY, 320, 29);
    mSubmitBtn.backgroundColor = [SharedAppData themeColor];
    [mSubmitBtn addTarget:self action:@selector(submitPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *submit = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 29)];
    submit.backgroundColor = [UIColor clearColor];
    submit.text = @"PIIKIT NOW";
    submit.font = [UIFont fontWithName:@"AudimatMonoBold" size:20.f];
    submit.textAlignment = NSTextAlignmentCenter;
    submit.textColor = [UIColor whiteColor];
    [mSubmitBtn addSubview:submit];
    
    [scrollView addSubview:mSubmitBtn];
    
    categoryOriginalFrame = mCategoryBtn.frame;
    submitOriginalFrame = mSubmitBtn.frame;
    
    
    scrollView.contentSize = CGSizeMake(320, mSubmitBtn.frame.origin.y + mSubmitBtn.frame.size.height+ 30);
    

}

- (void)clearLayout{
    mUserQuestion.text = @"";
    mDescription.text = @"";
    mUserQuestionPlaceholder.hidden = NO;
    mDescriptionPlaceholder.hidden = NO;
    
    mLeftImage.image = NULL;
    mRightImage.image = NULL;
    
    mLeftText.text = @"";
    mRightText.text = @"";
    
    mLeftImage.frame = CGRectMake(mLeftImage.frame.origin.x, mLeftImage.frame.origin.y,
                                  320, 180);
    mLeftText.frame = CGRectMake(mLeftText.frame.origin.x, 180, 320, 20);
    mPictureView.frame = CGRectMake(mPictureView.frame.origin.x,
                                    mPictureView.frame.origin.y,
                                    320,
                                    180 + 20);
    mRightImagePost.frame = CGRectMake(320 - 104, 180 - 45, 104, 45);
    mCategoryBtn.frame = categoryOriginalFrame;
    mSubmitBtn.frame = submitOriginalFrame;
    mMaximizeSinglePic.hidden = YES;
    mMinimizeSinglePic.hidden = YES;
    
    scrollView.contentSize = CGSizeMake(320, mSubmitBtn.frame.origin.y + mSubmitBtn.frame.size.height+ 30);
    
    //scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height - (365-180));
    
    mRightImage.hidden = YES;
    
    mLeftImagePost.hidden = NO;
    mRightImagePost.hidden = YES;
    mLeftImageDelete.hidden = YES;
    mRightImageDelete.hidden = YES;
    
    mCategory.text = @"Select Category";
    catId = NULL;
    
    
}



- (void) leftImagePostPressed{
    [self SelectPictureActionSheet];
}


- (void) rightImagePostPressed{
    [self SelectPictureActionSheet];
}

- (void) leftImageLoaded:(UIImage*)image{
    mLeftImage.image = image;
    mLeftImagePost.hidden = YES;
    
    mMaximizeSinglePic.hidden = NO;
    
    mRightImagePost.hidden = NO;
}

- (void) rightImageLoaded:(UIImage*)image{
    NSLog(@"right image Pressed");
    
    [self minimizeImage];
    
    mLeftImage.frame = CGRectMake(0, 0, 160, 180);
    mLeftText.frame = CGRectMake(10, 180, 140, 20);
    
    mRightImage.hidden = NO;
    mRightText.hidden = NO;
    
    mRightImage.image = image;
    
}

- (void) leftImageDeletePressed{
    // when single
    if(mRightImage.hidden){
        NSLog(@"single delete");
        mLeftImage.image = NULL;
        
        mLeftImageDelete.hidden = YES;
        mLeftImagePost.hidden = NO;
        
        [self minimizeImage];
        
        mMaximizeSinglePic.hidden = YES;
        mMinimizeSinglePic.hidden = YES;
        
        mRightImagePost.hidden = YES;
    }
    // when right image exists
    else{
        [UIView animateWithDuration:0.5
                         animations:^{
                             mLeftImage.frame = CGRectOffset(mLeftImage.frame, mLeftImage.frame.size.width, 0);
                             mRightImage.frame = CGRectMake(0, 0, 320, 180);
                             mRightText.frame = CGRectMake(10, 180, 300, 20);
                         } completion:^(BOOL finished) {
                             mLeftImage.frame = CGRectMake(0, 0, 320, 180);
                             
                             mLeftImage.image = mRightImage.image;
                             mLeftText.text = mRightText.text;
                             
                             mRightImage.frame = CGRectMake(160, 0, 160, 180);
                             mRightText.frame = CGRectMake(170, 180, 140, 20);
                             mRightImage.image = nil;
                             mRightText.text = @"";
                             
                             mRightImage.hidden = YES;
                             mRightText.hidden = YES;
                             
                             mMaximizeSinglePic.hidden = NO;
                             mMinimizeSinglePic.hidden = YES;
                         }];
    }

}



- (void) rightImageDeletePressed{
    [UIView animateWithDuration:0.5
                     animations:^{
                         mLeftImage.frame = CGRectMake(0, 0, 320, 180);
                         mLeftText.frame = CGRectMake(10, 180, 300, 20);
                         mRightImage.frame = CGRectOffset(mRightImage.frame, 160, 0);
                     } completion:^(BOOL finished) {
                         mRightImageDelete.hidden = YES;
                         mRightImage.image = nil;
                         mRightImage.hidden = YES;
                         mRightText.hidden = YES;
                         mRightText.text = @"";
                         mRightImage.frame = CGRectOffset(mRightImage.frame, -160, 0);
                     }];
    
}

- (void) maximizeImage{
    mMaximizeSinglePic.hidden = YES;
    mMinimizeSinglePic.hidden = NO;
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         mLeftImage.frame = CGRectMake(mLeftImage.frame.origin.x, mLeftImage.frame.origin.y,
                                                       mLeftImage.frame.size.width, 365);
                         mLeftText.frame = CGRectMake(mLeftText.frame.origin.x, 365, mLeftText.frame.size.width, 20);
                         mPictureView.frame = CGRectMake(mPictureView.frame.origin.x,
                                                         mPictureView.frame.origin.y,
                                                         mPictureView.frame.size.width,
                                                         365 + 20);
                         mRightImagePost.frame = CGRectMake(320 - 104, 365 - 45, 104, 45);
                         
                         mCategoryBtn.frame = CGRectMake(0, categoryOriginalFrame.origin.y + (365-180), categoryOriginalFrame.size.width, categoryOriginalFrame.size.height);
                         mSubmitBtn.frame = CGRectMake(0, submitOriginalFrame.origin.y + (365-180), submitOriginalFrame.size.width, submitOriginalFrame.size.height);
                     } completion:^(BOOL finished) {
                         scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height + (365-180));
                         
                     }];
}

- (void) minimizeImage{
    mMaximizeSinglePic.hidden = NO;
    mMinimizeSinglePic.hidden = YES;
    [UIView animateWithDuration:0.5
                     animations:^{
                         mLeftImage.frame = CGRectMake(mLeftImage.frame.origin.x, mLeftImage.frame.origin.y,
                                                       mLeftImage.frame.size.width, 180);
                         mLeftText.frame = CGRectMake(mLeftText.frame.origin.x, 180, mLeftText.frame.size.width, 20);
                         mPictureView.frame = CGRectMake(mPictureView.frame.origin.x,
                                                         mPictureView.frame.origin.y,
                                                         mPictureView.frame.size.width,
                                                         180 + 20);
                         mRightImagePost.frame = CGRectMake(320 - 104, 180 - 45, 104, 45);
                         mCategoryBtn.frame = categoryOriginalFrame;
                         mSubmitBtn.frame = submitOriginalFrame;
                         
                     } completion:^(BOOL finished) {
                         scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height);
                     }];
}





- (void) submitPressed{
  
    if([SharedProfile isLoginSkipped]){
        NSLog(@"login skipped!");
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"PIIKIT"
                              message:@"PIIKIT needs you to login.\nDo you want to Join or Login?"
                              delegate:self
                              cancelButtonTitle:Nil
                              otherButtonTitles:@"Yes", @"No", nil];
        
        [alert show];
        return;
    }
    
    
    
    if(mLeftImage.image == NULL || [mUserQuestion.text isEqualToString:@""] || catId == NULL){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Posting Error" message:@"Please Enter all fields and \nSelect a Category"
                                                       delegate:NULL cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];
    }
    else{
        [HUD show:YES];
        scrollView.userInteractionEnabled = NO;
        [SharedNetworkManager postPictureWithImage:mLeftImage.image side:Left sender:self ];
    }

}

- (void) categoryPressed{
    CategoryViewController *categoryVC = [[CategoryViewController alloc] init];
    categoryVC.categoryViewControllerDelegate = self;
    [categoryVC loadFromPost];
    [self presentModalViewController:categoryVC animated:YES];
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    int tag = [[[touches anyObject] view] tag];
    
    if(tag != QUESTION &&  [mUserQuestion isFirstResponder])
       [mUserQuestion resignFirstResponder];
    
    else if(tag != DESCRIPTION &&  [mDescription isFirstResponder])
        [mDescription resignFirstResponder];
    
    else if(tag != PRODUCTNAME_LEFT && [mLeftText isFirstResponder])
        [mLeftText resignFirstResponder];
    
    else if(tag != PRODUCTNAME_RIGHT && [mRightText isFirstResponder])
        [mRightText resignFirstResponder];
    
    
    //[self.view endEditing:YES];
    switch (tag) {
        case LEFTPIC:{
            if(mLeftImage.image == nil)
                return;
            if(mLeftImageDelete.hidden)
                mLeftImageDelete.hidden = NO;
            else
                mLeftImageDelete.hidden = YES;
            break;
        }
        case RIGHTPIC:{
            if(mRightImage.image == nil)
                return;
            if(mRightImageDelete.hidden)
                mRightImageDelete.hidden = NO;
            else
                mRightImageDelete.hidden = YES;
            break;
        }


        default:
            break;
    }
}



#pragma mark - category selection
- (void)loadCategory:(NSNumber *)categoryId name:(NSString *)str{
    NSLog(@"clicked  %@ : %@", categoryId, str);
    catId = categoryId;
    
    if(![str isEqualToString:@"BACK"])
        mCategory.text = str;
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - network delegate

- (void) returnImagePostedURL:(NSString*)urlStr side:(Side)side{
    BOOL isReady = NO;
    
    NSLog(@"RECEIVED");
    
    if(side == Left){
        leftImageURL = urlStr;
        if(mRightImage.hidden)
            isReady = YES;
        else{
            /*imageProcessHelperView.image = mRightImage.image;
            
            UIGraphicsBeginImageContext(imageProcessHelperView.bounds.size);
            [imageProcessHelperView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            */
            
            [SharedNetworkManager postPictureWithImage:mRightImage.image side:Right sender:self ];
        }
    }
    else{
        rightImageURL = urlStr;
        isReady = YES;
    }
    
    if(isReady){
        NSLog(@"READY TO POST");
        NSLog(@"desc %@", mDescription.text);

        [SharedNetworkManager createPostWithImage1URL:leftImageURL
                                            Image2URL:rightImageURL
                                            itemname1:mLeftText.text
                                            itemname2:mRightText.text
                                                title:mUserQuestion.text
                                          description:mDescription.text
                                             category:[catId intValue] sender:self];
        
    }
    
    
    
}

- (void)postingSuccess:(BOOL)success{
    [HUD hide:YES];
    scrollView.userInteractionEnabled = YES;
    if(success){
        NSLog(@"clear");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload complete" message:@""
                                                       delegate:NULL cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];
        
        [self clearLayout];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Error" message:@"Please retry later"
                                                       delegate:NULL cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}



-(void)networkManagerError{
    NSLog(@"Network Error");
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}


#pragma mark - Action sheet
// =====================================================
// action sheet makeup
// =====================================================

- (void)SelectPictureActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Select From"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Camera"];
    [sheet addButtonWithTitle:@"Photos"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 2;
    
    [sheet showInView:[self.view window]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"dismiss - %d", buttonIndex);
    if(buttonIndex == 2){ // cancelled
        ;
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex)
        return;
    else if(buttonIndex == 0){
        // camera
            NSLog(@"Start camera");
        imagePicker = [[GKImagePicker alloc] init];
        imagePicker.cropSize = CGSizeMake(315, 360);
        imagePicker.delegate = self;
        imagePicker.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:imagePicker.imagePickerController animated:YES completion:^{
            NSLog(@"Start camera - finish");;
        }];
    }
    else if(buttonIndex == 1){
        // photo
        imagePicker = [[GKImagePicker alloc] init];
        imagePicker.cropSize = CGSizeMake(315, 360);
        imagePicker.delegate = self;
        imagePicker.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker.imagePickerController animated:YES completion:^{
            NSLog(@"Start camera - finish");;
        }];
    }
    else{
        NSLog(@"Cancelled");
    }
    
}

#pragma mark - imagePicker delegate
- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    [self processImage:image];
    [self hideImagePicker];
}


- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        [popoverController dismissPopoverAnimated:YES];
        
    } else {
        [imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)processImage:(UIImage*)image{
    if(mLeftImage.image == NULL){
        [self leftImageLoaded:image];
    }
    else{
        [self rightImageLoaded:image];
    }
}

#pragma mark - user question delegate

// this is for product names
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    NSString *string = textView.text;
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    textView.text = trimmedString;
    
    return YES;
}



- (void) textViewDidEndEditing:(UITextView *)textView{
//    NSLog(@"end editing -> here extend other fields");
    NSString *string = textView.text;
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    textView.text = trimmedString;
    
    if(textView.tag == DESCRIPTION){
        CGSize newSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
        int offset = newSize.height - textView.frame.size.height;
        
        if(offset != 0){
            scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + offset);
            textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y,
                                        textView.frame.size.width, newSize.height);
            
            mPictureView.frame = CGRectOffset(mPictureView.frame, 0, offset);
            mCategoryBtn.frame = CGRectOffset(mCategoryBtn.frame, 0, offset);
            mSubmitBtn.frame = CGRectOffset(mSubmitBtn.frame, 0, offset);
        }
        
    }
    
    if(textView.tag == QUESTION && textView.text.length == 0){
        mUserQuestionPlaceholder.hidden = NO;
        [mUserQuestion resignFirstResponder];
    }
    else if(textView.tag == DESCRIPTION && textView.text.length == 0){
        mDescriptionPlaceholder.hidden = NO;
        [mDescription resignFirstResponder];
    }
    
    
    //mPictureView.frame = CGRectMake(0, mUserQuestion.frame.origin.y + mUserQuestion.frame.size.height + 10, 320, mPictureView.frame.size.height);
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if(textView.tag == QUESTION && [text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if(textView.tag == QUESTION)
        mUserQuestionPlaceholder.hidden = YES;

    else if(textView.tag == DESCRIPTION)
        mDescriptionPlaceholder.hidden = YES;
    
    // text length limit
    if(textView.tag == QUESTION && textView.text.length >= 50){
        const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
        int isBackSpace = strcmp(_char, "\b");
        
        if (isBackSpace == -8) {
            return YES;
        }
        return NO;
    }
    
    return YES;
}


-(void) textViewDidChange:(UITextView *)textView
{
    if(textView.tag == QUESTION && textView.text.length == 0){
        NSLog(@"clear");
        mUserQuestionPlaceholder.hidden = NO;
        //[mUserQuestion resignFirstResponder];
    }
    else if(textView.tag == DESCRIPTION && textView.text.length == 0){
        NSLog(@"clear");
        mDescriptionPlaceholder.hidden = NO;
        //[mDescription resignFirstResponder];
    }
    
    if(textView.tag == DESCRIPTION){
        CGSize newSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
        int offset = newSize.height - textView.frame.size.height;
        
        if(offset != 0){
            scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + offset);
            textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y,
                                        textView.frame.size.width, newSize.height);
            
            mPictureView.frame = CGRectOffset(mPictureView.frame, 0, offset);
            mCategoryBtn.frame = CGRectOffset(mCategoryBtn.frame, 0, offset);
            mSubmitBtn.frame = CGRectOffset(mSubmitBtn.frame, 0, offset);
        }
        
    }
    //
    //  CGSize newSize = [textView.text sizeWithFont:textView.font
    //                            constrainedToSize:CGSizeMake(textView.frame.size.width,70)
    //                               lineBreakMode:NSLineBreakByWordWrapping];
    //
    //   textView.frame = CGRectMake(mUserQuestion.frame.origin.x, mUserQuestion.frame.origin.y, mUserQuestion.frame.size.width, newSize.height);
   

}


#pragma mark - ui
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0)
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
