//
//  PostViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKImagePicker.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"

#import "CategoryViewController.h"
#import "AppScrollView.h"

@interface PostViewController : UIViewController <UITextViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, GKImagePickerDelegate, NetworkDelegate, CategoryViewControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate>{
    
    AppScrollView *scrollView;
    
    // top section
    UILabel *mUserName;
    UITextView *mUserQuestion; // title
    UILabel *mUserQuestionPlaceholder;
    
    UITextView *mDescription;
    UILabel *mDescriptionPlaceholder;
    
    UIImageView *mUserProfile;

    // picture section
    UIView *mPictureView;
    
    UITextField *mLeftText;
    UITextField *mRightText;

    UIImageView *mLeftImage;
    UIImageView *mRightImage;
    
    UIButton *mLeftImageDelete;
    UIButton *mRightImageDelete;
    UIButton *mLeftImagePost;
    UIButton *mRightImagePost;
    
    UIButton *mMaximizeSinglePic;
    UIButton *mMinimizeSinglePic;
    
    // bottom section (category & submit)
    
    UIButton *mCategoryBtn;
    UILabel *mCategory;
    UIButton *mSubmitBtn;
    
    // other variables
    NSString *leftImageURL;
    NSString *rightImageURL;
    
    NSNumber *catId;
    
    MBProgressHUD *HUD;
    
    CGRect categoryOriginalFrame;
    CGRect submitOriginalFrame;
}

@end
