//
//  AppScrollView.h
//  Piikit
//
//  Created by Jason Kim on 13. 9. 15..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//
#import <UIKit/UIKit.h>

// custom uiscrollview
// handles dragging only.
// other events are passed on to next reposnder in the chainß
@interface AppScrollView : UIScrollView
-(id)initWithFrame:(CGRect)frame;
@end