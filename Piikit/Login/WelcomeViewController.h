//
//  WelcomeViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "NetworkManager.h"

@interface WelcomeViewController : UIViewController{
    UIButton *mSignin;
    UIButton *mRegister;
    
    UIButton *skipBtn;
    
    MBProgressHUD *HUD;    
    
}

@end
