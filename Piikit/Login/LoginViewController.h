//
//  LoginViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "NetworkManager.h"
#import "AppData.h"

#import "MBProgressHUD.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>{
    UITextField *mEmail;
    UITextField *mPassword;
    
    MBProgressHUD *HUD;
    
}

@end
