//
//  LoginViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "MainTabBarController.h"
#import "CategoryViewController.h"
#import "Profile.h"

#import "ProcessLoginViewController.h"

#import <FacebookSDK/FacebookSDK.h>

#define BACKGROUND 100
#define EMAIL 101
#define PASSWORD 102
#define SIGNIN 103
#define FACEBOOK 104

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.Title = @"Log In";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setTag:BACKGROUND];
    
    
    CGFloat relativeY = 0;
    
    if([SharedAppData screenHeight] < 568){
        relativeY -= 20;
    }
    
    UIFont *fontBold = [UIFont fontWithName:@"AudimatMonoBold" size:20.f];
    UIFont *fontRegular = [UIFont fontWithName:@"Audimat Mono" size:20.f];
    UIFont *fontSmall = [UIFont fontWithName:@"Audimat Mono" size:9.f];
    
    //UIFont *fontBold = [UIFont fontWithName:@"Audimat Mono-Bold" size:20.f];
    //UIFont *fontRegular = [UIFont fontWithName:@"Audimat Mono" size:20.f];
    //UIFont *fontSmall = [UIFont fontWithName:@"Audimat Mono" size:9.f];
    
    
    // username
    relativeY += 100;
    UIView *email = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    email.backgroundColor = [UIColor blackColor];
    email.tag = EMAIL;
    email.userInteractionEnabled = YES;
    [self.view addSubview:email];
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 120, 29)];
    NSMutableAttributedString *emailStr = [[NSMutableAttributedString alloc] initWithString:@"EMAIL"];
    [emailStr addAttribute:NSFontAttributeName value:fontBold range:NSMakeRange(0, emailStr.string.length)];
    [emailStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, emailStr.string.length)];
    [emailStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.5f] range:NSMakeRange(0, emailStr.string.length)];
    emailLabel.attributedText = emailStr;
    emailLabel.backgroundColor = [UIColor clearColor];
    [email addSubview:emailLabel];
    
    mEmail = [[UITextField alloc] initWithFrame:CGRectMake(140, 0, 160, 29)];
    mEmail.textColor = [SharedAppData themeColor];
    mEmail.font = fontRegular;
    mEmail.backgroundColor = [UIColor clearColor];
    mEmail.delegate = self;
    mEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [email addSubview:mEmail];
    relativeY += 29;
    
    relativeY += 1;
    
    // password
    UIView *password = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    password.backgroundColor = [UIColor blackColor];
    password.tag = PASSWORD;
    [self.view addSubview:password];
    
    UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 120, 29)];
    NSMutableAttributedString *passwordStr = [[NSMutableAttributedString alloc] initWithString:@"PASSWORD"];
    [passwordStr addAttribute:NSFontAttributeName value:fontBold range:NSMakeRange(0, passwordStr.string.length)];
    [passwordStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, passwordStr.string.length)];
    [passwordStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.5f] range:NSMakeRange(0, passwordStr.string.length)];
    passwordLabel.attributedText = passwordStr;
    passwordLabel.backgroundColor = [UIColor clearColor];
    passwordLabel.textColor = [UIColor whiteColor];
    [password addSubview:passwordLabel];

    mPassword = [[UITextField alloc] initWithFrame:CGRectMake(140, 0, 160, 29)];
    mPassword.textColor = [SharedAppData themeColor];
    mPassword.font = fontRegular;
    mPassword.backgroundColor = [UIColor clearColor];
    mPassword.delegate = self;
    mPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    mPassword.secureTextEntry = YES;
    [password addSubview:mPassword];
    
    relativeY += 29;
    
    // SIGN IN
    UIView *signin = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 130, 29)];
    signin.backgroundColor = [SharedAppData themeColor];
    signin.userInteractionEnabled = YES;
    signin.tag = SIGNIN;
    
    UILabel *signinLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 120, 29)];
    
    NSMutableAttributedString *signinStr = [[NSMutableAttributedString alloc] initWithString:@"SIGN IN"];
    [signinStr addAttribute:NSFontAttributeName value:fontBold range:NSMakeRange(0, signinStr.string.length)];
    [signinStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, signinStr.string.length)];
    [signinStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.5f] range:NSMakeRange(0, signinStr.string.length)];
    signinLabel.attributedText = signinStr;
    signinLabel.backgroundColor = [UIColor clearColor];
    [signin addSubview:signinLabel];
    
    [self.view addSubview:signin];
    
    UILabel *forgotPassword = [[UILabel alloc] initWithFrame:CGRectMake(140, relativeY, 120, 20)];
//    forgotPassword.text = @"FORGOT YOUR PASSWORD?";
    forgotPassword.text = @"";
    forgotPassword.font = fontSmall;
    [self.view addSubview:forgotPassword];
    
    relativeY += 29;

    UIView *connectOtherView = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 300, 29)];
    
    UILabel *connectOther = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300, 29)];
//    connectOther.textColor = [UIColor blackColor];
    NSMutableAttributedString *connectOtherStr = [[NSMutableAttributedString alloc] initWithString:@"OR SIGN IN WITH"];
    [connectOtherStr addAttribute:NSFontAttributeName value:fontRegular range:NSMakeRange(0, connectOtherStr.string.length)];
    [connectOtherStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, connectOtherStr.string.length)];
    [connectOtherStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.5f] range:NSMakeRange(0, connectOtherStr.string.length)];
    connectOther.attributedText = connectOtherStr;
    [connectOtherView addSubview:connectOther];
    [self.view addSubview:connectOtherView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self.view addSubview:line];
    
    relativeY += 29;
    
    // FACEBOOK
    UIView *facebook = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    facebook.userInteractionEnabled = YES;
    facebook.tag = FACEBOOK;
    
    UILabel *facebookLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 29)];
    NSMutableAttributedString *facebookStr = [[NSMutableAttributedString alloc] initWithString:@"FACEBOOK"];
    [facebookStr addAttribute:NSFontAttributeName value:fontRegular range:NSMakeRange(0, facebookStr.string.length)];
    [facebookStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, facebookStr.string.length)];
    [facebookStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.5f] range:NSMakeRange(0, facebookStr.string.length)];
    facebookLabel.attributedText = facebookStr;
    [facebook addSubview:facebookLabel];
    
    UIImageView *facebookImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 18, 18)];
    facebookImage.image = [UIImage imageNamed:@"facebook.png"];
    [facebook addSubview:facebookImage];
    
    [self.view addSubview:facebook];

    line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self.view addSubview:line];
    

    relativeY += 29;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self.view addSubview:line];
    
    /*
    // TWITTER
    UIView *twitter = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    UILabel *twitterLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 29)];
    NSMutableAttributedString *twitterStr = [[NSMutableAttributedString alloc] initWithString:@"TWITTER"];
    [twitterStr addAttribute:NSFontAttributeName value:fontRegular range:NSMakeRange(0, twitterStr.string.length)];
    [twitterStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, twitterStr.string.length)];
    [twitterStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.5f] range:NSMakeRange(0, twitterStr.string.length)];
    twitterLabel.attributedText = twitterStr;
    [twitter addSubview:twitterLabel];
    
    
    UIImageView *twitterImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 18, 18)];
    twitterImage.image = [UIImage imageNamed:@"twitter.png"];
    [twitter addSubview:twitterImage];
    
    
    
    [self.view addSubview:twitter];
    
    
    line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self.view addSubview:line];
    
  
    
    relativeY += 29;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 1)];
    line.backgroundColor = [UIColor blackColor];
    [self.view addSubview:line];
    */
   
    mPassword.text = @"";
    mEmail.text = @"";
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	NSLog(@"should return");
    if(textField == mEmail)
        [mPassword becomeFirstResponder];
    else
        [textField resignFirstResponder];
	return YES;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    int tag = [[[touches anyObject] view] tag];
    switch (tag) {
        case EMAIL:
            [mEmail becomeFirstResponder];
            break;
        case PASSWORD:
            [mPassword becomeFirstResponder];
            break;
        case SIGNIN:
            [self signin];
            break;
        case FACEBOOK:{
            [self processFacebookLogin];
            break;
        }
        default:
            [self.view endEditing:YES];
            
            break;
    }
    
}

- (void)agreeWithEULA{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @""
                          message: @"Do you agree with the following EULA?"
                          delegate: self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Yes", @"No", @"Read", nil];
    [alert show];
    return;
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if( buttonIndex == 0)   //yes
        [self processFacebookLogin];
    else if(buttonIndex == 1)   //no
        ;
    else    //read EULA
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.piikit.com/terms.so"]];
}

- (void)launchMain{
    
   // ConnectionManager
    //  bool login (mUsername.text, mPassword.text)
    
    // launch main
    
    
    
    CategoryViewController *categoryVC = [[CategoryViewController alloc] initWithStyle:UITableViewStylePlain];
    
    MainTabBarController *mainTabBarController = [[MainTabBarController alloc] init];
    IIViewDeckController* controller = [[IIViewDeckController alloc] initWithCenterViewController:mainTabBarController leftViewController:categoryVC];
    controller.panningMode = IIViewDeckFullViewPanning;
    controller.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    controller.navigationControllerBehavior = IIViewDeckNavigationControllerContained;

    [controller setLeftSize:50.0];
    controller.delegate = mainTabBarController;
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationController pushViewController:controller animated:YES];

}


- (void)signin{
    /*
    if(1){
        [self launchMain];
        return;
    }
    */
    if([mEmail.text isEqualToString:@""] || [mPassword.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @""
                              message: @"Please enter Email and Password"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [SharedProfile setEmail:mEmail.text];
    [SharedProfile setPassword:mPassword.text];
    
    ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
    processVC.tryLogin = YES;
    [self.navigationController pushViewController:processVC animated:NO];
    
/*
    
    [SharedNetworkManager connectWithUsername:mUsername.text password:mPassword.text sender:self];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.labelText = @"Loading";
	[HUD show:YES];
	//[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
  */  
}

/*
#pragma mark - network delagate
- (void)returnSignin:(BOOL)success{
    if(success){
        NSLog(@"SUCCESS");
        [SharedProfile setUsername:mUsername.text];
        [SharedProfile setPassword:mPassword.text];
        [SharedProfile setLogin];
    }
    else
        NSLog(@"FAIL");
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do something...
       	[HUD hide:YES];
        if(success)
            [self launchMain];
        else{
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"Sign in unsuccessful"
                                  message: @"Please check Username and Password"
                                  delegate: nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
    });
}

-(void)networkManagerError{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do something...
       	[HUD hide:YES];
    });
}
*/

- (void)processFacebookLogin{
    NSLog(@"start facebook login");
    
    ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
    processVC.facebookLogin = YES;
    [self.navigationController pushViewController:processVC animated:NO];
    
    
   // [[FBSession activeSession] closeAndClearTokenInformation];
}


@end
