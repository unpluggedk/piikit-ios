//
//  WelcomeViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "WelcomeViewController.h"
#import "AppData.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "MainTabBarController.h"
#import "CategoryViewController.h"
#import "Profile.h"

#import <CoreText/CoreText.h>

#import "ProcessLoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

// tag values
static const int SIGNIN = 101;
static const int REGISTER = 102;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //themeOrange = [UIColor colorWithRed:238./256 green:46./256 blue:36./256 alpha:1.0];
        self.title = @"Welcome";
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];

    ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
    if([SharedProfile isFacebookLogin])
        processVC.facebookLogin = YES;

    [self.navigationController pushViewController:processVC animated:NO];

    [self setupPage];
}

- (void)setupPage{
    CGFloat relativeY = -20;
    
    // top black bar
    
    if([SharedAppData screenHeight] < 568){
        relativeY -= 20;
    }
    
    relativeY += 71;
    UIView *topBar = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    topBar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topBar];
    relativeY += 29;
    
    skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    skipBtn.frame = CGRectMake(320 - 60, 4, 50, 20);
    [skipBtn addTarget:self action:@selector(skipLogin) forControlEvents:UIControlEventTouchUpInside];
    [skipBtn setImage:[UIImage imageNamed:@"skipBtn.png"] forState:UIControlStateNormal];
    [skipBtn setContentMode:UIViewContentModeScaleAspectFit];
    [topBar addSubview:skipBtn];
    
    // logo
    relativeY += 17.5;
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((320-162)/2, relativeY, 162, 65)];
    logo.image = [UIImage imageNamed:@"piikit-logo.png"];
    [self.view addSubview:logo];
    relativeY += 65;
    
    // sample image
    relativeY += 17.5;
    UIImageView *sample = [[UIImageView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 175)];
    sample.image = [UIImage imageNamed:@"image-sample.png"];
    [self.view addSubview:sample];
    relativeY += 175;
    
    // sample vote
    UIImageView *sampleVote = [[UIImageView alloc] initWithFrame:CGRectMake(0, 125, 423/2, 20)];
    sampleVote.image = [UIImage imageNamed:@"sample-vote-bar.png"];
    [sample addSubview:sampleVote];
    
    // textbar
    UIView *textBlackBar = [[UIView alloc] initWithFrame:CGRectMake(0, relativeY, 320, 29)];
    relativeY += 29;
    textBlackBar.backgroundColor = [UIColor blackColor];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,0,120,29)];
    textLabel.backgroundColor = [UIColor clearColor];
    [textBlackBar addSubview:textLabel];
    
    UILabel *textLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 155, 29)];
    textLabel2.backgroundColor = [UIColor clearColor];
    textLabel2.textAlignment = NSTextAlignmentRight;
    [textBlackBar addSubview:textLabel2];

    [self.view addSubview:textBlackBar];
    
    UIFont *bebas = [UIFont fontWithName:@"Bebas Neue" size:11.f];
    
    NSMutableAttributedString *sampleStr = [[NSMutableAttributedString alloc] initWithString:@"APR.29.13 18:35:42"];
    [sampleStr addAttribute:NSFontAttributeName value:bebas range:NSMakeRange(0, sampleStr.string.length)];
    [sampleStr addAttribute:NSForegroundColorAttributeName value:[SharedAppData themeColor] range:NSMakeRange(0, 9)];
    [sampleStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(10, 8)];
    [sampleStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:0.8f] range:NSMakeRange(0, sampleStr.string.length)];
    textLabel.attributedText = sampleStr;
    
    NSMutableAttributedString *sampleStr2 = [[NSMutableAttributedString alloc] initWithString:@"387VOTES, 178COMMENTS, 48SHARED"];
    [sampleStr2 addAttribute:NSFontAttributeName value:bebas range:NSMakeRange(0, sampleStr2.string.length)];
    int currentRunningIndex = 0;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[SharedAppData themeColor] range:NSMakeRange(currentRunningIndex, 3)];
    currentRunningIndex += 3;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(currentRunningIndex, 7)];
    currentRunningIndex += 7;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[SharedAppData themeColor] range:NSMakeRange(currentRunningIndex, 3)];
    currentRunningIndex += 3;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(currentRunningIndex, 10)];
    currentRunningIndex += 10;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[SharedAppData themeColor] range:NSMakeRange(currentRunningIndex, 2)];
    currentRunningIndex += 2;
    [sampleStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(currentRunningIndex, 6)];
    [sampleStr2 addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:0.8f] range:NSMakeRange(0, sampleStr2.string.length)];

    textLabel2.attributedText = sampleStr2;
 
    // buttons
    UIFont *audimat = [UIFont fontWithName:@"AudimatMono" size:15.f];
    
    if([SharedAppData screenHeight] < 568)
        relativeY += 20;
    else
        relativeY += 50;
    
     

    mSignin = [UIButton buttonWithType:UIButtonTypeCustom];
    mSignin.frame = CGRectMake(107.5, relativeY, 105, 29);
    //[[UILabel alloc] initWithFrame:CGRectMake(107.5, relativeY, 105, 20)];
    mSignin.backgroundColor = [UIColor blackColor];
    mSignin.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *signinStr = [[NSMutableAttributedString alloc] initWithString:@"SIGN IN ."];
    [signinStr addAttribute:NSFontAttributeName value:audimat range:NSMakeRange(0, signinStr.string.length)];
    [signinStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, signinStr.string.length)];
    [signinStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.f] range:NSMakeRange(0, signinStr.string.length)];
    mSignin.tag = SIGNIN;
    mSignin.userInteractionEnabled = YES;

    [mSignin setAttributedTitle:signinStr forState:UIControlStateNormal];
   // mSignin.bounds = CGRectMake(0,0,105,20);
    
    UIImageView *facebookIcon = [[UIImageView alloc] initWithFrame:CGRectMake(105-20, 5, 17, 17)];
    facebookIcon.image = [UIImage imageNamed:@"facebook_nobg.png"];
    [mSignin addSubview:facebookIcon];
    
    [mSignin addTarget:self action:@selector(openSigninPage) forControlEvents:UIControlEventTouchUpInside];
    relativeY+=29;
    
    relativeY+=10;
    
    
    mRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    mRegister.frame = CGRectMake(107.5, relativeY, 105, 29);
    mRegister.backgroundColor = [UIColor blackColor];
    mRegister.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    NSMutableAttributedString *regStr = [[NSMutableAttributedString alloc] initWithString:@"REGISTER"];
    [regStr addAttribute:NSFontAttributeName value:audimat range:NSMakeRange(0, regStr.string.length)];
    [regStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, regStr.string.length)];
    [regStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.5f] range:NSMakeRange(0, regStr.string.length)];
    mRegister.tag = REGISTER;
    mRegister.userInteractionEnabled = YES;

    [mRegister setAttributedTitle:regStr forState:UIControlStateNormal];
    [mRegister addTarget:self action:@selector(openRegisterPage) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:mSignin];
    [self.view addSubview:mRegister];
    
}

- (void) skipLogin{
    NSLog(@"skip Login");
    [SharedProfile setIsLoginSkipped:YES];
    
    ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
    [self.navigationController pushViewController:processVC animated:NO];
}

- (void) openSigninPage{
    NSLog(@"open sign-in");
    mSignin.backgroundColor = [UIColor blackColor];
    
    //[SharedNetworkManager testFetching];
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

- (void) openRegisterPage{
    NSLog(@"open register");
    SignupViewController *signupVC = [[SignupViewController alloc] init];
    [signupVC disablePhotoUpload];
    [self.navigationController pushViewController:signupVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [SharedProfile setIsLoginSkipped:NO];
    
   // NSLog(@"viewWillAppear");
}




@end
