//
//  ProcessLoginViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 7. 21..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ProcessLoginViewController.h"
#import "Profile.h"
#import "MainTabBarController.h"
#import "CategoryViewController.h"

#import <FacebookSDK/FacebookSDK.h>

@interface ProcessLoginViewController ()

@end

@implementation ProcessLoginViewController

@synthesize tryLogin;
@synthesize facebookLogin;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        tryLogin = false;
        facebookLogin = false;
        popToWelcome = NO;
    }
    return self;
}



- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"view will appear!!");
    if(popToWelcome)
        [self.navigationController popToRootViewControllerAnimated:NO];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    self.view.backgroundColor = [UIColor whiteColor];
    signinType = SIGNIN_OTHER;
    
    
    if([SharedProfile isLoginSkipped]){
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.labelText = @"Loading";
        
        [self launchMain];
        return;
    }
    
    
    
    
    
    
    errorMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 210, 320, 40)];
    errorMessageLabel.numberOfLines = 0;
    errorMessageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    errorMessageLabel.font = [UIFont fontWithName:@"Audimat Mono" size:13.f];
    errorMessageLabel.textAlignment = NSTextAlignmentCenter;
    errorMessageLabel.text = @"PIIKIT is under maintenance\n Please try again later";
    [self.view addSubview:errorMessageLabel];
    
    retryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    retryBtn.frame = CGRectMake(110, 270, 100, 30);
    retryBtn.backgroundColor = [UIColor blackColor];
    
    UILabel *retryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    retryLabel.font =  [UIFont fontWithName:@"Bebas Neue" size:15.f];
    retryLabel.textColor = [UIColor blackColor];
    retryLabel.backgroundColor = [UIColor whiteColor];
    retryLabel.text = @"retry";
    retryLabel.layer.borderWidth = 0.5f;
    retryLabel.layer.borderColor = [UIColor blackColor].CGColor;
    retryLabel.textAlignment = NSTextAlignmentCenter;
    
    [retryBtn addTarget:self action:@selector(processLogin) forControlEvents:UIControlEventTouchUpInside];
    
    [retryBtn addSubview:retryLabel];
    
    errorMessageLabel.hidden = YES;
    retryBtn.hidden = YES;
    
    [self.view addSubview:retryBtn];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Loading";
    
    
}


- (void)processLogin{
    [HUD show:YES];
    
    popToWelcome = YES;
    
    if(facebookLogin){
        // facebookLogin = NO;
        NSArray *permissions =
        [NSArray arrayWithObjects:@"email", @"user_birthday", @"user_location", nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                          // handle success + failure in block
                                          if (status == FBSessionStateClosed || status == FBSessionStateClosedLoginFailed){
                                              NSLog(@"facebook login failed");
                                              [HUD hide:YES];
                                              //[self.navigationController popViewControllerAnimated:NO];
                                              retryBtn.hidden = NO;
                                              errorMessageLabel.hidden = NO;
                                              [self.navigationController popViewControllerAnimated:NO];
                                          }
                                          else{
                                              NSLog(@"facebook login success!");
                                              //NSLog(@"token = %@", session.accessTokenData.accessToken);
                                              signinType = SIGNIN_FACEBOOK;
                                              [SharedProfile setFacebookLogin:YES];
                                              [SharedNetworkManager connectWithFacebook:session.accessTokenData.accessToken sender:self];
                                          }
                                      }];
        
        return;
    }
    
    // try logging in -
    if([SharedProfile isLoggedIn] || tryLogin){
        NSLog(@"LOGGED IN");
        [self signIn];
    }
    else{
        NSLog(@"NOT LOGGED IN");
        [HUD hide:YES];
        [self.navigationController popToRootViewControllerAnimated:NO];
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
    
}


- (void)viewDidAppear:(BOOL)animated{
    
    errorMessageLabel.hidden = YES;
    retryBtn.hidden = YES;
    
    [self processLogin];
}


- (void) signIn{
    
    
    NSString *username = [SharedProfile getUsername];
    NSString *email = [SharedProfile getEmail];
    NSString *password = [SharedProfile getPassword];
    
    NSLog(@"connect with these info");
    NSLog(@"=======username %@      email %@   password %@", username, email, password);
    signinType = SIGNIN_EMAIL;
    [SharedNetworkManager connectWithUsername:NULL email:email password:password sender:self];
    return;
}


#pragma mark - network delagate
- (void)returnSignin:(BOOL)success{
    
    //success = NO;
    
    [HUD hide:YES];
    
    if(success){
        [SharedProfile setLogin];
    }
    else{
        NSLog(@"FAIL");
        [SharedProfile setLogout];
    }
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do something...
       	[HUD hide:YES];
        if(success)
            [self launchMain];
        else{
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Login failed"
                                  message:@"Email or password is entered incorrectly"
                                  delegate: nil
                                  cancelButtonTitle:@"Continue"
                                  otherButtonTitles:nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    });
}

- (void)launchMain{
    CategoryViewController *categoryVC = [[CategoryViewController alloc] initWithStyle:UITableViewStylePlain];
    MainTabBarController *mainTabBarController = [[MainTabBarController alloc] init];
    
    IIViewDeckController* controller = [[IIViewDeckController alloc] initWithCenterViewController:mainTabBarController leftViewController:categoryVC];
    controller.panningMode = IIViewDeckFullViewPanning;
    controller.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    controller.navigationControllerBehavior = IIViewDeckNavigationControllerContained;
    
    [controller setLeftSize:50.0];
    controller.delegate = mainTabBarController;
    
    categoryVC.categoryViewControllerDelegate = mainTabBarController;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.navigationController pushViewController:controller animated:YES];
    
    
}


-(void)networkManagerError{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Do something...
       	[HUD hide:YES];
        
        retryBtn.hidden = NO;
        errorMessageLabel.hidden = NO;
        
        
    });
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
