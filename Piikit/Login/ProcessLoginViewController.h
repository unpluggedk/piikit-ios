//
//  ProcessLoginViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 7. 21..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#import "NetworkManager.h"

typedef enum{
    SIGNIN_FACEBOOK,
    SIGNIN_EMAIL,
    SIGNIN_OTHER
} SigninRequestType;


@interface ProcessLoginViewController : UIViewController<NetworkDelegate>{
    MBProgressHUD *HUD;
    BOOL tryLogin;
    BOOL facebookLogin;
    
    SigninRequestType signinType;
    
    UILabel *errorMessageLabel;
    UIButton *retryBtn;
    
    BOOL popToWelcome;
}



@property (nonatomic, assign) BOOL tryLogin;
@property (nonatomic, assign) BOOL facebookLogin;
@end
