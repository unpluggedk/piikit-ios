//
//  GenderCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignTextDefaultCell.h"

@interface GenderCell : SignTextDefaultCell{
    UIImageView *femaleBox;
    UIImageView *maleBox;
    
    UILabel *femaleLabel;
    UILabel *maleLabel;
    
    BOOL maleSelected;
    BOOL femaleSelected;
}

@property (nonatomic, assign) BOOL maleSelected;
@property (nonatomic, assign) BOOL femaleSelected;

- (NSString*) getSex;
- (void)toggle;

- (void)setSex:(NSString*)sex; //"M", "F"
@end
