//
//  SignupViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 7..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"

#import "ProfileCell.h"
#import "SignTextDefaultCell.h"

#import "MBProgressHUD.h"

#import "BirthdayPickerViewController.h"


@interface SignupViewController : UITableViewController <NetworkDelegate, 
                            UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,
                            SignTextDefaultCellDelegate, BirthdayPickerDelegate, UIAlertViewDelegate>
{
    int cellTypes[12];
    NSArray *title_;
   
    NSMutableArray *blackBgList;
    NSMutableArray *redBgList;
    NSMutableArray *whiteBgList;
    
    UIImage *profileImage;
    
    MBProgressHUD *HUD;
    
    BOOL disablePhotoUpload;
    BOOL disableUsernameCheck;
  
    // pointers for edit-on-fly
    UITextField *birthdayTextField;
    

}

- (void) disablePhotoUpload;
- (void) disableUsernameCheck;
@end
