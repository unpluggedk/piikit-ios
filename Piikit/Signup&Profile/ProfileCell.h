//
//  ProfileCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ProfileCellDelegate
- (void)changeProfileSelected;
@end;


@interface ProfileCell : UITableViewCell
{
    UIImageView *profileImage;
}

//- (void) setProfileImage:(UIImage*)image;

@property (nonatomic, retain) UIImageView *profileImage;
/*{
    id<ProfileCellDelegate> delegate;
 
    //private
    BOOL initialSelect;
}

@property (nonatomic, retain) id<ProfileCellDelegate> delegate;
*/
@end
