//
//  ProfileCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

@synthesize profileImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //self.imageView.image = [UIImage imageNamed:@"Profile.png"];
        
        UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(140, 0, 320-160, 125)];
        logo.image = [UIImage imageNamed:@"piikit-logo"];
        logo.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:logo];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 124, 116)];
        [self.contentView addSubview:profileImage];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void) layoutSubviews {
    
    [super layoutSubviews];
//    self.imageView.frame = CGRectMake(0,0,124,116);
}


@end
