//
//  SignTextDefaultCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 7..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignTextDefaultCellDelegate
@optional
-(void)finishEditingCell:(NSInteger)row value:(NSString*)str;
@end


@interface SignTextDefaultCell : UITableViewCell <UITextFieldDelegate>{
    UIView *bar;
    UITextField *inputField;

    UILabel *supertext;
    
    int type;
    id<SignTextDefaultCellDelegate> delegate;
    
    UIActivityIndicatorView *indicator;
    
    UIImageView *image;
    BOOL isUsername;
    BOOL isDisplayname;
    
}

@property (nonatomic, retain) id<SignTextDefaultCellDelegate> delegate;

@property (nonatomic, retain) UIView *bar;
@property (nonatomic, retain) UITextField *inputField;

@property (nonatomic, assign) int type;

@property (nonatomic, assign) BOOL isUsername;
@property (nonatomic, assign) BOOL isDisplayname;


- (void) setBarAtX:(CGFloat)x;
- (CGFloat)setTitle:(NSString*)title color:(UIColor*)color fontsize:(CGFloat)fontsize bold:(BOOL)bold;
- (CGFloat)setSupertext:(NSString *)text currentBarAt:(CGFloat)x;

- (void)startAnimating;
- (void)stopAnimating;

- (void)clearCheckmark;
- (void)setCheckmark:(BOOL)ok;
- (BOOL)isChecked;

- (boolean_t)isAnimating;

- (NSString*)getInputText;
@end
