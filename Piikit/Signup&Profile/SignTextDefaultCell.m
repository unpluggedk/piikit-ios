//
//  SignTextDefaultCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 7..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "SignTextDefaultCell.h"
#import <CoreText/CoreText.h>

@implementation SignTextDefaultCell

@synthesize bar;
@synthesize inputField;
@synthesize delegate;
@synthesize type;

@synthesize isDisplayname, isUsername;

static UIImage *okIcon = NULL;
static UIImage *notOkIcon = NULL;

static NSCharacterSet *usernameBlockedCharacters;  // alphanumeric only
static NSCharacterSet *displaynameBlockedCharacters; // alphanumeric + space

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(!okIcon){
        okIcon = [UIImage imageNamed:@"ok-icon"];
    }
    if(!notOkIcon){
        notOkIcon = [UIImage imageNamed:@"not-ok-icon"];
    }
    
    if (self) {
        // Initialization code
        isUsername = NO;
        isDisplayname = NO;
        
        NSMutableCharacterSet *displaynameBlockedCharactersHelper = [NSMutableCharacterSet characterSetWithCharactersInString:@" "];
        [displaynameBlockedCharactersHelper formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
        
        displaynameBlockedCharacters = [displaynameBlockedCharactersHelper invertedSet];
        usernameBlockedCharacters =[[NSCharacterSet alphanumericCharacterSet] invertedSet];

        UIFont *inputFieldFont = [UIFont systemFontOfSize:18.f];
        //UIFont *inputFieldFont = [UIFont fontWithName:@"AudimatMonoLight" size:18.f];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        
        bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 29)];
        bar.backgroundColor = [UIColor blackColor];
        bar.hidden = YES;
        [self.contentView addSubview:bar];
        inputField = [[UITextField alloc] initWithFrame:CGRectMake(150, 0, 170, 29)];
        inputField.font = inputFieldFont;
        inputField.delegate = self;
        [self.contentView addSubview:inputField];
        
        supertext = [[UILabel alloc] init];
        supertext.hidden = YES;
        [self.contentView addSubview:supertext];
        
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.frame = CGRectMake(290, 0, 30, 30);
        [indicator stopAnimating];
        [self.contentView addSubview:indicator];
        
        image = [[UIImageView alloc] initWithFrame:CGRectMake(295, 5, 20, 20)];
        image.hidden = YES;
        image.image = okIcon;
        [self.contentView addSubview:image];
        
        
    }
    return self;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([self.textLabel.attributedText.string isEqualToString:@"AGE"]){
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
        if (range.length == 1){
            return YES;
        }else{
            return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
        }
    }

    if(isUsername){
        if (range.length == 1){
            return YES;
        }
        return ([string stringByTrimmingCharactersInSet:usernameBlockedCharacters].length > 0);
        //return ([string rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
    }
    
    else if(isDisplayname){
        if (range.length == 1){
            return YES;
        }
        if(displaynameBlockedCharacters == nil)
            NSLog(@"nil!");
        return ([string stringByTrimmingCharactersInSet:displaynameBlockedCharacters].length > 0);
        //return ([string rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
    }
    
    return true;
}

- (void) layoutSubviews {
    
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(20, 0, 250, 29);
    self.imageView.frame = CGRectMake(290, 0, 30, 30);
    
//    self.imageView.frame = CGRectMake(20, 8, 13, 13);
}


- (void) setBarAtX:(CGFloat)x{
    bar.frame = CGRectMake(x, 0, 1, 29);
    inputField.frame = CGRectMake(x+10, 3, 300 - (x+10), 27);
}

- (CGFloat) setTitle:(NSString*)title color:(UIColor*)color fontsize:(CGFloat)fontsize bold:(BOOL)bold{
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:title];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:bold?@"AudimatMonoBold":@"AudimatMono" size:fontsize] range:NSMakeRange(0, outstr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, outstr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.f] range:NSMakeRange(0, outstr.length)];
    
    self.textLabel.attributedText = outstr;
    
    return outstr.size.width + 20 + 10;
}


- (CGFloat)setSupertext:(NSString *)text currentBarAt:(CGFloat)x {
    NSMutableAttributedString *outstr = [[NSMutableAttributedString alloc] initWithString:text];
    
    [outstr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoBold" size:9.f] range:NSMakeRange(0, outstr.length)];
    [outstr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, outstr.length)];
    [outstr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:.5f] range:NSMakeRange(0, outstr.length)];

    supertext.frame = CGRectMake(x - 10,5,outstr.size.width, outstr.size.height);
    supertext.hidden = NO;
    supertext.attributedText = outstr;
    
    return x+outstr.size.width;
    

}


- (void)doneButtonClicked:(id)Sender {
    //Write your code whatever you want to do on done button tap
    //Removing keyboard or something else
    [self resignFirstResponder];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [delegate finishEditingCell:type value:inputField.text];
}

- (void)startAnimating{
    [indicator startAnimating];
    image.hidden = YES;
}

- (void)stopAnimating{
    [indicator stopAnimating];
    
}

- (boolean_t)isAnimating{
    return [indicator isAnimating];
}

- (void)clearCheckmark{
    image.hidden = YES;
}

- (void)setCheckmark:(BOOL)ok{
    image.hidden = NO;
    if(ok)
        image.image = okIcon;
    else
        image.image = notOkIcon;
}

- (BOOL)isChecked{
    if(image.image == okIcon)
        return YES;
    return NO;

}

- (NSString*)getInputText{
    if(self.inputField.text != NULL)
        return [NSString stringWithString:inputField.text];
    return NULL;
}

@end
