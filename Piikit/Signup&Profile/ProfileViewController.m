//
//  ProfileViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "ProfileViewController.h"
#import "IIViewDeckController.h"
#import "AppData.h"

#import "GenderCell.h"
#import "ProfileCell.h"
#import "FacebookCell.h"

#import "Profile.h"

#import "UIImageView+WebCache.h"

#import <QuartzCore/QuartzCore.h>
#import "ProcessLoginViewController.h"

#import "NeedToLoginView.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Profile";
        
        imageProcessHelperView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 400)];
        imageProcessHelperView.contentMode = UIViewContentModeScaleAspectFit;
        imageProcessHelperView.backgroundColor = [UIColor blackColor];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Reload"
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(reloadProfile)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    if([SharedProfile isLoginSkipped]){
        rightButton.enabled = NO;
        self.tableView.dataSource = NULL;
        
        NeedToLoginView *view = [[NeedToLoginView alloc] initWithFrame:self.tableView.frame];
        [view.loginBtn addTarget:self action:@selector(popToMain) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:view];
    }
    
    
}


- (void)popToMain{
    [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
}


- (void)reloadProfile{
    NSLog(@"reload profile");
    
    [HUD show:YES];
    [SharedNetworkManager getProfileWithSuccessPopUp:@"Profile successfully reloaded"
                                              sender:self];
}

- (void)viewWillAppear:(BOOL)animated{
    self.title = @"Profile";
    
    self.tabBarController.viewDeckController.panningMode = IIViewDeckNoPanning;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [super viewWillAppear:animated];

    //if(profileImageView)
    //    [profileImageView setImageWithURL:[NSURL URLWithString:[SharedProfile profileURLString]] placeholderImage:[UIImage imageNamed:@"PROFILE-ICON"]];
    
    [self disableUsernameCheck];
    
    /*    
    if([SharedProfile isLoginSkipped]){
        self.tableView.userInteractionEnabled = NO;
    }
    */
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

static const int DEFAULT = 0;
static const int PROFILE = 1;
static const int GENDER = 2;
static const int PASSWORD = 3;
static const int PASSWORDCONFIRM = 4;
static const int FACEBOOK = 5;

- (void) initDatasource{
    if([SharedAppData screenHeight] < 568){
        title_ = [NSArray arrayWithObjects:@"", @"USERNAME", @"EMAIL", 
                  @"BIRTHDAY", @"SEX", @"", @"SAVE YOUR EDIT", @"SIGN OUT", @"", nil];
        cellTypes[0] = PROFILE;
        cellTypes[1] = DEFAULT;
        cellTypes[2] = DEFAULT;
        cellTypes[3] = DEFAULT;
        cellTypes[4] = GENDER;
        cellTypes[5] = DEFAULT;
        cellTypes[6] = DEFAULT;
        cellTypes[7] = DEFAULT;
       // cellTypes[7] = FACEBOOK;
    }
    else{
        title_ = [NSArray arrayWithObjects:@"", @"CHANGE YOUR PHOTO", @"", @"USERNAME", @"EMAIL", 
                  @"BIRTHDAY", @"SEX", @"", @"SAVE YOUR EDIT", @"SIGN OUT", @"", nil];
        cellTypes[0] = DEFAULT;
        cellTypes[1] = DEFAULT;
        cellTypes[2] = PROFILE;
        cellTypes[3] = DEFAULT;
        cellTypes[4] = DEFAULT;

        cellTypes[5] = DEFAULT;
        cellTypes[6] = GENDER;
        cellTypes[7] = DEFAULT;
        cellTypes[8] = DEFAULT;
        cellTypes[9] = DEFAULT;

        //cellTypes[9] = FACEBOOK;
    }
    [whiteBgList addObject:@""];
    [whiteBgList addObject:@"CHANGE YOUR PHOTO"];
    [redBgList addObject:@"SAVE YOUR EDIT"];
    [blackBgList addObject:@"SIGN OUT"];
    
    //= {DEFAULT, DEFAULT, PROFILE, DEFAULT, DEFAULT, PASSWORD, PASSWORDCONFIRM, DEFAULT, GENDER, DEFAULT, DEFAULT, FACEBOOK};
}

- (NSString*)getNewUsername{
    int count = [title_ count];
    int objectIndex;
    
    for(int i=0; i<count; i++){
        if([[title_ objectAtIndex:i] caseInsensitiveCompare:@"USERNAME"] == NSOrderedSame)
            objectIndex = i;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:objectIndex inSection:0];
    SignTextDefaultCell *cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:index];
    
    return [cell inputField].text;
}

- (NSString*)getNewEmail{
    int count = [title_ count];
    int objectIndex;
    
    for(int i=0; i<count; i++){
        if([[title_ objectAtIndex:i] caseInsensitiveCompare:@"EMAIL"] == NSOrderedSame)
            objectIndex = i;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:objectIndex inSection:0];
    SignTextDefaultCell *cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:index];

    if(![cell isChecked])
        return NULL;
    return [cell inputField].text;
}

- (NSString*)getNewAge{
    int count = [title_ count];
    int objectIndex;
    
    for(int i=0; i<count; i++){
        if([[title_ objectAtIndex:i] caseInsensitiveCompare:@"BIRTHDAY"] == NSOrderedSame)
            objectIndex = i;
    }
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:objectIndex inSection:0];
    SignTextDefaultCell *cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:index];
    
    return [cell inputField].text;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(cellTypes[indexPath.row] == PROFILE)
        return 116;
    return 29;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if([SharedAppData screenHeight] < 568)
        return 8;
    else
        return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[6] = {@"defaulttext-cell", @"profile-cell", @"gender-cell", @"password-cell", @"passwordconfirm-cell", @"facebook-cell"};
    NSString *identifier = identifiers[cellTypes[indexPath.row]];
    
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil){
        switch (cellTypes[indexPath.row]) {
            case DEFAULT:
                cell = [[SignTextDefaultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                [((SignTextDefaultCell*)cell) setType:indexPath.row];
                ((SignTextDefaultCell*)cell).delegate = self;
                break;
            case GENDER:
                cell = [[GenderCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            case PROFILE:{
                cell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                profileImageView = ((ProfileCell*)cell).profileImage;
                break;
            }
            case FACEBOOK:
                cell = [[FacebookCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
                
            default:
                cell = [[SignTextDefaultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    if(cellTypes[indexPath.row] == DEFAULT ||
       cellTypes[indexPath.row] == PASSWORD ||
       cellTypes[indexPath.row] == PASSWORDCONFIRM ||
       cellTypes[indexPath.row] == GENDER){
        
        SignTextDefaultCell *currentCell = (SignTextDefaultCell*)cell;
        NSString *title = [title_ objectAtIndex:indexPath.row];
        
        // background colors
        if( [self containsString:title list:whiteBgList] ||
           [self containsString:title list:redBgList] ||
           [self containsString:title list:blackBgList]){
            currentCell.bar.hidden = YES;
            currentCell.inputField.hidden = YES;
            
            if( [self containsString:title list:blackBgList]){
                currentCell.contentView.backgroundColor = [UIColor blackColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor whiteColor] fontsize:18.f bold:YES];
            }
            
            else if( [self containsString:title list:redBgList]){
                currentCell.contentView.backgroundColor = [SharedAppData themeColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor whiteColor] fontsize:18.f bold:YES];
            }
            
            else{
                currentCell.contentView.backgroundColor = [UIColor whiteColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor blackColor] fontsize:18.f bold:YES];
                
            }
        }
        else{
            currentCell.contentView.backgroundColor = [UIColor whiteColor];
            
            // input type
            if( [title isEqualToString:@"BIRTHDAY"]){
                currentCell.inputField.text = [SharedProfile birthdate];
                currentCell.inputField.userInteractionEnabled = NO;
                
                birthdayTextField = currentCell.inputField;
            }
            
            // gender
            else if( [title isEqualToString:@"SEX"] ){
                currentCell.inputField.hidden = YES;
                GenderCell *cell = (GenderCell*)currentCell;
                [cell setSex:[SharedProfile sex]];
                
                sex = [cell getSex];  // "M", "F"
                

            }
            
            else if( [title isEqualToString:@"USERNAME"] ){
                currentCell.inputField.text = [SharedProfile displayname];
                currentCell.isDisplayname = YES;
                
  //              currentCell.inputField.userInteractionEnabled = NO;
            }

            else if( [title isEqualToString:@"EMAIL"] ){
                currentCell.inputField.text = [SharedProfile getEmail];
                [currentCell clearCheckmark];
    //            currentCell.inputField.userInteractionEnabled = NO;
            }
            
            // set up bar
            CGFloat x = [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor blackColor] fontsize:18.f bold:NO] + 5;
            
            currentCell.bar.hidden = NO;
            [currentCell setBarAtX:x];
            currentCell.inputField.hidden = NO;
           

        }
        
        
        
    }
    
    
    if(cellTypes[indexPath.row] == PROFILE){
        //ProfileCell *profileCell = (ProfileCell*)cell;
        //profileCell.profileImage.image = profileImage;
        
        [profileImageView setImageWithURL:[NSURL URLWithString:[SharedProfile profileURLString]] placeholderImage:[UIImage imageNamed:@"PROFILE-ICON"]];
        
    }
    
    return cell;
}

- (BOOL) containsString:(NSString*)string list:(NSArray*)list{
    for(NSString *str in list){
        if([str isEqualToString:string])
            return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSLog(@"SELECTED");
    if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"SEX"]){
        GenderCell* cell = (GenderCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell toggle];
        
        sex = [cell getSex];  // "M", "F"
        
    }
    else if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"SIGN OUT"]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SIGN OUT"
                                                        message:@"Are you sure you want to log out?"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Yes", @"No", nil];
        alert.cancelButtonIndex = 1;
        [alert show];

        
        
       
        
    }
    
    else if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"BIRTHDAY"]){

        BirthdayPickerViewController *datePicker = [[BirthdayPickerViewController alloc] init];
        //[self presentModalViewController:datePicker animated:YES];
        datePicker.delegate = self;
        self.title = @"Back";
        
        [self.navigationController pushViewController:datePicker animated:YES];
    }
    
    
    else if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"SAVE YOUR EDIT"]){
        
        NSString *newUsername = [self getNewUsername];
        //NSString *newAge = [self getNewAge];
        NSString *newEmail = [self getNewEmail];

        /*
        if(getNewEmail == NULL)
            NSLog(@"email not valid");
        */
        
        //NSMutableString *ageStr = [NSMutableString stringWithString:[SharedProfile birthdate]];
        //ageStr replaceCharactersInRange:<#(NSRange)#> withString:<#(NSString *)#>
        
        
        [HUD show:YES];
        NSLog(@"save edit!");
       [SharedNetworkManager editProfileWithUsername:[SharedProfile getUsername]
                                             password:[SharedProfile getPassword]
                                          newPassword:[SharedProfile getPassword]
                                          displayname:newUsername
                                            birthdate:birthdayTextField.text
                                            joindate:[SharedProfile joindate]
                                                 sex:sex
                                             country:[SharedProfile country]
                                               email:newEmail
                                              sender:self];
        
        
     
    }
    
    
    if([SharedAppData screenHeight] < 568){
        if(indexPath.row == 0)
            [self SelectPictureActionSheet];
    }
    else{
        if(indexPath.row == 1 || indexPath.row == 2)
            [self SelectPictureActionSheet];
    }
        
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [SharedProfile setLogout];
        
        if([SharedProfile isFacebookLogin]){
            NSLog(@"CLEAR FACEBOOK");
            [[FBSession activeSession] closeAndClearTokenInformation];
            [SharedProfile setFacebookLogin:NO];
        }

        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];

    }
}



#pragma mark - Profile picture change


- (void)SelectPictureActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Select From"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Camera"];
    [sheet addButtonWithTitle:@"Photos"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 2;
    
    [sheet showInView:self.view.window];
}


/*
 - (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
 NSLog(@"dismiss");
 if(buttonIndex == 2){
 if(mRightImage.image == NULL){
 [UIView animateWithDuration:0.5
 animations:^{
 mLeftImage.frame = CGRectMake(0, 30, 310, 200-30);
 } completion:^(BOOL finished) {
 ;
 }];
 }
 
 }
 }
 */

//UIImagePickerController *picker;
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex)
        return;
    else if(buttonIndex == 0){
        // camera
        NSLog(@"Start camera");
        imagePicker = [[GKImagePicker alloc] init];
        imagePicker.cropSize = CGSizeMake(310, 310);
        imagePicker.delegate = self;
        imagePicker.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker.imagePickerController animated:YES completion:^{
            NSLog(@"Start camera - finish");;
        }];

        /* original : use simple iOS built-in
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:^{
                ;
            }];
        }
        */
    }
    else if(buttonIndex == 1){
        // photo
        imagePicker = [[GKImagePicker alloc] init];
        imagePicker.cropSize = CGSizeMake(310, 310);
        imagePicker.delegate = self;
        
        imagePicker.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker.imagePickerController animated:YES completion:^{
            NSLog(@"Start camera - finish");;
        }];
        
        /*
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:^{
                ;
            }];
        }
         */
    }
    else{
        ;
    }
}

#pragma mark- imagePicker delegate
- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    [self processImage:image];
    [self hideImagePicker];
}

- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        [popoverController dismissPopoverAnimated:YES];
        
    } else {
        [imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)processImage:(UIImage*)image{
    
    imageProcessHelperView.image = image;
    
    UIGraphicsBeginImageContext(imageProcessHelperView.bounds.size);
    [imageProcessHelperView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    /*
    profileImage = viewImage;
    
    [self.tableView reloadData];
    */

    [SharedNetworkManager postProfilePictureWithImage:viewImage sender:self];
}





- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    profileImage = image;
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tableView reloadData];
    }];
    /*
     
     [self dismissViewControllerAnimated:YES completion:^{
     if(mRightImage.image != NULL){
     [UIView animateWithDuration:1.0
     animations:^{
     mLeftImage.frame = CGRectMake(0, 30, 155, 200-30);
     //mCameraBtn.frame = CGRectOffset(mCameraBtn.frame, 100, 0);
     }
     completion:^(BOOL finished) {
     ;
     }];
     }
     */
    
    
    //    [NSThread detachNewThreadSelector:@selector(addPageWithImage:) toTarget:self withObject:image];
    
}


-(void)returnProfilePostedURL:(NSString*)urlStr{
    NSLog(@"profileViewController - update pic %@", urlStr);
    if(profileImageView){
        profileImageView.contentMode = UIViewContentModeScaleAspectFit;
       [profileImageView setImageWithURL:[NSURL URLWithString:urlStr]];
    }
    [SharedProfile setProfileURLString:urlStr];

}
/*
#pragma mark - birthday picker delegate
- (void)updateBirthday:(NSString*)dateStr{
    NSLog(@"returned %@", dateStr);
    birthdayTextField.text = dateStr;
}
*/
#pragma mark - network delegate
- (void)returnProfileUpdate:(BOOL)success{
    if(!success){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Please retry in few moments"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [HUD hide:YES];
    }
    else{
        [SharedNetworkManager getProfileWithSuccessPopUp:@"Profile successfully updated" sender:self];
    }
}

- (void)returnProfile:(BOOL)success displayname:(NSString *)displaynameStr birthday:(NSString *)birthdayStr email:(NSString *)emailStr sex:(NSString *)sexStr successPopUp:(NSString *)successPopUp{
    [HUD hide:YES];
    
    if(!success){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Please retry again after reload."
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [SharedProfile setDisplayname:displaynameStr];
    [SharedProfile setBirthdate:birthdayStr];
    [SharedProfile setSex:sexStr];
    [SharedProfile setEmail:emailStr];
    
    [self.tableView reloadData];
    
    if(successPopUp){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Profile Loaded"
                                                        message:successPopUp
                                                       delegate: nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    return;
    
}

@end
