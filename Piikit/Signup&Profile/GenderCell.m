//
//  GenderCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "GenderCell.h"
#import <CoreText/CoreText.h>

@implementation GenderCell

@synthesize maleSelected;
@synthesize femaleSelected;

#define MALE 101
#define FEMALE 102

UIImage *checkedBox;
UIImage *uncheckedBox;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        checkedBox = [UIImage imageNamed:@"checked-box.png"];
        uncheckedBox = [UIImage imageNamed:@"unchecked-box.png"];
        
        femaleBox = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
        femaleBox.tag = FEMALE;
        [femaleBox setImage:checkedBox];
        femaleBox.hidden = NO;
        //femaleBox.userInteractionEnabled = YES;
        [self.contentView addSubview:femaleBox];
        
        maleBox = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 14, 14)];
        maleBox.tag = MALE;
        [maleBox setImage:uncheckedBox];
        maleBox.hidden = NO;
       // maleBox.userInteractionEnabled = YES;
        [self.contentView addSubview:maleBox];
        
        femaleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        femaleLabel.tag = FEMALE;
       // femaleLabel.userInteractionEnabled = YES;
        
        maleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        maleLabel.tag = MALE;
       // maleLabel.userInteractionEnabled = YES;
        [self.contentView addSubview:femaleLabel];
        [self.contentView addSubview:maleLabel];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        femaleSelected = YES;
        maleSelected = NO;
        
        self.inputField.userInteractionEnabled = NO;
    }
    return self;
}

- (void)setSex:(NSString*)sex{
    if([sex compare:@"M"] == NSOrderedSame){
        maleSelected = YES;
        femaleSelected = NO;
        maleBox.image = checkedBox;
        femaleBox.image = uncheckedBox;
    }
    else{
        maleSelected = NO;
        femaleSelected = YES;
        maleBox.image = uncheckedBox;
        femaleBox.image = checkedBox;
    }
    
}

- (void)toggle{
    if(maleSelected){
        maleSelected = NO;
        femaleSelected = YES;
        maleBox.image = uncheckedBox;
        femaleBox.image = checkedBox;
    }
    else{
        maleSelected = YES;
        femaleSelected = NO;
        maleBox.image = checkedBox;
        femaleBox.image = uncheckedBox;
    }
}
/*
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    int tag = [[[touches anyObject] view] tag];
    NSLog(@"QWEQWE");
    switch (tag) {
        case MALE:
            if(maleSelected){
                maleSelected = NO;
                femaleSelected = YES;
                maleBox.image = uncheckedBox;
                femaleBox.image = checkedBox;
            }
            else{
                maleSelected = YES;
                femaleSelected = NO;
                maleBox.image = checkedBox;
                femaleBox.image = uncheckedBox;

            }
            break;
        case FEMALE:
            if(femaleSelected){
                femaleSelected = NO;
                maleSelected = YES;
                femaleBox.image = uncheckedBox;
                maleBox.image = checkedBox;
            }
            else{
                femaleSelected = YES;
                maleSelected = NO;
                femaleBox.image = checkedBox;
                maleBox.image = uncheckedBox;
                
            }

            break;
        default:
            break;
    }
}
*/


- (void) setBarAtX:(CGFloat)x{
    bar.frame = CGRectMake(x, 0, 1, 29);
    
    x+=10;
    femaleBox.frame = CGRectMake(x, 7.5, 14, 14);
    x+=14;
    
    x+=5;
    NSMutableAttributedString *femaleStr = [[NSMutableAttributedString alloc] initWithString:@"FEMALE"];
    [femaleStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoLight" size:18.f] range:NSMakeRange(0, femaleStr.string.length)];
    [femaleStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, femaleStr.string.length)];
    [femaleStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:0.3f] range:NSMakeRange(0, femaleStr.string.length)];

    femaleLabel.frame = CGRectMake(x,0,femaleStr.size.width,  29);
    femaleLabel.attributedText = femaleStr;
    
    x+= femaleStr.size.width + 15;
    maleBox.frame = CGRectMake(x, 7.4, 14, 14);
    x+= 14;
    
    x+=5;
    NSMutableAttributedString *maleStr = [[NSMutableAttributedString alloc] initWithString:@"MALE"];
    [maleStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMonoLight" size:18.f] range:NSMakeRange(0, maleStr.string.length)];
    [maleStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, maleStr.string.length)];
    [maleStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:0.3f] range:NSMakeRange(0, maleStr.string.length)];
    
    maleLabel.frame = CGRectMake(x,0,maleStr.size.width,  29);
    maleLabel.attributedText = maleStr;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  //    [super setSelected:selected animated:animated];
  // Configure the view for the selected state
}

- (NSString*) getSex{
    if(self != NULL)
        return maleSelected?@"M":@"F";
    return NULL;
}

@end
