//
//  ProfileViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "SignupViewController.h"
#import "GKImagePicker.h"

#import "BirthdayPickerViewController.h"

#import "NetworkManager.h"

@interface ProfileViewController : SignupViewController <UIAlertViewDelegate, GKImagePickerDelegate>{
    UIImageView *imageProcessHelperView;
    
    GKImagePicker *imagePicker;
    UIPopoverController *popoverController;
 
    // pointers for edit-on-fly
    UIImageView *profileImageView;

    
    // saved sex
    NSString *sex;

    
}

@end
