//
//  BirthdayPickerViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 10. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "BirthdayPickerViewController.h"
#import "Profile.h"

@interface BirthdayPickerViewController ()

@end

@implementation BirthdayPickerViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Select Birthday";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =  [UIColor colorWithRed:0.875 green:0.88 blue:0.91 alpha:1];    

	// Do any additional setup after loading the view.
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(donePressed)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    picker = [[UIDatePicker alloc] init];
    picker.datePickerMode = UIDatePickerModeDate;
    picker.frame = CGRectOffset(picker.frame, 0, 100);
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    [comp setDay:0];
    [comp setMonth:0];
    [comp setYear:-99];
    
    NSDate *minDate = [calendar dateByAddingComponents:comp toDate:[NSDate date] options:0];
    NSDate *maxDate = [NSDate date];
    
    picker.minimumDate = minDate;
    picker.maximumDate = maxDate;
    
    NSDateFormatter *fulldateFormat = [[NSDateFormatter alloc] init];
    fulldateFormat.dateFormat = @"yyyy-MM-dd";
    
    if([SharedProfile birthdate])
        picker.date = [fulldateFormat dateFromString:[SharedProfile birthdate]];
    
    [self.view addSubview:picker];
}

- (void)donePressed{
    NSDate *dateFromPicker = [picker date];
    
    NSDateFormatter *fulldateFormat = [[NSDateFormatter alloc] init];
    fulldateFormat.dateFormat = @"yyyy-MM-dd";
    
#ifdef IGNORED
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit;// | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *comp = [calendar components:unitFlags fromDate:dateFromPicker];
    int year = [comp year];
#endif
    
    [delegate updateBirthday:[fulldateFormat stringFromDate:dateFromPicker]];
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [super viewWillAppear:animated];
    
    
}


@end
