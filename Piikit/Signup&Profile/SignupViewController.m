//
//  SignupViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 7..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "SignupViewController.h"
#import "AppData.h"
#import "GenderCell.h"
#import "ProfileCell.h"
#import "FacebookCell.h"

#import "Profile.h"

#import "ProcessLoginViewController.h"


@interface SignupViewController ()

@end

@implementation SignupViewController

bool pageLoaded = false;

- (void) disablePhotoUpload{
    disablePhotoUpload = YES;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        blackBgList = [[NSMutableArray alloc] init];
        redBgList = [[NSMutableArray alloc] init];
        whiteBgList  = [[NSMutableArray alloc] init];

        profileImage = [UIImage imageNamed:@"Profile.png"];
        
        disablePhotoUpload = NO;
        disableUsernameCheck = NO;
        
        [self initDatasource];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;


    self.tableView.separatorColor = [UIColor blackColor];

    pageLoaded = true;

    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = @"Loading";
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    self.tableView.tableFooterView = view;
    
}


- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([SharedAppData screenHeight] < 568 && indexPath.row == 1)
        return 10;
    
    if(cellTypes[indexPath.row] == PROFILE)
        return 116;
    return 29;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [title_ count];
}



static const int DEFAULT = 0;
static const int PROFILE = 1;
static const int GENDER = 2;
static const int PASSWORD = 3;
static const int PASSWORDCONFIRM = 4;
static const int FACEBOOK = 5;

- (void) initDatasource{
    title_ = [NSArray arrayWithObjects:@"SIGN UP", @"", @"USERNAME", @"EMAIL", @"PASSWORD", @"BIRTHDAY", @"SEX", @"SIGN UP NOW!", @"CONNECT WITH", @"", nil];
    cellTypes[0] = DEFAULT; // SIGN UP
    cellTypes[1] = DEFAULT; // EMPTY CELL
    cellTypes[2] = DEFAULT; // USERNAME
    cellTypes[3] = DEFAULT;// EMAIL
    cellTypes[4] = PASSWORD; // PASSWORD
    cellTypes[5] = DEFAULT;  // BIRTHDAY
    cellTypes[6] = GENDER; // SEX
    cellTypes[7] = DEFAULT; //
    cellTypes[8] = DEFAULT; //
    cellTypes[9] = FACEBOOK;
    //cellTypes[10] = DEFAULT;
    //cellTypes[11] = FACEBOOK;

    [blackBgList addObject:@"SIGN UP"];
    [whiteBgList addObject:@"ADD YOUR PHOTO"];
    [blackBgList addObject:@"CONNECT WITH"];
    [redBgList addObject:@"SIGN UP NOW!"];
    
}

- (BOOL) containsString:(NSString*)string list:(NSArray*)list{
    for(NSString *str in list){
        if([str isEqualToString:string])
            return YES;
    }
    return NO;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[6] = {@"defaulttext-cell", @"profile-cell", @"gender-cell", @"password-cell", @"passwordconfirm-cell", @"facebook-cell"};
    NSString *identifier = identifiers[cellTypes[indexPath.row]];
   
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil){
        switch (cellTypes[indexPath.row]) {
            case DEFAULT:
                cell = [[SignTextDefaultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                [((SignTextDefaultCell*)cell) setType:indexPath.row];
                ((SignTextDefaultCell*)cell).delegate = self;
                break;
            case GENDER:
                cell = [[GenderCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            case PROFILE:
                cell = [[ProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
            case FACEBOOK:
                cell = [[FacebookCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;

            default:
                cell = [[SignTextDefaultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                break;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    }
    
    if(indexPath.row == 1)
        cell.userInteractionEnabled = NO;
    
    if(cellTypes[indexPath.row] == DEFAULT ||
       cellTypes[indexPath.row] == PASSWORD ||
       cellTypes[indexPath.row] == PASSWORDCONFIRM ||
       cellTypes[indexPath.row] == GENDER){
        
        SignTextDefaultCell *currentCell = (SignTextDefaultCell*)cell;
        NSString *title = [title_ objectAtIndex:indexPath.row];

        // background colors
        if( [self containsString:title list:whiteBgList] ||
           [self containsString:title list:redBgList] ||
           [self containsString:title list:blackBgList]){
            currentCell.bar.hidden = YES;
            currentCell.inputField.hidden = YES;

            if( [self containsString:title list:blackBgList]){
                currentCell.contentView.backgroundColor = [UIColor blackColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor whiteColor] fontsize:18.f bold:YES];
            }
       
            else if( [self containsString:title list:redBgList]){
                currentCell.contentView.backgroundColor = [SharedAppData themeColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor whiteColor] fontsize:18.f bold:YES];
            }
            
            else{
                currentCell.contentView.backgroundColor = [UIColor whiteColor];
                [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor blackColor] fontsize:18.f bold:YES];
                
            }
        }
        else{
            currentCell.contentView.backgroundColor = [UIColor whiteColor];
            
            // input type
            if( [title isEqualToString:@"BIRTHDAY"]){
//                currentCell.inputField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                currentCell.inputField.text = @"";
                currentCell.inputField.userInteractionEnabled = NO;
                birthdayTextField = currentCell.inputField;
                
//                currentCell.userInteractionEnabled = NO;
            }
            // gender
            else if( [title isEqualToString:@"SEX"] ){
                currentCell.inputField.hidden = YES;
                
            }
            
            // username
            else if( [title isEqualToString:@"USERNAME"]){
                currentCell.isUsername = YES;
            }
            
            // set up bar
            CGFloat x = [currentCell setTitle:[title_ objectAtIndex:indexPath.row] color:[UIColor blackColor] fontsize:18.f bold:NO] + 5;

            currentCell.bar.hidden = NO;
            [currentCell setBarAtX:x];
            currentCell.inputField.hidden = NO;
         
            // secure entry
            if( [title isEqualToString:@"PASSWORD"]){
                currentCell.inputField.secureTextEntry = YES;
                if(cellTypes[indexPath.row] == PASSWORDCONFIRM){
                    x = [currentCell setSupertext:@"CONFIRM" currentBarAt:x];
                    [currentCell setBarAtX:x];
                }
            }
                        
         
            if([title isEqualToString:@""])
                 currentCell.bar.hidden = YES;
        }

        
        
    }
   

    if(cellTypes[indexPath.row] == PROFILE){
     //   cell.imageView.image = profileImage;
       // [((ProfileCell*)cell) setProfileImage:profileImage];
        ((ProfileCell*)cell).profileImage.image = profileImage;

        
        
    }


    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

NSString *mPassword;
NSString *mPasswordConfirm;
NSString *mAge;
NSString *mUsername;
NSString *mEmail;
NSString *mSex;

BOOL mIsEULAChecked = NO;
NSIndexPath* mSelectedIndexPath = nil;


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];

    mSelectedIndexPath = indexPath;
    
    if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"SIGN UP NOW!"]){
        NSLog(@"SIGN UP");
       
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
        UITableViewCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
       
        // get username
        indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        mUsername = [(SignTextDefaultCell*)cell getInputText];
        
        // get email
        indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        mEmail = [(SignTextDefaultCell*)cell getInputText];
        
        // check password length
        indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        mPassword = [(SignTextDefaultCell*)cell getInputText];
        if([mPassword length] < 4){
            NSLog(@"Password too short");
            [self showAlertWithTitle:@"Check Password" message:@"Password must be over 4 characters"];
            return;
        }
        
        // check password match
        /*
        indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
        cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        
        mPasswordConfirm = [(SignTextDefaultCell*)cell getInputText];
        
       
       
        if(![mPasswordConfirm isEqualToString:mPassword]){
            [self showAlertWithTitle:@"Check Password" message:@"Password confirmation do not match"];
            NSLog(@"password do not match");
            return;
        }
        */
        
        indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
        cell = (GenderCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        mSex = [(GenderCell*)cell getSex];

        if([birthdayTextField.text isEqualToString:@""]){
            [self showAlertWithTitle:@"Check Birthday" message:@"Please select Birthday"];
            return;
        }
        
        if(mIsEULAChecked){
            [SharedNetworkManager createAccount:mUsername password:mPassword email:mEmail birthdate:birthdayTextField.text sex:mSex country:NULL receiveupdates:@"T" status:@"V" sender:self];
            [HUD show:YES];
        }
        else{
            [self agreeWithEULA];
        }
    }
    
    else if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"SEX"]){
        GenderCell* cell = (GenderCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell toggle];
        
    }
    
    else if([[title_ objectAtIndex:indexPath.row] isEqualToString:@"BIRTHDAY"]){
        BirthdayPickerViewController *datePicker = [[BirthdayPickerViewController alloc] init];
        //[self presentModalViewController:datePicker animated:YES];
        datePicker.delegate = self;
        self.title = @"Back";
        
        [self.navigationController pushViewController:datePicker animated:YES];
        
    }
    
    else if(indexPath.row == 2 && !disablePhotoUpload){
        [self SelectPictureActionSheet];
    }
    
    else if(indexPath.row == [title_ count] - 1){
        if(mIsEULAChecked){
            ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
            processVC.facebookLogin = YES;
            [self.navigationController pushViewController:processVC animated:NO];
        }
        else{
            [self agreeWithEULA];
        }
        
    }

}


#pragma mark - Profile picture change


- (void)SelectPictureActionSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    [sheet setTitle:@"Select From"];
    [sheet setActionSheetStyle:UIActionSheetStyleDefault];
    sheet.delegate = self;
    [sheet addButtonWithTitle:@"Camera"];
    [sheet addButtonWithTitle:@"Photos"];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.cancelButtonIndex = 2;
    
    [sheet showInView:self.view.window];
}


/*
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"dismiss");
    if(buttonIndex == 2){
        if(mRightImage.image == NULL){
            [UIView animateWithDuration:0.5
                             animations:^{
                                 mLeftImage.frame = CGRectMake(0, 30, 310, 200-30);
                             } completion:^(BOOL finished) {
                                 ;
                             }];
        }
        
    }
}
*/

UIImagePickerController *picker;
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex)
        return;
    else if(buttonIndex == 0){
        // camera
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:^{
                ;
            }];
        }
    }
    else if(buttonIndex == 1){
        // photo
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        if([UIImagePickerController isSourceTypeAvailable:sourceType]){
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:^{
                ;
            }];
        }
    }
    else{
        ;
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    profileImage = image;
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tableView reloadData];
    }];
    /*
     
     [self dismissViewControllerAnimated:YES completion:^{
     if(mRightImage.image != NULL){
     [UIView animateWithDuration:1.0
     animations:^{
     mLeftImage.frame = CGRectMake(0, 30, 155, 200-30);
     //mCameraBtn.frame = CGRectOffset(mCameraBtn.frame, 100, 0);
     }
     completion:^(BOOL finished) {
     ;
     }];
     }
     */
    
    
    //    [NSThread detachNewThreadSelector:@selector(addPageWithImage:) toTarget:self withObject:image];
    
}


#pragma mark - finish editing cell
- (void)finishEditingCell:(NSInteger)row value:(NSString *)str{
    NSLog(@"finish editing");
    //NSLog([NSString stringWithFormat:@"%@ - type:%d", str, type]);
    if(!disableUsernameCheck && [[title_ objectAtIndex:row] caseInsensitiveCompare:@"username"] == NSOrderedSame){    //    row == 3){  // username
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SignTextDefaultCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];

        if([str isEqualToString:@""]){
            [cell setCheckmark:NO];
            return;
        }
        
        [SharedNetworkManager checkUsernameAvailable:str sender:self];
        [cell startAnimating];
    }
    
    else if([[title_ objectAtIndex:row] caseInsensitiveCompare:@"email"] == NSOrderedSame){  // email
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SignTextDefaultCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        
        if([str isEqualToString:@""]){
            [cell setCheckmark:NO];
            return;
        }
        
        if(![self validateEmail:str]){
            [cell setCheckmark:NO];
            return;
        }
        
        if([str caseInsensitiveCompare:[SharedProfile getEmail]] == NSOrderedSame){
            [cell setCheckmark:YES];
            return;
        }
        
        [SharedNetworkManager checkEmailAvailable:str sender:self];
        [cell startAnimating];
    }
    
    /*
    else if(row == 5){ //password
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:5 inSection:0];
        SignTextDefaultCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        if([str length] < 4)
            [cell setCheckmark:NO];
        else
            [cell setCheckmark:YES];
    }
    
    else if(row == 6){ //password
        NSIndexPath *passwordIndexPath = [NSIndexPath indexPathForRow:5 inSection:0];
        NSIndexPath *checkIndexPath = [NSIndexPath indexPathForRow:6 inSection:0];

        SignTextDefaultCell* passwordCell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:passwordIndexPath];
        SignTextDefaultCell* checkCell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:checkIndexPath];
        if([checkCell.textLabel.text isEqualToString:passwordCell.textLabel.text])
           [checkCell setCheckmark:YES];
        else
           [checkCell setCheckmark:NO];
    }
    */
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}



#pragma mark - Network
-(void)returnUsernameAvailable:(BOOL)available username:(NSString*)username{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    SignTextDefaultCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell stopAnimating];
    [cell setCheckmark:available];
    if(available){
        NSLog(@"%@ available", username);


    }
    else{
        NSLog(@"%@ not available", username);
        //cell.imageView.image = [UIImage imageNamed:@"not-ok-icon.png"];
    }
    
}

-(void)returnEmailAvailable:(BOOL)available email:(NSString*)email{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    SignTextDefaultCell* cell = (SignTextDefaultCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell stopAnimating];
    [cell setCheckmark:available];

    //cell.imageView.image = [UIImage imageNamed:@"ok-icon.png"];
    if(available){
        NSLog(@"%@ available", email);
        
        
    }
    else{
        NSLog(@"%@ not available", email);
      //  cell.imageView.image = [UIImage imageNamed:@"not-ok-icon.png"];
    }
    
    
    NSLog(@"RETURNED");
}

-(void)networkManagerError{
    NSLog(@"Network Error");
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Network Error"
                          message:@"Network temporarily down.\n Please retry later"
                          delegate: nil
                          cancelButtonTitle:@"Continue"
                          otherButtonTitles:nil];
    [alert show];
}


-(void)returnSignup:(BOOL)success message:(NSString*)msg{
    [HUD hide:YES];
    mIsEULAChecked = NO;
    
    if(success){
        NSLog(@"success");
        
//        [SharedProfile setUsername:mUsername];
        [SharedProfile setEmail:mEmail];
        [SharedProfile setPassword:mPassword];
        
        [SharedProfile setLogin];
        
        //[self.navigationController popViewControllerAnimated:YES];

        ProcessLoginViewController *processVC = [[ProcessLoginViewController alloc] init];
        [self.navigationController pushViewController:processVC animated:NO];
 
    }
    else{
        NSLog(@"signup fail");
        NSLog(@"msg: %@", msg);
        if([msg isEqual: [NSNull null]]){
            NSLog(@"msg is null");
            msg = @"";
        }
        [self showAlertWithTitle:@"Sign up Error" message:msg];
    }
}

-(void)showAlertWithTitle:(NSString*)title message:(NSString*)msg{

    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: title
                          message:msg
                          delegate: nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}



#pragma mark - EULA & alertview delegate

- (void)agreeWithEULA{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @""
                          message: @"Do you agree with the following EULA?"
                          delegate: self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"Yes", @"No", @"Read", nil];
    [alert show];
    return;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if( buttonIndex == 0) {  //yes
        mIsEULAChecked = YES;
        [self tableView:self.tableView didSelectRowAtIndexPath:mSelectedIndexPath];
    }
    else if(buttonIndex == 1)   //no
        ;
    else    //read EULA
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.piikit.com/terms.so"]];
}


#pragma mark - birthday picker delegate
- (void)updateBirthday:(NSString*)dateStr{
    NSLog(@"returned %@", dateStr);
    birthdayTextField.text = dateStr;
}

- (void) disableUsernameCheck{
    disableUsernameCheck = YES;
}
@end
