//
//  BirthdayPickerViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 10. 11..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BirthdayPickerDelegate
- (void)updateBirthday:(NSString*)dateStr;
@end;


@interface BirthdayPickerViewController : UIViewController{
    UIDatePicker *picker;
}

@property (nonatomic, retain) id<BirthdayPickerDelegate> delegate;

@end

