//
//  FacebookCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 8..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "FacebookCell.h"
#import <CoreText/CoreText.h>

@implementation FacebookCell

#define FACEBOOK 101
#define TWITTER 102

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        // FACEBOOK
        CGFloat x = 20;
        
        UIImageView *facebookLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"facebook.png"]];
        facebookLogo.frame = CGRectMake(x, 5, 18, 18);
        facebookLogo.tag = FACEBOOK;
        facebookLogo.userInteractionEnabled = YES;
        x+=18;
        
        x+=5;
        
        NSMutableAttributedString *facebookStr = [[NSMutableAttributedString alloc] initWithString:@"FACEBOOK"];
        [facebookStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:18.f] range:NSMakeRange(0, facebookStr.string.length)];
        [facebookStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, facebookStr.string.length)];
        [facebookStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.5f] range:NSMakeRange(0, facebookStr.string.length)];
        
        UILabel *facebookLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, facebookStr.size.width, 29)];
        facebookLabel.tag = FACEBOOK;
        facebookLabel.userInteractionEnabled = YES;
        facebookLabel.attributedText = facebookStr;
        
        [self.contentView addSubview:facebookLogo];
        [self.contentView addSubview:facebookLabel];

        x+= facebookLabel.frame.size.width;
        /*
        x+= 70;
        UIImageView *twitterLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"twitter.png"]];
        twitterLogo.frame = CGRectMake(x, 5, 18, 18);
        x+=18;
        
        x+=5;
        
        NSMutableAttributedString *twitterStr = [[NSMutableAttributedString alloc] initWithString:@"TWITTER"];
        [twitterStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:18.f] range:NSMakeRange(0, twitterStr.string.length)];
        [twitterStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, twitterStr.string.length)];
        [twitterStr addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:1.5f] range:NSMakeRange(0, twitterStr.string.length)];
        
        UILabel *twitterLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, twitterStr.size.width, 29)];
        twitterLabel.tag = TWITTER;
        twitterLabel.userInteractionEnabled = YES;
        twitterLabel.attributedText = twitterStr;
        [self.contentView addSubview:twitterLabel];
        [self.contentView addSubview:twitterLogo];
        */
               

    }
    return self;
}

/*
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    NSLog(@"set selected - facebook");
}
*/
@end
