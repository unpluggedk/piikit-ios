//
//  NetworkQueryBuilder.h
//  Piikit
//
//  Created by Jason Kim on 13. 6. 14..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkQueryBuilder : NSObject{
    NSMutableString *str;
}

// for default url with added arguments
- (id)initWithURL:(NSString*)url;
- (void)addArgumentWithKey:(NSString*)key value:(NSString*)value;

- (void)print;

// specific urls
//+ (NSURL*)getHomeItemsAtPage:(int)page;
+ (NSURL*)getHomeItemsAtPage:(int)page category:(NSNumber*)catId userId:(NSNumber*)userId;

+ (NSURL*)getResultsVotedAtPage:(int)page userId:(NSNumber*)userId;
+ (NSURL*)getResultsPostedAtPage:(int)page userId:(NSNumber*)userId;
+ (NSURL*)getResultsBookmarsAtPage:(int)page userId:(NSNumber*)userId;

+ (NSString*)getImageURLString:(NSString*)url;

+ (NSURL*)getFacebookLogin;


@property (nonatomic, retain) NSMutableString *str;
@end
