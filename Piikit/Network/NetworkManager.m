//
//  NetworkManager.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "NetworkManager.h"
#import "GCDSingleton.h"

#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"


#import "NetworkQueryBuilder.h"

#import "Profile.h"

@implementation NetworkManager

@synthesize username = mUsername;

+(id)sharedNetworkManager{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

-(id)init{
    NSLog(@"init Network manager");
    mPageComments = 1;
    return self;

    
}

#pragma mark - Connection
- (void) connectWithUsername:(NSString*)username email:(NSString*)email password:(NSString*)password sender:(id<NetworkDelegate>)delegate{
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"user/portalLogin.so"];
    if(username){
        [query addArgumentWithKey:@"username" value:username];
        NSLog(@"set username = %@", username);
    }
    if(email){
        [query addArgumentWithKey:@"email" value:email];
        NSLog(@"set email = %@", email);
    }
    
    [query addArgumentWithKey:@"password" value:password];
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {

                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        id user = [JSON objectForKey:@"user"];
                                                        
                                                        BOOL signinSuccess;
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            signinSuccess = true;

                                                            [SharedProfile setUsername:[user objectForKey:@"username"]];
                                                            [SharedProfile setDisplayname:[user objectForKey:@"displayname"]];
                                                            [SharedProfile setBirthdate:[user objectForKey:@"birthdate"]];
                                                            [SharedProfile setJoindate:[user objectForKey:@"joindate"]];
                                                            [SharedProfile setSex:[user objectForKey:@"sex"]];
                                                            [SharedProfile setCountry:[user objectForKey:@"country"]];
                                                            [SharedProfile setEmail:[user objectForKey:@"email"]];
                                                            [SharedProfile setUserId:[user objectForKey:@"userid"]];
                                                            
                                                            
                                                            NSString *photoImg = [user objectForKey:@"photoImg"];
                                                            
                                                            if([photoImg isEqual: [NSNull null]]){
                                                                [SharedProfile setProfileURLString:NULL];
                                                            //    NSLog(@"photoImage = NULL");
                                                            }
                                                            else{
                                                                [SharedProfile setProfileURLString:[NetworkQueryBuilder getImageURLString:photoImg]];
                                                            //    NSLog(@"photoImage : %@", [SharedProfile profileURLString]);
                                                            }
                                                        }
                                                        else{
                                                            signinSuccess = false;
                                                            NSLog(@"error return code = %@", num);
                                                        }
                                                        [SharedProfile printProfile];
                                                        [delegate returnSignin:signinSuccess];
                                                        
                                                        return;
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        NSLog(@"fail!!  - %d", [response statusCode]);
                                                        
                                                        NSLog(@"%@", [error localizedDescription]);
                                                        NSLog(@"%@", [error localizedFailureReason]);
                                                        NSLog(@"%@", [error localizedRecoverySuggestion]);
                                                        
                                                        [delegate networkManagerError];
                                                        
                                                        
                                                    }];
    
    [operation start];
}

- (void) connectWithFacebook:(NSString*)accessToken sender:(id<NetworkDelegate>)delegate{

    NSURL *url = [NetworkQueryBuilder getFacebookLogin];
//    NSURL *url = [NSURL URLWithString:@"http://test.piikit.com:8080/facebook/login2.so"];
  
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    NSLog(@"token = %@", accessToken);
    [parameters setObject:accessToken forKey:@"accessToken"];
    [parameters setObject:[NSNumber numberWithBool:YES] forKey:@"apiCall"];

    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"login2.so"
                                                      parameters:parameters];

   
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSLog(@"try sending");
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        BOOL signinSuccess;
                                                        id user = [JSON objectForKey:@"user"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            signinSuccess = true;
                                                            /*
                                                            NSString *displayname = [user objectForKey:@"displayname"];
                                                            NSString *birthdate = [user objectForKey:@"birthdate"];
                                                            NSString *email = [user objectForKey:@"email"];
                                                            NSNumber *userId = [user objectForKey:@"userid"];
                                                            */
                                                            
                                                            [SharedProfile setUsername:[user objectForKey:@"username"]];
                                                            [SharedProfile setPassword:[user objectForKey:@"password"]];
                                                            
                                                            [SharedProfile setDisplayname:[user objectForKey:@"displayname"]];
                                                            [SharedProfile setBirthdate:[user objectForKey:@"birthdate"]];
                                                            [SharedProfile setJoindate:[user objectForKey:@"joindate"]];
                                                            [SharedProfile setSex:[user objectForKey:@"sex"]];
                                                            [SharedProfile setCountry:[user objectForKey:@"country"]];
                                                            [SharedProfile setEmail:[user objectForKey:@"email"]];
                                                            [SharedProfile setUserId:[user objectForKey:@"userid"]];
                                                           
                                                            NSString *photoImg = [user objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]]){
                                                                NSLog(@"picture NULL");
                                                                NSString *facebookUser = [((NSString*)[user objectForKey:@"username"]) substringFromIndex:3];
                                                                [SharedProfile setProfileURLString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", facebookUser]];
                                                            }
                                                            else{
                                                                NSLog(@"picture %@", photoImg);
                                                                [SharedProfile setProfileURLString:[NetworkQueryBuilder getImageURLString:photoImg]];
                                                            }
                                                            
                                                            [SharedProfile setFacebookLogin:YES];
                                                            NSLog(@"facebook sign in success");
                                                        }

                                                        else{
                                                            NSLog(@"facebook sign in failed - fb");
                                                            signinSuccess = false;
                                                           
                                                            [[FBSession activeSession] closeAndClearTokenInformation];
                                                            [SharedProfile setFacebookLogin:NO];
                                                        }
                                                        
                                                        [SharedProfile printProfile];
                                                        
                                                        
                                                        [delegate returnSignin:signinSuccess];
                              
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail!!  - %d", [response statusCode]);
                                                        
                                                        NSLog(@"%@", [error localizedDescription]);
                                                        NSLog(@"%@", [error localizedFailureReason]);
                                                        NSLog(@"%@", [error localizedRecoverySuggestion]);
                                                        
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];
    


}



- (void) disconnect{
    [SharedProfile setLogout];
}

- (BOOL) isConnected{
    return [SharedProfile isLoggedIn];
}


#pragma mark - Profile
- (void) checkUsernameAvailable:(NSString*)username sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"user/checkAvailable.so"];
    [query addArgumentWithKey:@"username" value:username];
    
    NSURL *url = [NSURL URLWithString:query.str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
        [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                            
                                                            NSNumber *num = [JSON valueForKeyPath:@"result"];
                                                            BOOL availability;
                                                            if([num isEqualToNumber:[NSNumber numberWithInt:0]])
                                                                availability = true;
                                                            else
                                                                availability = false;
                                                            [delegate returnUsernameAvailable:availability username:username];
                                                            return;
                                                        }
                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                            NSLog(@"Network Error");
                                                            [delegate networkManagerError];
                                                        }];
    
    [operation start];
}

- (void) checkEmailAvailable:(NSString*)email sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"user/checkEmail.so"];
    [query addArgumentWithKey:@"email" value:email];
    
    NSURL *url = [NSURL URLWithString:query.str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {

                                                        NSNumber *num = [JSON valueForKeyPath:@"result"];
                                                        BOOL availability;
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]])
                                                            availability = true;
                                                        else
                                                            availability = false;
                                                        [delegate returnEmailAvailable:availability email:email];
                                                        return;
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
}


- (void) createAccount:(NSString*)username password:(NSString*)password email:(NSString*)email birthdate:(NSString*)birthdate
                   sex:(NSString*)sex country:(NSString*)country receiveupdates:(NSString*)receiveupdates status:(NSString*)status sender:(id<NetworkDelegate>)delegate{

    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"user/createAccount.so"];
    
    [query addArgumentWithKey:@"username" value:username];
    [query addArgumentWithKey:@"displayname" value:username];
    [query addArgumentWithKey:@"password" value:password];
    [query addArgumentWithKey:@"email" value:email];
    if(birthdate != NULL)
        [query addArgumentWithKey:@"birthdate" value:birthdate];
    [query addArgumentWithKey:@"sex" value:sex];

    if(country != NULL)
        [query addArgumentWithKey:@"country" value:country];    // 2 letter
    [query addArgumentWithKey:@"receiveupdates" value:receiveupdates];  // for now, 't'
    [query addArgumentWithKey:@"status" value:status];  // for now 'v'

    [query print];
    
    NSURL *url = [NSURL URLWithString:query.str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKeyPath:@"result"];
                                                        BOOL signupSuccess;
                                                        NSString *retMsg = NULL;
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]])
                                                            signupSuccess = true;
                                                        else{
                                                            signupSuccess = false;
                                                            retMsg = [JSON valueForKeyPath:@"msg"];
                                                        }

                                                        [delegate returnSignup:signupSuccess message:retMsg];
                                                        return;
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];

}

- (void) postProfilePictureWithImage:(UIImage*)image sender:(id<NetworkDelegate>)delegate{
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"profile"];
    NSURL *url = [NSURL URLWithString:query.str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[SharedProfile getUsername] forKey:@"username"];
    NSLog(@"%@  %@", [SharedProfile getUsername], [SharedProfile getPassword]);
    [parameters setObject:[SharedProfile getPassword] forKey:@"password"];
    
    NSString *photoName= @"photo.png";
    
    // the path to write file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:photoName];
    NSData * photoImageData = UIImagePNGRepresentation(image);
    [photoImageData writeToFile:filePath atomically:YES];
    
    NSLog(@"url photo written to path: %@", filePath);
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                         path:@"uploadPhoto.so"
                                                                   parameters:parameters
                                                    constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                                                        
                                                        [formData appendPartWithFileData:photoImageData
                                                                                    name:@"photo"
                                                                                fileName:filePath
                                                                                mimeType:@"image/png"];
                                                        //[formData appendPartWithFileData:imageData name:@"files" fileName:photoName mimeType:@"image/jpeg"];
                                                        // [formData appendPartWithFormData:[key2 dataUsingEncoding:NSUTF8StringEncoding] name:@"key2"];
                                                        // etc.
                                                        NSLog(@"SEND!");
                                                    }];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"%@", [JSON valueForKey:@"filename"]);
                                                            [delegate returnProfilePostedURL:[NetworkQueryBuilder getImageURLString:[JSON valueForKey:@"filename"]]];
                                                        }
                                                        
                                                        NSLog(@"returned code = %@", num);

                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail");
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];
    
    
}

- (void) editProfileWithUsername:(NSString*)username
                        password:(NSString*)password
                     newPassword:(NSString*)newPassword
                     displayname:(NSString*)displayname
                       birthdate:(NSString*)birthdate
                        joindate:(NSString*)joindate
                             sex:(NSString*)sex
                         country:(NSString*)country
                           email:(NSString*)email
                          sender:(id<NetworkDelegate>)delegate{

    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"profile"];
    NSURL *url = [NSURL URLWithString:query.str];
    
    //NSURL *url = [NSURL URLWithString:@"http://test.piikit.com:8080/api/post/comment"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    username = [SharedProfile getUsername];
    
    
    NSLog(@"username %@", username);
    NSLog(@"password %@", password);
    NSLog(@"email %@", email);
    NSLog(@"displayname => %@", displayname);
    NSLog(@"sex %@", sex);
    NSLog(@"birthdate %@", birthdate);
    
    [parameters setObject:username forKey:@"username"];
    [parameters setObject:password forKey:@"password"];
    [parameters setObject:newPassword forKey:@"newPassword"];
    [parameters setObject:email forKey:@"email"];
    [parameters setObject:sex forKey:@"sex"];
    [parameters setObject:birthdate forKey:@"birthdate"];
    [parameters setObject:displayname forKey:@"displayname"];
    if(joindate)
        [parameters setObject:joindate forKey:@"joindate"];
   
    if(country)
        [parameters setObject:country forKey:@"country"];
   
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:[SharedProfile isFacebookLogin]?@"editFacebook.so":@"edit.so"
                                                      parameters:parameters];
    

    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSLog(@"try sending");
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        // current API doesnot return success value
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"edit complete");
                                                            
                                                            id user = [JSON valueForKey:@"selectedUser"];
                                                            
                                                            NSLog(@"edit - check facebook login");
                                                            if([SharedProfile isFacebookLogin]){
                                                                NSLog(@"it is a facebook login");
                                                                [SharedProfile setPassword:[user objectForKey:@"password"]];
                                                            }
                                                            [delegate returnProfileUpdate:YES];
                                                        }
                                                        else if([num isEqualToNumber:[NSNumber numberWithInt:-98]] ||
                                                                [num isEqualToNumber:[NSNumber numberWithInt:-1]]){
                                                            NSLog(@"posting failed - %@", num);
                                                            //                                                          [delegate returnCommentComplete:NO Comment:NULL];
                                                            NSLog(@"field - %@", [JSON valueForKey:@"field"]);
                                                            NSLog(@"error - %@", [JSON valueForKey:@"error"]);
                                                            
                                                            [delegate returnProfileUpdate:NO];
                                                        }
                                                                                          
                                                            
                                                        else{
                                                            NSLog(@"profile edit exception");
                                                            [delegate returnProfileUpdate:NO];
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail");
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];
    

    
    
}


- (void) getProfileWithSuccessPopUp:(NSString *)successPopUp sender:(id<NetworkDelegate>)delegate{
    
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"profile"];
    NSURL *url = [NSURL URLWithString:query.str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setObject:[SharedProfile getUsername] forKey:@"username"];
    [parameters setObject:[SharedProfile getPassword] forKey:@"password"];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"get.so"
                                                      parameters:parameters];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSLog(@"try getting profile");
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        // current API doesnot return success value
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            id user = [JSON objectForKey:@"user"];
                                                            
                                                            [delegate returnProfile:YES
                                                                        displayname:[user objectForKey:@"displayname"]
                                                                           birthday:[user objectForKey:@"birthdate"]
                                                                              email:[user objectForKey:@"email"]
                                                                                sex:[user objectForKey:@"sex"]
                                                                       successPopUp:successPopUp];
                                                            
                                                        }
                                                        
                                                        else{

                                                            NSLog(@"return code = %@", num);
                                                            
                                                            [delegate returnProfile:NO
                                                                        displayname:nil birthday:nil email:nil sex:nil successPopUp:successPopUp];
                                                      
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail");
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];
    
    

    
}



#pragma mark - Fetch Posts

- (void) fetchNewCategory:(NSNumber*)catId sender:(id<NetworkDelegate>)delegate{
    mPageHome = 1;
    [self fetchPosting:Recent category:catId sender:delegate];
}

- (void) fetchNewPostings:(PostRequestType)type sender:(id<NetworkDelegate>)delegate{
    mPageHome = 1;
    [self fetchPosting:type category:NULL sender:delegate];
}

- (void) fetchMorePostings:(PostRequestType)type category:(NSNumber*)catId  sender:(id<NetworkDelegate>)delegate{
    mPageHome++;
    [self fetchPosting:type category:catId sender:delegate];
}

- (void) fetchPosting:(PostRequestType)type category:(NSNumber*)catId sender:(id<NetworkDelegate>)delegate{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSURL *url = [NetworkQueryBuilder getHomeItemsAtPage:mPageHome category:catId userId:[SharedProfile userId]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"url = %@", url);

    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSDictionary *jsonDict = (NSDictionary *) JSON;
                                                        
                                                        NSArray *singleItems = [jsonDict objectForKey:@"postsType1"];
                                                        NSArray *vsItems = [jsonDict objectForKey:@"postsType2"];
                                                        
                                                        //NSLog(@"single items %d", [singleItems count]);
                                                        //NSLog(@"double iterms %d", [vsItems count]);
                                                        
                                                        // single items
                                                        [singleItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                            // postID
                                                            NSNumber *postId = [obj objectForKey:@"postid"];
                                                            
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];
                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            
                                                            
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];
                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }
                                                            

                                                            // item
//                                                            NSString *string = [NSString stringWithCString:cString encoding:NSUTF8StringEncoding];
                                                            NSString *title = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"title"]];
                                                            
                                                            NSString *description = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"description"]];
                                                            NSString *labelLeft = [obj objectForKey:@"itemid1"];
                                                            NSString *picSingle = [obj objectForKey:@"itemurl1"];
                                                            NSNumber *votesSingle = [obj objectForKey:@"vote1"];
                                                            NSNumber *numComments = [obj objectForKey:@"comments"];
                                                            
                                                            NSNumber *dateUpdated = [obj objectForKey:@"updated"];
                                                            if([dateUpdated isEqual:[NSNull null]])
                                                                dateUpdated = [NSNumber numberWithInt:0];
                                                            NSNumber *dateCreated = [obj objectForKey:@"created"];
                                                            
                                                            NSNumber *myVote = [obj objectForKey:@"myvote"];
                                                            
                                                           /*
                                                             NSLog(@"post ID:%@", postID);
                                                             NSLog(@"myVote %@", myVote);
                                                             
                                                             NSLog(@"post ID:%@", postID);
                                                             NSLog(@"username:%@", username);
                                                             NSLog(@"profileURL:%@", profileURL);
                                                             NSLog(@"title:%@", title);
                                                             NSLog(@"description:%@", description);
                                                             NSLog(@"label:%@", labelLeft);
                                                             NSLog(@"picURL:%@", picSingle);
                                                             NSLog(@"votes:%@", votesSingle);
                                                             NSLog(@"numComments:%@", numComments);
                                                             */
                                                            
                                                            PostingItem *item = [[PostingItem alloc] initWithID:postId
                                                                                                    DisplayName:displayname
                                                                                                          Title:title
                                                                                                    Description:description
                                                                                                      LabelLeft:labelLeft LabelRight:NULL
                                                                                                     ProfileURL:profileURL
                                                                                                        PicLeft:NULL
                                                                                                       PicRight:NULL
                                                                                                      PicSingle:picSingle
                                                                                                   NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:[votesSingle intValue]
                                                                                                    NumComments:[numComments intValue]
                                                                                                      NumShared:0
                                                                                               DateCreatedMilli:[dateCreated floatValue]
                                                                                               DateUpdatedMilli:[dateUpdated floatValue]];
                                                            
                                                            if([myVote intValue] == 0)
                                                                item.mMyVote = NotVoted;
                                                            else if([myVote intValue] == 1)
                                                                item.mMyVote = VoteLeft;
                                                            else
                                                                item.mMyVote = VoteRight;
                                                            
                                                            item.mAppVote = item.mMyVote;
                                                            [result addObject:item];
                                                        }];
                                                        
                                                        
                                                        [vsItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                            // postID
                                                            NSNumber *postId = [obj objectForKey:@"postid"];
                                                            
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];

                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];
                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }
                                                            
                                                            // item
                                                            
                                                            NSString *title = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"title"]];
                                                            
                                                            NSString *description = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"description"]];
                                                            NSString *labelLeft = [obj objectForKey:@"itemid1"];
                                                            NSString *labelRight = [obj objectForKey:@"itemid2"];
                                                            
                                                            NSString *picLeft = [obj objectForKey:@"itemurl1"];
                                                            NSString *picRight = [obj objectForKey:@"itemurl2"];
                                                            
                                                            NSNumber *votesLeft = [obj objectForKey:@"vote1"];
                                                            NSNumber *votesRight = [obj objectForKey:@"vote2"];
                                                            NSNumber *numComments = [obj objectForKey:@"comments"];
                                                            
                                                            NSNumber *dateUpdated = [obj objectForKey:@"updated"];
                                                            if([dateUpdated isEqual:[NSNull null]])
                                                                dateUpdated = [NSNumber numberWithInt:0];
                                                            NSNumber *dateCreated = [obj objectForKey:@"created"];
                                                            
                                                            //NSLog(@"display name = %@     photo = %@   title =%@", username, profileURL, title);
                                                        
                                                            
                                                            /*
                                                             NSLog(@"post ID:%@", postID);
                                                             NSLog(@"username:%@", username);
                                                             NSLog(@"profileURL:%@", profileURL);
                                                             NSLog(@"title:%@", title);
                                                             NSLog(@"description:%@", description);
                                                             NSLog(@"label:%@", labelLeft);
                                                             NSLog(@"picURL:%@", picSingle);
                                                             NSLog(@"votes:%@", votesSingle);
                                                             NSLog(@"numComments:%@", numComments);
                                                             */
                                                            NSNumber *myVote = [obj objectForKey:@"myvote"];
                                                            
                                                            
                                                            PostingItem *item = [[PostingItem alloc] initWithID:postId
                                                                                                       DisplayName:displayname
                                                                                                          Title:title
                                                                                                    Description:description
                                                                                                      LabelLeft:labelLeft LabelRight:labelRight
                                                                                                     ProfileURL:profileURL
                                                                                                        PicLeft:picLeft
                                                                                                       PicRight:picRight
                                                                                                      PicSingle:NULL
                                                                                                   NumVotesLeft:[votesLeft intValue] NumVotesRight:[votesRight intValue] NumVotesSingle:0
                                                                                                    NumComments:[numComments intValue]
                                                                                                      NumShared:0
                                                                                               DateCreatedMilli:[dateCreated floatValue]
                                                                                               DateUpdatedMilli:[dateUpdated floatValue]];
                                                            
                                                            if([myVote intValue] == 0)
                                                                item.mMyVote = NotVoted;
                                                            else if([myVote intValue] == 1)
                                                                item.mMyVote = VoteLeft;
                                                            else
                                                                item.mMyVote = VoteRight;
                                                            item.mAppVote = item.mMyVote;
                                                            [result addObject:item];
                                                        }];
                                                        
                                                        NSArray *sortedArray = [result sortedArrayUsingComparator: ^(PostingItem* obj1, PostingItem* obj2) {
                                                            
                                                            if (obj1.mDateUpdatedMilliseconds < obj2.mDateUpdatedMilliseconds) {
                                                                return (NSComparisonResult)NSOrderedDescending;
                                                            }
                                                            
                                                            if (obj1.mDateUpdatedMilliseconds > obj2.mDateUpdatedMilliseconds) {
                                                                return (NSComparisonResult)NSOrderedAscending;
                                                            }
                                                            return (NSComparisonResult)NSOrderedSame;
                                                        }];
                                                        
                                                        //NSLog(@"single items %d", [singleItems count]);
                                                        //NSLog(@"double iterms %d", [vsItems count]);
                                                        BOOL isEnd = NO;
                                                        if([singleItems count] < 8 && [vsItems count] < 8)
                                                            isEnd = YES;
                                                        
                                                        [delegate returnFetchedItems:sortedArray newItems:(mPageHome==1)?YES:NO isEnd:isEnd];
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
    
    
    
    //return result;
}



- (void) fetchSinglePosting:(NSNumber*)itemId sender:(id<NetworkDelegate>)delegate{
    
    NSString *urlHelper = [NSString stringWithFormat:@"post/view/%@.so", itemId];
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:urlHelper];
    
    if(![SharedProfile isLoginSkipped])
        [query addArgumentWithKey:@"userid" value:[NSString stringWithFormat:@"%@", [SharedProfile userId]]];
    //[query print];
    
    NSURL *url = [NSURL URLWithString:query.str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSNumber *num = [JSON valueForKeyPath:@"result"];
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:-1]]){
                                                            [delegate returnSinglePost:NULL itemExist:NO];
                                                            return;
                                                        }

                                                        BOOL isBookmarked = [[JSON objectForKey:@"isbookmarked"] isEqualToNumber:[NSNumber numberWithInt:1]];
                                                        
                                                        id post = [JSON objectForKey:@"post"];
                                                        
                                                        NSNumber *postId = [post objectForKey:@"postid"];

                                                        // user
                                                        id userobj = [post objectForKey:@"user"];
                                                        NSString *username = [userobj objectForKey:@"username"];
                                                        NSString *displayname = [userobj objectForKey:@"displayname"];

                                                        NSString *profileURL = NULL;
                                                        
                                                        NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                        NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                        if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                            profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];

                                                        }
                                                        else{
                                                            profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                        }
                                                        
                                                        // item
                                                        
                                                        NSString *title = [NetworkManager replaceHTMLCodes:[post objectForKey:@"title"]];
                                                        NSString *description = [NetworkManager replaceHTMLCodes:[post objectForKey:@"description"]];

                                                        NSNumber *posttype = [post objectForKey:@"posttype"];
                                                        
                                                        NSString *labelLeft = [post objectForKey:@"itemid1"];
                                                        NSString *labelRight = [post objectForKey:@"itemid2"];
                                                        NSString *picLeft = [post objectForKey:@"itemurl1"];
                                                        NSString *picRight = [post objectForKey:@"itemurl2"];

                                                        NSNumber *dateUpdated = [post objectForKey:@"updated"];
                                                        if([dateUpdated isEqual:[NSNull null]])
                                                            dateUpdated = [NSNumber numberWithInt:0];
                                                        NSNumber *dateCreated = [post objectForKey:@"created"];
                                                        
                                                        NSNumber *votesLeft = [post objectForKey:@"vote1"];
                                                        NSNumber *votesRight = [post objectForKey:@"vote2"];
                                                        
                                                        NSNumber *numComments = [post objectForKey:@"comments"];
                                                        
                                                        NSNumber *myVote = [post objectForKey:@"myvote"];
                                                        
                                                        PostingItem *item;
                                                        
                                                        if([posttype isEqualToNumber:[NSNumber numberWithInt:1]]){ //single posting
                                                            item = [[PostingItem alloc] initWithID:postId
                                                                                          DisplayName:displayname
                                                                                             Title:title
                                                                                       Description:description
                                                                                         LabelLeft:labelLeft LabelRight:NULL
                                                                                        ProfileURL:profileURL
                                                                                           PicLeft:NULL PicRight:NULL PicSingle:picLeft
                                                                                      NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:[votesLeft intValue]
                                                                                       NumComments:[numComments intValue]
                                                                                         NumShared:0
                                                                                  DateCreatedMilli:[dateCreated floatValue]
                                                                                  DateUpdatedMilli:[dateUpdated floatValue]];
                                                        }
                                                        else{
                                                            item = [[PostingItem alloc] initWithID:postId
                                                                                       DisplayName:displayname
                                                                                             Title:title
                                                                                       Description:description
                                                                                         LabelLeft:labelLeft LabelRight:labelRight
                                                                                        ProfileURL:profileURL
                                                                                           PicLeft:picLeft PicRight:picRight PicSingle:NULL
                                                                                      NumVotesLeft:[votesLeft intValue] NumVotesRight:[votesRight intValue] NumVotesSingle:0
                                                                                       NumComments:[numComments intValue]
                                                                                         NumShared:0
                                                                                  DateCreatedMilli:[dateCreated floatValue]
                                                                                  DateUpdatedMilli:[dateUpdated floatValue]];
                                                        }
                                                        
                                                   //     NSLog(@"left = %@  right = %@", votesLeft, votesRight);
                                                   //     NSLog(@"item left %d   right %d", item.mVotesLeft, item.mVotesRight);
                                                        
                                                        item.mVotesLeftMale = [[post objectForKey:@"vote1_male"] intValue];
                                                        item.mVotesLeftFemale = [[post objectForKey:@"vote1_female"] intValue];
                                                        item.mVotesLeftUnder21 = [[post objectForKey:@"vote1_age1"] intValue];
                                                        item.mVotesLeft21to30 = [[post objectForKey:@"vote1_age2"] intValue];
                                                        item.mVotesLeftOver30 = [[post objectForKey:@"vote1_age3"] intValue];

                                                        item.mVotesRightMale = [[post objectForKey:@"vote2_male"] intValue];
                                                        item.mVotesRightFemale = [[post objectForKey:@"vote2_female"] intValue];
                                                        item.mVotesRightUnder21 = [[post objectForKey:@"vote2_age1"] intValue];
                                                        item.mVotesRight21to30 = [[post objectForKey:@"vote2_age2"] intValue];
                                                        item.mVotesRightOver30 = [[post objectForKey:@"vote2_age3"] intValue];

                                                        if([myVote intValue] == 0)
                                                            item.mMyVote = NotVoted;
                                                        else if([myVote intValue] == 1)
                                                            item.mMyVote = VoteLeft;
                                                        else
                                                            item.mMyVote = VoteRight;
                                                        item.mAppVote = item.mMyVote;
                                                        
                                                        item.mIsBookmarked = isBookmarked;
                                                        
                                                        [delegate returnSinglePost:item itemExist:YES];
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON){
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
    
    
    
}


#pragma mark - Comments

// ======================================================
// comments
// ======================================================
/*- (void) fetchComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
- (void) postComment:(NSNumber*)postId comment:(NSString*)comment sender:(id<NetworkDelegate>)delegate;
- (void) deleteComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
- (void) likeComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
- (void) dislikeComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
*/


- (void) fetchNewComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate{
    mPageComments = 1;
    [self fetchComments:postId sender:delegate];
    
}
- (void) fetchMoreComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate{
    mPageComments++;
    [self fetchComments:postId sender:delegate]; 
}

- (void) fetchComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate{
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/comment/load.so"];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postId]];
//    [query addArgumentWithKey:@"postid" value:@"111"];  // post with 7 commennts
    [query addArgumentWithKey:@"pageNum" value:[NSString stringWithFormat:@"%d", mPageComments]];
   [query print];
   
    NSURL *url = [NSURL URLWithString:query.str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSNumber *num = [JSON valueForKeyPath:@"result"];
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:-1]]){
                                                            [delegate returnSinglePost:NULL itemExist:NO];
                                                            return;
                                                        }
                                                        NSDictionary *jsonDict = (NSDictionary *) JSON;
                                                        
                                                        NSArray *comments = [jsonDict objectForKey:@"comments"];
                                                        [comments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                            Comment *comment = [[Comment alloc] init];
                                                            comment.mCommentId = [obj objectForKey:@"commentid"];
                                                            comment.mComment = [obj objectForKey:@"comment"];
                                                            comment.mNumLikes = [obj objectForKey:@"like"];
                                                            comment.mNumDisikes = [obj objectForKey:@"dislike"];
                                                            comment.mTimeDiff = [obj objectForKey:@"timeDiff"];
                                                            comment.mTimeCreatedFloat = [[obj objectForKey:@"created"] floatValue];
                                                            
                                                       
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];
                                                            

                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];

                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }

                                                            
                                                            comment.mUsername = displayname;
                                                            comment.mProfileURLString = profileURL;
                                                            
                                                            [result addObject:comment];
                                                            
                                                        }];
                                                        [delegate returnComments:result newItems:(mPageComments==1) endOfComments:([result count]<10)];
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON){
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
    
    
}

- (void) postComment:(NSNumber*)postId comment:(NSString*)comment sender:(id<NetworkDelegate>)delegate{
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/comment"];
    NSURL *url = [NSURL URLWithString:query.str];
    
    //NSURL *url = [NSURL URLWithString:@"http://test.piikit.com:8080/api/post/comment"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];

    [parameters setObject:[SharedProfile getUsername] forKey:@"username"];
    [parameters setObject:[SharedProfile getPassword] forKey:@"password"];
    [parameters setObject:comment forKey:@"comment"];
    [parameters setObject:[NSString stringWithFormat:@"%@", postId] forKey:@"postid"];
    /*
    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:[[NSArray alloc] initWithObjects:@"highroller", @"k124sj06", @"comment_api", @"116", nil]
                                                       forKeys:[[NSArray alloc] initWithObjects:@"username", @"password", @"comment", @"postid", nil]];
    */
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"post.so"
                                                      parameters:parameters];
                                    
                                    
                                    
                                    /*multipartFormRequestWithMethod:@"POST"
                                                                         path:@"post.so"
                                                                   parameters:dict
                                                    constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                                                        NSLog(@"SEND!");;
                                                    }];
                                     
                                     */
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSLog(@"try sending");
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        id commentObj = [JSON valueForKey:@"comment"];
                                                       /* NSString *ddd = [comment valueForKey:@"timeDiff"];
                                                        NSNumber *postId = [comment valueForKey:@"postid"];
                                                        NSLog(@"%@", ddd);
                                                        NSLog(@"post id %@", postId);
                                                        NSLog(@"result = %@", num);
                                                        */
                                                        
                                                        Comment *comment = [[Comment alloc] init];
                                                        comment.mCommentId = [commentObj objectForKey:@"commentid"];
                                                        comment.mComment = [commentObj objectForKey:@"comment"];
                                                        comment.mNumLikes = [commentObj objectForKey:@"like"];
                                                        comment.mNumDisikes = [commentObj objectForKey:@"dislike"];
                                                        comment.mTimeDiff = [commentObj objectForKey:@"timeDiff"];
                                                        comment.mTimeCreatedFloat = [[commentObj objectForKey:@"created"] floatValue];
                                                        
                                                        
                                                        // user
                                                        id userobj = [commentObj objectForKey:@"user"];
                                                        NSString *username = [userobj objectForKey:@"username"];
                                                        NSString *displayname = [userobj objectForKey:@"displayname"];
                                                        NSString *profileURL = NULL;
                                                        
                                                        NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                        NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                        if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                            profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];
                                                        }
                                                        else{
                                                            profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                        }
                                                        
                                                        comment.mUsername = displayname;
                                                        comment.mProfileURLString = profileURL;
                                                        
  
                                                        
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"posting done");
                                                            [delegate returnCommentComplete:YES Comment:comment];
                                                        }
                                                        else{
                                                            NSLog(@"posting failed");
                                                            [delegate returnCommentComplete:NO Comment:NULL];
                  

                                                        }
                                                        
                                                        
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail");
                                                        
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];

        
}


- (void) likeOrDislikeComment:(NSNumber*)commentId isLike:(BOOL)isLike sender:(id<NetworkDelegate>)delegate{
    
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/comment/likeComment.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];

    [query addArgumentWithKey:@"commentid" value:[NSString stringWithFormat:@"%@", commentId]];
    [query addArgumentWithKey:@"likeType" value:isLike?@"0":@"1"];
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"vote success");
                                                            [delegate returnCommentLikeDislikeComplete:YES CommentId:commentId];
                                                        }
                                                        else{
                                                            NSLog(@"vote unsuccessful");
                                                            [delegate returnCommentLikeDislikeComplete:NO CommentId:commentId];
                                                        }
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
    
    
    
    
    
}

#pragma mark - Fetch Results
- (void) fetchResultsWithType:(ResultRequestType)type isNew:(BOOL)isNew userId:(NSNumber*)userId sender:(id<NetworkDelegate>)delegate{
    if(isNew)
        mPageResults = 1;

    if(type == Voted){
        NSURL *url = [NetworkQueryBuilder getResultsVotedAtPage:mPageResults userId:userId];
        [self fetchResultsVotedWithURL:url isNew:isNew userId:userId sender:delegate];
    }
    else if(type == Posted){
        NSURL *url = [NetworkQueryBuilder getResultsPostedAtPage:mPageResults userId:userId];
        [self fetchResultsPostedWithURL:url isNew:isNew userId:userId sender:delegate];
    }
    else if(type == Bookmarks){
        NSURL *url = [NetworkQueryBuilder getResultsBookmarsAtPage:mPageResults userId:userId];
        [self fetchResultsBookmarksWithURL:url isNew:isNew userId:userId sender:delegate];
    }
       
    mPageResults++;
}

- (void) fetchResultsVotedWithURL:(NSURL*)url isNew:(BOOL)isNew userId:(NSNumber*)userId sender:(id<NetworkDelegate>)delegate{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                   
                                                        NSDictionary *jsonDict = (NSDictionary *) JSON;
                                                        NSArray *acts = [jsonDict objectForKey:@"acts"];
                                                        
                                                        // single items
                                                        [acts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                            id post = [obj objectForKey:@"post"];
                                                            if([post isEqual:[NSNull null]])
                                                                return;
                                                            
                                                            NSNumber *postId = [post objectForKey:@"postid"];
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];

                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];
                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }
                                                            
                                                            // item
                                                            NSString *title = [NetworkManager replaceHTMLCodes:[post objectForKey:@"title"]];
                                                            
                                                            NSString *description = [NetworkManager replaceHTMLCodes:[post objectForKey:@"description"]];
                                                            
                                                            NSString *labelLeft = [post objectForKey:@"itemid1"];
                                                            NSString *labelRight = [post objectForKey:@"itemid2"];
                                                            NSString *picLeft = [post objectForKey:@"itemurl1"];
                                                            NSString *picRight = [post objectForKey:@"itemurl2"];
                                                            
                                                            NSNumber *votesLeft = [post objectForKey:@"vote1"];
                                                            NSNumber *votesRight = [post objectForKey:@"vote2"];
                                                            
                                                            NSNumber *numComments = [post objectForKey:@"comments"];
                                                            
                                                            NSNumber *dateUpdated = [post objectForKey:@"updated"];
                                                            if([dateUpdated isEqual:[NSNull null]])
                                                                dateUpdated = [NSNumber numberWithInt:0];
                                                            NSNumber *dateCreated = [post objectForKey:@"created"];
                                                            
                                                            NSNumber *myVote = [post objectForKey:@"myvote"];
                                                            
                                                            NSNumber *posttype = [post objectForKey:@"posttype"];
                                                            
                                                            PostingItem *item;
                                                            
                                                            if([posttype intValue] == 1){
                                                                item = [[PostingItem alloc] initWithID:postId
                                                                                              DisplayName:displayname
                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:NULL
                                                                                              PicRight:NULL
                                                                                             PicSingle:picLeft
                                                                                          NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:[votesLeft intValue]
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            else{
                                                                item = [[PostingItem alloc] initWithID:postId
                                                                                              DisplayName:displayname
                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:picLeft
                                                                                              PicRight:picRight
                                                                                             PicSingle:NULL
                                                                                          NumVotesLeft:[votesLeft intValue] NumVotesRight:[votesRight intValue] NumVotesSingle:0
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            
                                                            if([myVote intValue] == 0)
                                                                item.mMyVote = NotVoted;
                                                            else if([myVote intValue] == 1)
                                                                item.mMyVote = VoteLeft;
                                                            else
                                                                item.mMyVote = VoteRight;
                                                            
                                                            [result addObject:item];
                                                            
                                                            
                                                        }];
                                                        //NSLog(@"delegate call");
                                                        [delegate returnFetchedItems:result newItems:isNew isEnd:([acts count] < 10)];
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
}


- (void) fetchResultsPostedWithURL:(NSURL*)url isNew:(BOOL)isNew userId:(NSNumber*)userId sender:(id<NetworkDelegate>)delegate{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSDictionary *jsonDict = (NSDictionary *) JSON;
                                                        NSArray *posts = [jsonDict objectForKey:@"posts"];
                                                        
                                                        // single items
                                                        [posts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                             // post id
                                                            NSNumber *postId = [obj objectForKey:@"postid"];
                                                            
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];
                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];

                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }
                                                            
                                                            // item
                                                            NSString *title = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"title"]];
                                                            
                                                            NSString *description = [NetworkManager replaceHTMLCodes:[obj objectForKey:@"description"]];
                                                            
                                                            NSString *labelLeft = [obj objectForKey:@"itemid1"];
                                                            NSString *labelRight = [obj objectForKey:@"itemid2"];
                                                            NSString *picLeft = [obj objectForKey:@"itemurl1"];
                                                            NSString *picRight = [obj objectForKey:@"itemurl2"];
                                                            
                                                            NSNumber *votesLeft = [obj objectForKey:@"vote1"];
                                                            NSNumber *votesRight = [obj objectForKey:@"vote2"];
                                                            
                                                            NSNumber *numComments = [obj objectForKey:@"comments"];
                                                            
                                                            NSNumber *dateUpdated = [obj objectForKey:@"updated"];
                                                            if([dateUpdated isEqual:[NSNull null]])
                                                                dateUpdated = [NSNumber numberWithInt:0];
                                                            NSNumber *dateCreated = [obj objectForKey:@"created"];
                                                            
                                                            NSNumber *myVote = [obj objectForKey:@"myvote"];
                                                            
                                                            NSNumber *posttype = [obj objectForKey:@"posttype"];
                                                            
                                                            PostingItem *item;
                                                            
                                                            if([posttype intValue] == 1){
                                                                item = [[PostingItem alloc] initWithID:postId 
                                                                                              DisplayName:displayname                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:NULL
                                                                                              PicRight:NULL
                                                                                             PicSingle:picLeft
                                                                                          NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:[votesLeft intValue]
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            else{
                                                                item = [[PostingItem alloc] initWithID:postId 
                                                                                              DisplayName:displayname                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:picLeft
                                                                                              PicRight:picRight
                                                                                             PicSingle:NULL
                                                                                          NumVotesLeft:[votesLeft intValue] NumVotesRight:[votesRight intValue] NumVotesSingle:0
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            
                                                            if([myVote intValue] == 0)
                                                                item.mMyVote = NotVoted;
                                                            else if([myVote intValue] == 1)
                                                                item.mMyVote = VoteLeft;
                                                            else
                                                                item.mMyVote = VoteRight;
                                                            
                                                            [result addObject:item];
                                                             
                                                        }];
                                                        
                                                        [delegate returnFetchedItems:result newItems:isNew isEnd:([posts count] < 10)];
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];

    
}


- (void) fetchResultsBookmarksWithURL:(NSURL*)url isNew:(BOOL)isNew userId:(NSNumber*)userId sender:(id<NetworkDelegate>)delegate{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSDictionary *jsonDict = (NSDictionary *) JSON;
                                                        NSArray *acts = [jsonDict objectForKey:@"acts"];
                                                        
                                                        // single items
                                                        [acts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                                            id post = [obj objectForKey:@"post"];
                                                            if([post isEqual:[NSNull null]])
                                                                return;
                                                            
                                                            NSNumber *postId = [post objectForKey:@"postid"];
                                                            // user
                                                            id userobj = [obj objectForKey:@"user"];
                                                            NSString *username = [userobj objectForKey:@"username"];
                                                            NSString *displayname = [userobj objectForKey:@"displayname"];
                                                            NSString *profileURL = NULL;
                                                            
                                                            NSNumber *facebookUser = [userobj objectForKey:@"facebookUser"];
                                                            NSString *photoImg = [userobj objectForKey:@"photoImg"];
                                                            if([photoImg isEqual: [NSNull null]] && [facebookUser isEqualToNumber:[NSNumber numberWithInt:1]]){
                                                                profileURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [username substringFromIndex:3]];

                                                            }
                                                            else{
                                                                profileURL = [NetworkQueryBuilder getImageURLString:photoImg];
                                                            }
                                                            
                                                            // item
                                                            NSString *title = [NetworkManager replaceHTMLCodes:[post objectForKey:@"title"]];
                                                            
                                                            NSString *description = [NetworkManager replaceHTMLCodes:[post objectForKey:@"description"]];
                                                            
                                                            NSString *labelLeft = [post objectForKey:@"itemid1"];
                                                            NSString *labelRight = [post objectForKey:@"itemid2"];
                                                            NSString *picLeft = [post objectForKey:@"itemurl1"];
                                                            NSString *picRight = [post objectForKey:@"itemurl2"];
                                                            
                                                            NSNumber *votesLeft = [post objectForKey:@"vote1"];
                                                            NSNumber *votesRight = [post objectForKey:@"vote2"];
                                                            
                                                            NSNumber *numComments = [post objectForKey:@"comments"];
                                                            
                                                            NSNumber *dateUpdated = [post objectForKey:@"updated"];
                                                            if([dateUpdated isEqual:[NSNull null]])
                                                                dateUpdated = [NSNumber numberWithInt:0];
                                                            NSNumber *dateCreated = [post objectForKey:@"created"];
                                                            
                                                            NSNumber *myVote = [post objectForKey:@"myvote"];
                                                            
                                                            NSNumber *posttype = [post objectForKey:@"posttype"];
                                                            
                                                            PostingItem *item;
                                                            
                                                            if([posttype intValue] == 1){
                                                                item = [[PostingItem alloc] initWithID:postId
                                                                                             DisplayName:displayname
                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:NULL
                                                                                              PicRight:NULL
                                                                                             PicSingle:picLeft
                                                                                          NumVotesLeft:0 NumVotesRight:0 NumVotesSingle:[votesLeft intValue]
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            else{
                                                                item = [[PostingItem alloc] initWithID:postId
                                                                                            DisplayName:displayname                                                                                                 Title:title
                                                                                           Description:description
                                                                                             LabelLeft:labelLeft LabelRight:labelRight
                                                                                            ProfileURL:profileURL
                                                                                               PicLeft:picLeft
                                                                                              PicRight:picRight
                                                                                             PicSingle:NULL
                                                                                          NumVotesLeft:[votesLeft intValue] NumVotesRight:[votesRight intValue] NumVotesSingle:0
                                                                                           NumComments:[numComments intValue]
                                                                                             NumShared:0
                                                                                      DateCreatedMilli:[dateCreated floatValue]
                                                                                      DateUpdatedMilli:[dateUpdated floatValue]];
                                                            }
                                                            
                                                            if([myVote intValue] == 0)
                                                                item.mMyVote = NotVoted;
                                                            else if([myVote intValue] == 1)
                                                                item.mMyVote = VoteLeft;
                                                            else
                                                                item.mMyVote = VoteRight;
                                                            
                                                            [result addObject:item];
                                                            
                                                            
                                                        }];
                                                        
                                                        [delegate returnFetchedItems:result newItems:isNew isEnd:([acts count] < 10)];
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
    

}



#pragma mark - Friends & Following
- (BOOL) addToFriends:(NSString*)userID{
    return YES;
}

- (BOOL) removeFriend:(NSString*)userID{
    return YES;
}

- (NSMutableArray*) getFriends{
    return NULL;
}


#pragma mark - Posting Request
-(UIImage*)resizedImage:(UIImage*)inImage  inRect:(CGRect)thumbRect {
    // Creates a bitmap-based graphics context and makes it the current context.
    UIGraphicsBeginImageContext(thumbRect.size);
    [inImage drawInRect:thumbRect];
    return UIGraphicsGetImageFromCurrentImageContext();
}


- (void) postPictureWithImage:(UIImage*)image side:(Side)side sender:(id<NetworkDelegate>)delegate {
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post"];
    NSURL *url = [NSURL URLWithString:query.str];
 
    
    //NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[SharedProfile getUsername] forKey:@"username"];
    [parameters setObject:[SharedProfile getPassword] forKey:@"password"];
    
    NSString *photoName= @"photo.png";
    
    // the path to write file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:photoName];
    //NSData * photoImageData = UIImagePNGRepresentation([self resizedImage:image inRect:CGRectMake(0, 0, 400, image.size.height * 400/image.size.width)]);
    NSData * photoImageData = UIImagePNGRepresentation(image);
    [photoImageData writeToFile:filePath atomically:YES];
    
    NSLog(@"photo written to path: %@", filePath);
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                         path:@"uploadPhoto.so"
                                                                   parameters:parameters
                                                    constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                                                        
                                                        [formData appendPartWithFileData:photoImageData
                                                                                    name:@"photo"
                                                                                fileName:filePath
                                                                                mimeType:@"image/png"];
        //[formData appendPartWithFileData:imageData name:@"files" fileName:photoName mimeType:@"image/jpeg"];
       // [formData appendPartWithFormData:[key2 dataUsingEncoding:NSUTF8StringEncoding] name:@"key2"];
        // etc.
                                                        NSLog(@"SEND!");
    }];
    
    AFJSONRequestOperation *operation =
        [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                            NSLog(@"success - upload pic");
//                                                            NSLog(@"response: %@",[respons]);
                                                            
                                                            NSNumber *num = [JSON valueForKey:@"result"];
                                                            
                                                            if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                                NSLog(@"%@", [JSON valueForKey:@"filename"]);
                                                                [delegate returnImagePostedURL:[JSON valueForKey:@"filename"] side:side];
                                                                
                                                            }
#warning to-do : if upload fails..
                                                            else{
                                                                NSLog(@"returned %@", num);
                                                                [delegate networkManagerError];
                                                            }

         
                                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                            NSLog(@"fail");
                                                            [delegate networkManagerError];
                                                        }];
   /*
     
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
      //  NSNumber *num = [responseObject valueForKeyPath:@"result"];
      //  NSString *err = [responseObject valueForKeyPath:@"msg"];
        NSLog(@"SUCCESS");

        NSLog(@"response: %@",[operation responseString]);

        NSNumber *num = [operation valueForKey:@"result"];
        return;

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"FAIL");
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
 */
    
    [operation start];
}


- (void) createPostWithImage1URL:(NSString*)image1url
                       Image2URL:(NSString*)image2url
                       itemname1:(NSString*)item1name
                       itemname2:(NSString*)item2name
                           title:(NSString*)title
                     description:(NSString*)desc
                        category:(int)category
                          sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post"];
    NSURL *url = [NSURL URLWithString:query.str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[SharedProfile getUsername] forKey:@"username"];
    [parameters setObject:[SharedProfile getPassword] forKey:@"password"];
    
    [parameters setObject:title forKey:@"title"];
    [parameters setObject:[NSString stringWithFormat:@"%@", image1url] forKey:@"itemurl1"];
    [parameters setObject:[NSString stringWithFormat:@"%d", category] forKey:@"categoryid"];
    [parameters setObject:[NSString stringWithFormat:@"%d", 1] forKey:@"posttype"];
    
    if(image2url){
        [parameters setObject:[NSString stringWithFormat:@"%d", 2] forKey:@"posttype"];
        [parameters setObject:[NSString stringWithFormat:@"%@", image2url] forKey:@"itemurl2"];
        
    }
    
    if(![desc isEqualToString:@""]){
        [parameters setObject:desc forKey:@"description"];
    }

    if(![item1name isEqualToString:@""]){
        [parameters setObject:item1name forKey:@"itemid1"];
    }
    if(![item2name isEqualToString:@""]){
        [parameters setObject:item2name forKey:@"itemid2"];
    }
        
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"create.so"
                                                      parameters:parameters];
                                    
                                    /*multipartFormRequestWithMethod:@"POST"
                                                                         path:@"create.so"
                                                                   parameters:parameters
                                                    constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                                                        NSLog(@"SEND!");
                                                    }];
                                     
                                     */
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        NSLog(@"success - create post");
                                                       
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"posting done");
                                                            [delegate postingSuccess:YES];
                                                        }
                                                        else{
                                                            NSLog(@"posting failed");
                                                            [delegate postingSuccess:NO];
                                                        }
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"fail");
                                                        [delegate networkManagerError];
                                                    }];
    [operation start];
}


#pragma mark - report post
- (void) reportPost:(NSNumber*)postId memo:(NSString*)memo sender:(id<NetworkDelegate>)delegate{
    NSLog(@"network manager: report %@", postId);

    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"report.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postId]];
    [query addArgumentWithKey:@"reportcode" value:@"1"];
    if(memo)
        [query addArgumentWithKey:@"memo" value:memo];
    
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"report success");
                                                            [delegate returnReportPost:YES];
                                                        }
                                                        else{
                                                            NSLog(@"report unsuccessful");
                                                            [delegate returnReportPost:NO];
                                                        }
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];

}


#pragma mark - vote & unvote
- (void) votePostID:(NSNumber*)postid left:(BOOL)isLeft sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/vote.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    NSLog(@"userid = %@", [SharedProfile userId]);
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postid]];
    [query addArgumentWithKey:@"itemNum" value:isLeft?@"1":@"2"];
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                      
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"vote success");
                                                            [delegate returnVotingComplete:YES];
                                                        }
                                                        else{
                                                            NSLog(@"vote unsuccessful");
                                                            [delegate returnVotingComplete:NO];
                                                        }

                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
}

- (void) unvotePostID:(NSNumber *)postid sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/unvote.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    NSLog(@"userid = %@", [SharedProfile userId]);
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postid]];

    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"unvote success");
                                                            [delegate returnVotingComplete:YES];
                                                        }
                                                        else{
                                                            NSLog(@"unvote unsuccessful");
                                                            [delegate returnVotingComplete:NO];
                                                        }
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];
}


    
    
#pragma mark - Bookmark
/*
 username	String	로그인 아이디
 password	string
 postid	int	bookmark할 포스트 ID
 */

- (void) bookmarkPostID:(NSNumber*)postid sender:(id<NetworkDelegate>)delegate{
    
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/addBookmark.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    NSLog(@"userid = %@", [SharedProfile userId]);
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postid]];
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"bookmark success");
                                                            [delegate returnBookmarkCommit:YES isBookmark:YES];
                                                        }
                                                        else{
                                                            NSLog(@"bookmark unsuccessful");
                                                            [delegate returnBookmarkCommit:NO isBookmark:YES];
                                                        
                                                        }
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];

}
- (void) removeBookmarkPostID:(NSNumber*)postid sender:(id<NetworkDelegate>)delegate{
    NetworkQueryBuilder *query = [[NetworkQueryBuilder alloc] initWithURL:@"post/removeBookmark.so"];
    [query addArgumentWithKey:@"username" value:[NSString stringWithFormat:@"%@", [SharedProfile getUsername]]];
    NSLog(@"userid = %@", [SharedProfile userId]);
    [query addArgumentWithKey:@"password" value:[SharedProfile getPassword]];
    [query addArgumentWithKey:@"postid" value:[NSString stringWithFormat:@"%@", postid]];
    
    NSURL *url = [NSURL URLWithString:query.str];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        
                                                        NSNumber *num = [JSON valueForKey:@"result"];
                                                        
                                                        if([num isEqualToNumber:[NSNumber numberWithInt:0]]){
                                                            NSLog(@"removeBookmark.so success");
                                                            [delegate returnBookmarkCommit:YES isBookmark:NO];
                                                        }
                                                        else{
                                                            NSLog(@"removeBookmark.so unsuccessful");
                                                            [delegate returnBookmarkCommit:NO isBookmark:NO];
                                                        }
                                                        
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        NSLog(@"Network Error");
                                                        [delegate networkManagerError];
                                                    }];
    
    [operation start];

}


#pragma mark - misc
+(NSString *) replaceHTMLCodes:(NSString *)text{
    
    if([text isEqual:[NSNull null]])
        return NULL;
    @try {
        if (text){
            NSString *tmpString=[NSString stringWithString:text];
            tmpString = [text copy];
            NSString *tmpText = @"";
            int locAmp = [tmpString rangeOfString:@"&"].location;
            NSString * Code = @"";
            int locComa;
            
            while (locAmp!=NSNotFound && locAmp!=-1) {
                tmpText = [tmpText stringByAppendingString:[tmpString substringToIndex:locAmp]];
                tmpString = [tmpString stringByReplacingCharactersInRange:NSMakeRange(0, locAmp) withString:@""];
                locComa = [tmpString rangeOfString:@";"].location;
                Code = [NSString stringWithString:[tmpString substringWithRange:NSMakeRange(0, locComa)]];
                Code = [Code stringByReplacingOccurrencesOfString:@"&" withString:@""];
                if ([Code characterAtIndex:0]=='#') {
                    Code = [Code stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    tmpText = [tmpText stringByAppendingFormat:@"%C", (unsigned short)[Code intValue]];
                } else {
                    if ([Code compare:@"amp"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@"&"];
                    } else if ([Code compare:@"quot"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@"\""];
                    } else if ([Code compare:@"gt"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@">"];
                    } else if ([Code compare:@"lt"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@"<"];
                    } else if ([Code compare:@"laquo"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@"«"];
                    } else if ([Code compare:@"raquo"]==NSOrderedSame) {
                        tmpText = [tmpText stringByAppendingString:@"»"];
                    }
                }
                tmpString = [tmpString stringByReplacingCharactersInRange:NSMakeRange(0, locComa+1) withString:@""];
                locAmp = [tmpString rangeOfString:@"&"].location;
            }
            tmpText = [tmpText  stringByAppendingString:tmpString];
            return tmpText;
        }

    }
    @catch (NSException *exception) {
        return text;
    }
    @finally {
        ;
    }

        
}
 @end
