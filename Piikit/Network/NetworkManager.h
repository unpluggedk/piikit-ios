//
//  NetworkManager.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostingItem.h"
#import "Comment.h"

typedef NS_ENUM(NSInteger, PostRequestType){
    Recent,
    Popular,
    Following
};

typedef NS_ENUM(NSInteger, ResultRequestType){
    Voted,
    Posted,
    Bookmarks,
    Voted_New
};

typedef NS_ENUM(NSInteger, Side){
    Left,
    Right
};

@protocol NetworkDelegate
@optional
-(void)returnUsernameAvailable:(BOOL)available username:(NSString*)username;
-(void)returnSignin:(BOOL)success;
-(void)returnEmailAvailable:(BOOL)available email:(NSString*)email;
-(void)returnSignup:(BOOL)success message:(NSString*)msg;

-(void)returnFetchedItems:(NSArray*)items newItems:(BOOL)isNew isEnd:(BOOL)isEnd;

-(void)returnImagePostedURL:(NSString*)urlStr side:(Side)side;
-(void)postingSuccess:(BOOL)success;

-(void)returnSinglePost:(PostingItem*)returnedItem itemExist:(BOOL)itemExist;
-(void)returnVotingComplete:(BOOL)success;

-(void)returnComments:(NSArray*)comments newItems:(BOOL)isNew endOfComments:(BOOL)isEnd;
-(void)returnCommentComplete:(BOOL)success Comment:(Comment*)comment;
-(void)returnCommentLikeDislikeComplete:(BOOL)success CommentId:(NSNumber*)commentId;

-(void)returnProfilePostedURL:(NSString*)urlStr;

-(void)returnProfileUpdate:(BOOL)success;
- (void)returnProfile:(BOOL)success
          displayname:(NSString *)displaynameStr
             birthday:(NSString *)birthdayStr
                email:(NSString *)emailStr
                  sex:(NSString *)sexStr
         successPopUp:(NSString*)successPopUp;

- (void)returnBookmarkCommit:(BOOL)success isBookmark:(BOOL)isBookmark;
- (void)returnReportPost:(BOOL)success;

@required
-(void)networkManagerError;

@end



@interface NetworkManager : NSObject{
    int mPageHome;
    int mPageResults;
    int mPageComments;
    

}

@property (nonatomic, retain) NSString *username;


+(id)sharedNetworkManager;

// ======================================================
// connection
// ======================================================
// login
- (void) connectWithUsername:(NSString*)username email:(NSString*)email password:(NSString*)password sender:(id<NetworkDelegate>)delegate;
- (void) connectWithFacebook:(NSString*)accessToken sender:(id<NetworkDelegate>)delegate;


// logout
- (void) disconnect;
- (BOOL) isConnected;

// ======================================================
// signup & profile
// ======================================================
- (void) checkUsernameAvailable:(NSString*)username sender:(id<NetworkDelegate>)delegate;
- (void) checkEmailAvailable:(NSString*)email sender:(id<NetworkDelegate>)delegate;
- (void) createAccount:(NSString*)username password:(NSString*)password email:(NSString*)email birthdate:(NSString*)birthdate
                   sex:(NSString*)sex country:(NSString*)country receiveupdates:(NSString*)receiveupdates status:(NSString*)status sender:(id<NetworkDelegate>)delegate;
- (void) postProfilePictureWithImage:(UIImage*)image sender:(id<NetworkDelegate>)delegate ;
- (void) editProfileWithUsername:(NSString*)username
                        password:(NSString*)password
                     newPassword:(NSString*)newPassword
                     displayname:(NSString*)displayname
                       birthdate:(NSString*)birthdate
                        joindate:(NSString*)joindate
                             sex:(NSString*)sex
                         country:(NSString*)country
                           email:(NSString*)email
                          sender:(id<NetworkDelegate>)delegate;


- (void) getProfileWithSuccessPopUp:(NSString*)successPopUp sender:(id<NetworkDelegate>)delegate;

// ======================================================
// fetching posts & results
// ======================================================

- (void) fetchNewCategory:(NSNumber*)catId sender:(id<NetworkDelegate>)delegate;
- (void) fetchNewPostings:(PostRequestType)type sender:(id<NetworkDelegate>)delegate;
- (void) fetchMorePostings:(PostRequestType)type category:(NSNumber*)catId sender:(id<NetworkDelegate>)delegate;
- (void) fetchPosting:(PostRequestType)type category:(NSNumber*)catId sender:(id<NetworkDelegate>)delegate;
- (void) fetchSinglePosting:(NSNumber*)itemId sender:(id<NetworkDelegate>)delegate;

- (void) fetchResultsWithType:(ResultRequestType)type isNew:(BOOL)isNew userId:(NSNumber*)userId sender:(id<NetworkDelegate>)delegate;

// ======================================================
// comments
// ======================================================
- (void) fetchNewComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
- (void) fetchMoreComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;

- (void) fetchComments:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
- (void) postComment:(NSNumber*)postId comment:(NSString*)comment sender:(id<NetworkDelegate>)delegate;

- (void) likeOrDislikeComment:(NSNumber*)commentId isLike:(BOOL)isLike sender:(id<NetworkDelegate>)delegate;

//- (void) deleteComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
//- (void) likeComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;
//- (void) dislikeComment:(NSNumber*)postId sender:(id<NetworkDelegate>)delegate;



// ======================================================
// post creation
// ======================================================
// upload posting
// return YES for success, NO for failure
- (void) postPictureWithImage:(UIImage*)image side:(Side)side sender:(id<NetworkDelegate>)delegate ;
- (void) createPostWithImage1URL:(NSString*)image1url
                       Image2URL:(NSString*)image2url
                       itemname1:(NSString*)item1name
                       itemname2:(NSString*)item2name
                           title:(NSString*)title
                     description:(NSString*)desc
                        category:(int)category
                          sender:(id<NetworkDelegate>)delegate;

// ======================================================
// post report
// ======================================================
- (void) reportPost:(NSNumber*)postId memo:(NSString*)memo sender:(id<NetworkDelegate>)delegate;

// ======================================================
// friends
// ======================================================
- (BOOL) addToFriends:(NSString*)userID;
- (BOOL) removeFriend:(NSString*)userID;

- (NSMutableArray*) getFriends;




// ======================================================
// Vote & Unvote
// ======================================================
- (void) votePostID:(NSNumber*)postid left:(BOOL)isLeft sender:(id<NetworkDelegate>)delegate;
- (void) unvotePostID:(NSNumber*)postid sender:(id<NetworkDelegate>)delegate;

//http://test.piikit.com:8080/api/post/vote.so?username=highroller&password=k124sj06&postid=122&itemNum=1
//cherk
// http://test.piikit.com:8080/api/post/view/122.so?userid=8


// ======================================================
// Bookmark
// ======================================================
- (void) bookmarkPostID:(NSNumber*)postid sender:(id<NetworkDelegate>)delegate;
- (void) removeBookmarkPostID:(NSNumber*)postid sender:(id<NetworkDelegate>)delegate;



// ======================================================
// Misc
// ======================================================
+(NSString *) replaceHTMLCodes:(NSString *)text;

@end
