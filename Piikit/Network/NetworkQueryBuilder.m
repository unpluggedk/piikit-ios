//
//  NetworkQueryBuilder.m
//  Piikit
//
//  Created by Jason Kim on 13. 6. 14..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "NetworkQueryBuilder.h"

//#define TEST

@implementation NetworkQueryBuilder

@synthesize str;

- (id)initWithURL:(NSString*)url{
    if(self = [super init])
#ifdef TEST
        str = [NSMutableString stringWithFormat:@"http://test.piikit.com:8080/api/%@?",url];
#else
        str = [NSMutableString stringWithFormat:@"http://www.piikit.com/api/%@?",url];
#endif
    return self;
}


- (void)addArgumentWithKey:(NSString*)key value:(NSString*)value{
    [str appendFormat:@"%@=%@&", key, value];
}

+ (NSURL*)getHomeItemsAtPage:(int)page{
#ifdef TEST
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/home_more.so?page=%d", page]];
#else
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/home_more.so?page=%d", page]];
#endif
}

+ (NSURL*)getHomeItemsAtPage:(int)page category:(NSNumber*)catId  userId:(NSNumber*)userId{
    
    NSMutableString *str;
    
#ifdef Test
    str = [NSMutableString stringWithFormat:@"http://test.piikit.com:8080/"];
#else
    str = [NSMutableString stringWithFormat:@"http://www.piikit.com/"];
#endif

    if(catId)
        [str appendFormat:@"home_more/cat.so?page=%d&catId=%@", page, catId];
    else
        [str appendFormat:@"home_more.so?page=%d", page];
    
    if(userId)
        [str appendFormat:@"&userid=%@", userId];
    
    return [NSURL URLWithString:str];
    
    /*
    if(catId){
   //     NSLog(@"%@", [NSString stringWithFormat:@"http://test.piikit.com:8080/home_more/cat.so?page=1&catId=%@&userid=%@", catId, userId]);
#ifdef TEST
        return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/home_more/cat.so?page=%d&catId=%@&userid=%@", page, catId, userId]];
#else
        return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/home_more/cat.so?page=%d&catId=%@&userid=%@", page, catId, userId]];
#endif
    }
    
  //  NSLog(@"%@", [NSString stringWithFormat:@"http://www.piikit.com/home_more.so?page=%d&userid=%@", page, userId]);

#ifdef TEST
   
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/home_more.so?page=%d&userid=%@", page, userId]];
#else
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/home_more.so?page=%d&userid=%@", page, userId]];
#endif
    */
}


+ (NSURL*)getResultsVotedAtPage:(int)page userId:(NSNumber*)userId{
   // NSLog(@"%@",[NSString stringWithFormat:@"http://test.piikit.com:8080/api/mypage/votesComments.so?page=%d&userid=%@", page, userId] );
#ifdef TEST
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/api/mypage/votesComments.so?page=%d&userid=%@", page, userId]];
#else
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/api/mypage/votesComments.so?page=%d&userid=%@", page, userId]];
#endif
}

+ (NSURL*)getResultsPostedAtPage:(int)page userId:(NSNumber*)userId{
    
#ifdef TEST
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/api/mypage/posts.so?page=%d&userid=%@", page, userId]];
#else
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/api/mypage/posts.so?page=%d&userid=%@", page, userId]];
#endif
}

+ (NSURL*)getResultsBookmarsAtPage:(int)page userId:(NSNumber*)userId{
#ifdef TEST
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://test.piikit.com:8080/api/mypage/bookmarks.so?page=%d&userid=%@", page, userId]];
#else
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.piikit.com/api/mypage/bookmarks.so?page=%d&userid=%@", page, userId]];
#endif
}


+ (NSURL*)getFacebookLogin{
#ifdef TEST
    return [NSURL URLWithString:@"http://test.piikit.com:8080/facebook"];
#else
    return [NSURL URLWithString:@"http://www.piikit.com/facebook"];
#endif
}



+ (NSString*)getImageURLString:(NSString*)url{
#ifdef TEST
    return [NSString stringWithFormat:@"http://test.piikit.com:8080/static/uploads/photo/%@", url];
#else
    return [NSString stringWithFormat:@"http://image.piikit.com/uploads/photo/%@", url];
#endif
}







- (void)print{
    NSLog(@"%@", str);
}
@end
