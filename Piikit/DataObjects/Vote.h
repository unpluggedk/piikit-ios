//
//  Vote.h
//  Piikit
//
//  Created by Jason Kim on 13. 8. 15..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vote : NSObject{
    NSDate *time;
    BOOL isVote;
    BOOL isLeft;
    
   // BOOL isPreviouslyVoted;
    NSNumber *itemId;
}

@property (nonatomic, retain) NSDate *time;
@property (nonatomic, assign) BOOL isVote;
@property (nonatomic, assign) BOOL isLeft;

@property (nonatomic, retain) NSNumber *itemId;

//@property (nonatomic, assign) BOOL isPreviouslyVotes;

@end
