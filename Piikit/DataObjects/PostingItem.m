//
//  PostingItem.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "PostingItem.h"

@implementation PostingItem

@synthesize mDisplayName;
@synthesize mLabelLeft, mLabelRight;
@synthesize mUserProfileURL;
@synthesize mPicLeft, mPicRight;
@synthesize mDateStr;
@synthesize mNumComments,mPicSingle,mNumShared;
@synthesize mItemID;
@synthesize mTitle;
@synthesize mDescription;
@synthesize mDateCreatedMilliseconds;
@synthesize mDateUpdatedMilliseconds;
@synthesize mMyVote;
@synthesize mAppVote;
@synthesize mIsBookmarked;
@synthesize mVotesLeft;
@synthesize mVotesRight;
@synthesize mVotesSingle;
@synthesize mVotesLeftMale;
@synthesize mVotesLeftFemale;
@synthesize mVotesRightMale;
@synthesize mVotesRightFemale;
@synthesize mVotesLeftUnder21;
@synthesize mVotesLeft21to30;
@synthesize mVotesLeftOver30;
@synthesize mVotesRightUnder21;
@synthesize mVotesRight21to30;
@synthesize mVotesRightOver30;




static NSDateFormatter *dateFormatter = NULL;

- (id) initWithID:(NSNumber*)postingID
      DisplayName:(NSString*)displayname
            Title:(NSString*)title
      Description:(NSString*)description
        LabelLeft:(NSString*)labelLeft
       LabelRight:(NSString*)labelRight
       ProfileURL:(NSString*)profileURL
          PicLeft:(NSString*)picLeft
         PicRight:(NSString*)picRight
        PicSingle:(NSString*)picSingle
     NumVotesLeft:(int)numVotesLeft
    NumVotesRight:(int)numVotesRight
   NumVotesSingle:(int)numVotesSingle
      NumComments:(int)numComments
        NumShared:(int)numShared
 DateCreatedMilli:(float)dateCreatedMilli
 DateUpdatedMilli:(float)dateUpdatedMilli{
    self = [super init];
    if (self) {
        // Initialize self.
        mItemID = postingID;
        mDisplayName = displayname;
        mTitle = title;
        mDescription = description;
        mLabelLeft = labelLeft;
        mLabelRight = labelRight;
        mUserProfileURL = profileURL;
        mPicLeft = picLeft;
        mPicRight = picRight;
        mPicSingle = picSingle;
        mVotesLeft = numVotesLeft;
        mVotesRight = numVotesRight;
        mVotesSingle = numVotesSingle;
        mNumShared = numShared;
        mNumComments = numComments;
        mDateCreatedMilliseconds = dateCreatedMilli/1000;
        mDateUpdatedMilliseconds = dateUpdatedMilli/1000;
        
        mMyVote = NotVoted;
        
        //NSDate *date = [NSDate dateWithTimeIntervalSince1970:mDateUpdatedMilliseconds];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:mDateCreatedMilliseconds];
        
        
        if(!dateFormatter){
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yy h:mma"];

            [dateFormatter setAMSymbol:@"am"];
            [dateFormatter setPMSymbol:@"pm"];
        }
        
        NSString *formattedDateString = [dateFormatter stringFromDate:date];
        mDateStr = formattedDateString;
        mIsBookmarked = NO;
        
    }
    return self;
}

- (BOOL)isPostingSingle{
    return (mPicSingle != NULL);
}

@end
