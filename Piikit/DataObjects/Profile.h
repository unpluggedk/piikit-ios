//
//  Profile.h
//  Piikit
//
//  Created by Jason Kim on 13. 6. 23..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface Profile : NSObject{
    // preferences
    NSString* mUsername;
    NSString* mPassword;
    NSString *mEmail;
    
    NSUserDefaults *pref;
    
    // non-stored
    NSString *birthdate; // YYYY-MM-DD

    NSString *sex;
    
    NSString *displayname;
    NSString *joindate;
    NSString *country;

    NSString *profileURLString;
    
    NSNumber *userId;
    
    BOOL isLoginSkipped;
}

@property (nonatomic, retain) NSString *birthdate;
@property (nonatomic, retain) NSString *sex;
@property (nonatomic, retain) NSString *profileURLString;
@property (nonatomic, retain) NSNumber *userId;
// added
@property (nonatomic, retain) NSString *displayname;
@property (nonatomic, retain) NSString *joindate;
@property (nonatomic, retain) NSString *country;

@property (nonatomic, assign) BOOL isLoginSkipped;

+(id)sharedProfile;

- (void)setUsername:(NSString*)username;
- (NSString*)getUsername;
- (void)setPassword:(NSString*)password;
- (NSString*)getPassword;
- (void)setEmail:(NSString*)email;
- (NSString*)getEmail;


- (void)setLogin;
- (void)setLogout;
- (BOOL)isLoggedIn;

- (int) getAge;

- (void)setFacebookLogin:(BOOL)enable;
- (BOOL)isFacebookLogin;

- (void)printProfile;

- (void)clearProfile;
@end
