//
//  Profile.m
//  Piikit
//
//  Created by Jason Kim on 13. 6. 23..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "Profile.h"
#import "GCDSingleton.h"

@implementation Profile

@synthesize birthdate, sex, profileURLString, userId;
@synthesize country, displayname, joindate;

@synthesize isLoginSkipped;

+(id)sharedProfile{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}


- (id)init{
    NSLog(@"init profile data");
    pref = [NSUserDefaults standardUserDefaults];
    mUsername = [pref valueForKey:@"username"];
    mPassword = [pref valueForKey:@"password"];
    mEmail = [pref valueForKey:@"email"];
    
    return self;
}

- (void)setUsername:(NSString*)username{
    mUsername = username;
    [pref setValue:mUsername forKey:@"username"];
    [pref synchronize];
}

- (NSString*)getUsername{
    return mUsername;
}

- (void)setPassword:(NSString*)password{
    mPassword = password;
    [pref setValue:mPassword forKey:@"password"];
    [pref synchronize];
}

- (NSString*)getPassword{
    return mPassword;
}

- (void)setEmail:(NSString*)email{
    mEmail = email;
    [pref setValue:mEmail forKey:@"email"];
    [pref synchronize];
}

- (NSString*)getEmail{
    return mEmail;
}


- (void)setLogin{
    [pref setObject:@"login" forKey:@"login"];
    [pref synchronize];
}
- (void)setLogout{
    [pref removeObjectForKey:@"login"];
    [pref synchronize];
    
    [self clearProfile];
}


- (BOOL)isLoggedIn{
    if([pref objectForKey:@"login"])
        return YES;
    return NO;
}

- (void)setFacebookLogin:(BOOL)enable{
    
    [pref setObject:enable?[NSNumber numberWithBool:YES]:[NSNumber numberWithBool:NO]
             forKey:@"facebookLogin"];
    [pref synchronize];

    
}

- (BOOL)isFacebookLogin{
    if(![pref objectForKey:@"facebookLogin"])
        return NO;
    if([[pref objectForKey:@"facebookLogin"] isEqual:[NSNumber numberWithBool:NO]])
        return NO;
    return YES;
        
}


- (int) getAge{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy"];
    int currentYear = [[df stringFromDate:[NSDate date]] intValue];
    return currentYear - [[birthdate substringToIndex:4] intValue];
    
    
}

- (void) setBirthdate:(NSString *)input_birthdate{
//    NSMutableString *birthstrWithDashes = [NSMutableString stringWithString:input_birthdate];
//    birthdate = [birthstrWithDashes stringByReplacingOccurrencesOfString:@"-" withString:@""];
    birthdate = input_birthdate;
}




- (void)printProfile{
    NSLog(@"%@  %@", mUsername, mPassword);
    NSLog(@"birthdate : %@", birthdate);
    NSLog(@"joindate : %@", joindate);
    NSLog(@"email : %@", mEmail);
    NSLog(@"sex : %@", sex);
    NSLog(@"displayname: %@", displayname);
    NSLog(@"country: %@", country);
    
}

- (void)clearProfile{
    mUsername = NULL;
    mPassword = NULL;
    birthdate = NULL;
    joindate = NULL;
    mEmail = NULL;
    sex = NULL;
    displayname = NULL;
    country = NULL;
    
    [self setEmail:nil];
    [self setUsername:nil];
    [self setPassword:nil];
    
    //[self setFacebookLogin:NO];
}


@end
