//
//  PostingItem.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enums.h"

@interface PostingItem : NSObject{
    NSString *mDisplayName;

    NSString *mLabelLeft;
    NSString *mLabelRight;
    
    NSString *mUserProfileURL;
    
    NSString *mPicLeft;
    NSString *mPicRight;
    NSString *mPicSingle;
    
    NSString *mDateStr;
    
    NSString *mTitle;
    NSString *mDescription;
    
    int mVotesLeft;
    int mVotesRight;
    int mVotesSingle;
    int mNumComments;
    int mNumShared;
    
    int mVotesLeftMale;
    int mVotesLeftFemale;
    int mVotesRightMale;
    int mVotesRightFemale;
    
    int mVotesLeftUnder21;
    int mVotesLeft21to30;
    int mVotesLeftOver30;
    
    int mVotesRightUnder21;
    int mVotesRight21to30;
    int mVotesRightOver30;
    
    NSNumber* mItemID;
    
    float mDateCreatedMilliseconds;
    float mDateUpdatedMilliseconds;
    
    VoteSide mMyVote;
    VoteSide mAppVote;
    
    BOOL mIsBookmarked;
}

@property (nonatomic, retain) NSString *mDisplayName;
@property (nonatomic, retain) NSString *mLabelLeft;
@property (nonatomic, retain) NSString *mLabelRight;
@property (nonatomic, retain) NSString *mUserProfileURL;

@property (nonatomic, retain) NSString *mPicLeft;
@property (nonatomic, retain) NSString *mPicRight;
@property (nonatomic, retain) NSString *mPicSingle;
@property (nonatomic, retain) NSString *mDateStr;

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mDescription;

@property (nonatomic, assign) int mNumComments;
@property (nonatomic, assign) int mNumShared;

@property (nonatomic, assign) int mVotesLeft;
@property (nonatomic, assign) int mVotesRight;
@property (nonatomic, assign) int mVotesSingle;
@property (nonatomic, assign) int mVotesLeftMale;
@property (nonatomic, assign) int mVotesLeftFemale;
@property (nonatomic, assign) int mVotesRightMale;
@property (nonatomic, assign) int mVotesRightFemale;
@property (nonatomic, assign) int mVotesLeftUnder21;
@property (nonatomic, assign) int mVotesLeft21to30;
@property (nonatomic, assign) int mVotesLeftOver30;
@property (nonatomic, assign) int mVotesRightUnder21;
@property (nonatomic, assign) int mVotesRight21to30;
@property (nonatomic, assign) int mVotesRightOver30;




@property (nonatomic, retain) NSNumber *mItemID;

@property (nonatomic, assign) float mDateCreatedMilliseconds;
@property (nonatomic, assign) float mDateUpdatedMilliseconds;

@property (nonatomic, assign) VoteSide mMyVote;
@property (nonatomic, assign) VoteSide mAppVote;

@property (nonatomic, assign) BOOL mIsBookmarked;
- (BOOL) isPostingSingle;

- (id) initWithID:(NSNumber*)postingID
      DisplayName:(NSString*)displayname
            Title:(NSString*)title
      Description:(NSString*)description
        LabelLeft:(NSString*)labelLeft
       LabelRight:(NSString*)labelRight
       ProfileURL:(NSString*)profileURL
          PicLeft:(NSString*)picLeft
         PicRight:(NSString*)picRight
        PicSingle:(NSString*)picSingle
     NumVotesLeft:(int)numVotesLeft
    NumVotesRight:(int)numVotesRight
   NumVotesSingle:(int)numVotesSingle
      NumComments:(int)numComments
        NumShared:(int)numShared
 DateCreatedMilli:(float)dateCreatedMilli
 DateUpdatedMilli:(float)dateUpdatedMilli;



@end
