//
//  Comment.h
//  Piikit
//
//  Created by Jason Kim on 13. 9. 2..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject{
    NSString *mComment;
    NSNumber *mCommentId;
    
    float mTimeCreatedFloat;
    NSString *mTimeDiff;
    
    NSNumber *mNumLikes;
    NSNumber *mNumDislikes;
    
    NSString *mUsername;
    NSString *mProfileURLString;
}

@property (nonatomic, retain) NSString *mComment;
@property (nonatomic, retain) NSNumber *mCommentId;

@property (nonatomic, assign) float mTimeCreatedFloat;
@property (nonatomic, retain) NSString *mTimeDiff;

@property (nonatomic, retain) NSNumber *mNumLikes;
@property (nonatomic, retain) NSNumber *mNumDisikes;

@property (nonatomic, retain) NSString *mUsername;
@property (nonatomic, retain) NSString *mProfileURLString;

@end
