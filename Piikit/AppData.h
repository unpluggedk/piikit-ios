//
//  AppData.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface AppData : NSObject{
    
    
    
}

@property (nonatomic, assign) bool isLoggedIn;
@property (nonatomic, assign) CGFloat screenHeight;
@property (nonatomic, retain) UIColor *themeColor;
+(id)sharedAppData;

-(BOOL)isIos7;
@end
