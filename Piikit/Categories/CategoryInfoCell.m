//
//  CategoryInfoCell.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "CategoryInfoCell.h"

@implementation CategoryInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.textLabel.font = [UIFont fontWithName:@"AudimatMonoLight" size:19.f];
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.backgroundColor = [UIColor clearColor];

        self.selectionStyle = UITableViewCellSelectionStyleGray;

        self.imageView.image = [UIImage imageNamed:@"category_not_selected.png"];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(20, 0, 200, 29);
    self.imageView.frame = CGRectMake(20, 8, 13, 13);
}


- (void)selectCell{
    self.imageView.image = [UIImage imageNamed:@"checked-box.png"];
}
- (void)deselectCell{
    self.imageView.image = [UIImage imageNamed:@"unchecked-box.png"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
