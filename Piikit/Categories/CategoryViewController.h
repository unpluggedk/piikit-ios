//
//  CategoryViewController.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, CategoryStatus){
    NoneExpanded,
    MaleExpanded,
    FemaleExpanded,
    AllExpanded
};

typedef NS_ENUM(NSInteger, Gender){
    Male,
    Female
    
    
};

@protocol CategoryViewControllerDelegate <NSObject>
- (void) loadCategory:(NSNumber*)categoryId name:(NSString*)str;
@end


@interface CategoryViewController : UITableViewController{
    NSArray *gender_;
    NSArray *mensCategories_;
    NSArray *womensCategories_;
    
    BOOL categories_selection[12];
    BOOL gender_selection[3];
  
    CategoryStatus currentStatus;
    
    NSMutableArray *dataSource_;
    NSMutableDictionary *mensCatId;
    NSMutableDictionary *womensCatId;
    
    BOOL loadFromPost;
}

- (void) loadFromPost;
@property (assign) id<CategoryViewControllerDelegate> categoryViewControllerDelegate;


@end
