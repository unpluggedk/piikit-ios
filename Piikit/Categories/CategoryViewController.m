//
//  CategoryViewController.m
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import "CategoryViewController.h"
#import <CoreText/CoreText.h>
#import "CategoryHeaderCell.h"
#import "CategoryInfoCell.h"

#define NOTINITIALIZED 0
#define SELECTED 1
#define NOTSELECTED 2

@interface CategoryViewController ()

@end

@implementation CategoryViewController

@synthesize categoryViewControllerDelegate;



int categoryStart;
int genderStart;

NSArray *categories;

- (void) loadFromPost{
    [dataSource_ removeObjectAtIndex:0];
    
    [dataSource_ addObject:@""];
    [dataSource_ addObject:@""];
    [dataSource_ addObject:@"BACK"];

    mensCategories_ = [[NSArray alloc] initWithObjects:
                       @"   \u2022ACCESSORIES",
                       @"   \u2022BAGS",
                       @"   \u2022BEAUTY & MAKE UP",
                       @"   \u2022CELEBRITIES",
                       @"   \u2022SUITS",
                       @"   \u2022JACKETS",
                       @"   \u2022PANTS & DENIMS",
                       @"   \u2022SHOES",
                       @"   \u2022STYLE CHECK",
                       @"   \u2022TOPS & SHIRTS",
                       @"   \u2022OTHERS", nil];
    
    womensCategories_ = [[NSArray alloc] initWithObjects:
                         @"   \u2022ACCESSORIES",
                         @"   \u2022BAGS",
                         @"   \u2022BEAUTY & MAKE UP",
                         @"   \u2022CELEBRITIES",
                         @"   \u2022DRESSES",
                         @"   \u2022JACKETS",
                         @"   \u2022PANTS & DENIMS",
                         @"   \u2022SHOES",
                         @"   \u2022SKIRTS",
                         @"   \u2022STYLE CHECK",
                         @"   \u2022TOPS & SHIRTS",
                         @"   \u2022OTHERS", nil];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        gender_ = [[NSArray alloc] initWithObjects:@"ALL", @"WOMEN", @"MEN", @"OTHERS", nil];
//        categories_ = [[NSArray alloc] initWithObjects:@"ACCESSORIES", @"BAGS", @"BEAUTY & MAKE UP", @"CELEBRITIES", @"DRESSES", @"JACKETS",
  //                     @"PANTS & DENIMS", @"SHOES", @"SKIRTS", @"STYLE CHECK", @"TOPS & SHIRTS", @"OTHERS", nil];

        mensCategories_ = [[NSArray alloc] initWithObjects:
                            @" -SEE ALL",
                            @"   \u2022ACCESSORIES",
                            @"   \u2022BAGS",
                            @"   \u2022BEAUTY & MAKE UP",
                            @"   \u2022CELEBRITIES",
                            @"   \u2022SUITS",
                            @"   \u2022JACKETS",
                            @"   \u2022PANTS & DENIMS",
                            @"   \u2022SHOES",
                            @"   \u2022STYLE CHECK",
                            @"   \u2022TOPS & SHIRTS",
                            @"   \u2022OTHERS", nil];
        
        womensCategories_ = [[NSArray alloc] initWithObjects:
                           @" -SEE ALL",
                           @"   \u2022ACCESSORIES",
                           @"   \u2022BAGS",
                           @"   \u2022BEAUTY & MAKE UP",
                           @"   \u2022CELEBRITIES",
                           @"   \u2022DRESSES",
                           @"   \u2022JACKETS",
                           @"   \u2022PANTS & DENIMS",
                           @"   \u2022SHOES",
                           @"   \u2022SKIRTS",
                           @"   \u2022STYLE CHECK",
                           @"   \u2022TOPS & SHIRTS",
                           @"   \u2022OTHERS", nil];
        
        
        categoryStart = 2;
        
        dataSource_ = [[NSMutableArray alloc] initWithArray:gender_];
        currentStatus = NoneExpanded;
        
        [self createCategoryDict];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
   // self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.rowHeight = 32;
    self.tableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2+[dataSource_ count];
}

- (NSAttributedString *)getHeaderString:(NSString *)headerStr{
    NSMutableAttributedString *out = [[NSMutableAttributedString alloc] initWithString:headerStr];
    
    [out addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AudimatMono" size:20.f] range:NSMakeRange(0, headerStr.length)];
    [out addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, headerStr.length)];
    [out addAttribute:(NSString*)kCTKernAttributeName value:[NSNumber numberWithFloat:2.f] range:NSMakeRange(0, headerStr.length)];

    return out;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static const id identifiers[3] = {@"empty-cell", @"header-cell", @"info-cell"};
    static const int EMPTYCELL = 0;
    static const int HEADERCELL = 1;
    static const int INFOCELL = 2;
    
    int cellTypeSelector;
    
    if(indexPath.row == 1)
        cellTypeSelector = HEADERCELL;
    else if(indexPath.row == 0 || indexPath.row > (2 + [dataSource_ count]))
        cellTypeSelector = EMPTYCELL;
    else
        cellTypeSelector = INFOCELL;
    
    NSString *identifier = identifiers[cellTypeSelector];
    
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil){
          switch (cellTypeSelector) {
            case HEADERCELL:
                  cell = [[CategoryHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                  break;
            case INFOCELL:
                  cell = [[CategoryInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                  break;
            default:
                  cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                   break;
        }
    }
    
    if(cellTypeSelector == EMPTYCELL){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if(indexPath.row == 1)
        cell.textLabel.attributedText = [self getHeaderString:@"CATEGORIES"];
    
    else{
        cell.textLabel.text = [dataSource_ objectAtIndex:indexPath.row -2];

    }
    
    return cell;


}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1; // it was 0 before the fix
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0; // it was 1 before the fix
}
/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
*/
 
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int selectedRow = indexPath.row;
    int selectedIndex = indexPath.row - 2;
    
    if(selectedIndex < 0)
        return;
    
    if([[dataSource_ objectAtIndex:selectedIndex] isEqualToString:@"WOMEN"]){
        if(currentStatus == NoneExpanded || currentStatus == MaleExpanded){
            NSRange range = NSMakeRange(selectedIndex + 1, [womensCategories_ count]);
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
        
            [dataSource_ insertObjects:womensCategories_ atIndexes:indexSet];

            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            for(int i=0; i<[womensCategories_ count]; i++){
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:(i+selectedRow+1) inSection:0];
                [indexPaths addObject:indexpath];
            }
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
            
            if(currentStatus == NoneExpanded)
                currentStatus = FemaleExpanded;
            else if(currentStatus == MaleExpanded)
                currentStatus = AllExpanded;
        }
        
        else{
            [dataSource_ removeObjectsInRange:NSMakeRange(selectedIndex+1, [womensCategories_ count])];
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            for(int i=0; i<[womensCategories_ count]; i++){
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:(i+selectedRow+1) inSection:0];
                [indexPaths addObject:indexpath];
            }
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
            
            if(currentStatus == FemaleExpanded)
                currentStatus = NoneExpanded;
            else if(currentStatus == AllExpanded)
                currentStatus = MaleExpanded;
            
        }
    }
    
    else if([[dataSource_ objectAtIndex:selectedIndex] isEqualToString:@"MEN"]){
        if(currentStatus == NoneExpanded || currentStatus == FemaleExpanded){
            NSRange range = NSMakeRange(selectedIndex + 1, [mensCategories_ count]);
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
            
            [dataSource_ insertObjects:mensCategories_ atIndexes:indexSet];
            
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            for(int i=0; i<[mensCategories_ count]; i++){
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:(i+selectedRow+1) inSection:0];
                [indexPaths addObject:indexpath];
            }
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
            
            if(currentStatus == NoneExpanded)
                currentStatus = MaleExpanded;
            else if(currentStatus == FemaleExpanded)
                currentStatus = AllExpanded;
        }
        
        else{
            [dataSource_ removeObjectsInRange:NSMakeRange(selectedIndex+1, [mensCategories_ count])];
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            for(int i=0; i<[mensCategories_ count]; i++){
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:(i+selectedRow+1) inSection:0];
                [indexPaths addObject:indexpath];
            }
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
            
            if(currentStatus == MaleExpanded)
                currentStatus = NoneExpanded;
            else if(currentStatus == AllExpanded)
                currentStatus = FemaleExpanded;
            
        }
    }

    else{
       // NSLog(@"catagoryViewController : clicked - %@", [dataSource_ objectAtIndex:selectedIndex]);
        
        NSNumber *catId = NULL;
        if(currentStatus == FemaleExpanded){
            catId = [self getCategoryId:[dataSource_ objectAtIndex:selectedIndex] gender:Female];
            
        }
        else if(currentStatus == MaleExpanded){
            catId = [self getCategoryId:[dataSource_ objectAtIndex:selectedIndex] gender:Male];
        }
        else{ // All Expanded
            if(selectedIndex < [womensCategories_ count])
                catId = [self getCategoryId:[dataSource_ objectAtIndex:selectedIndex] gender:Female];
            else
                catId = [self getCategoryId:[dataSource_ objectAtIndex:selectedIndex] gender:Male];
        }

        NSString *strVal = [self getCategoryName:catId];
        if([strVal isEqualToString:@""])
            strVal = [dataSource_ objectAtIndex:selectedIndex];
        
        if([[dataSource_ objectAtIndex:selectedIndex] isEqualToString:@""]){
            NSLog(@"category view controller - empty cell clicked");
            return;
        }

        [categoryViewControllerDelegate loadCategory:catId name:strVal];
        //NSNumber *catId = [self getCategoryId:[dataSource_ objectAtIndex:selectedIndex]];
        
    }
    
    
}


- (NSString*) getCategoryName:(NSNumber*)categoryId{
    if(categoryId == NULL)
        return @"";
    
    int catId = [categoryId intValue];
    if(catId == -2)
        return @"Men's All";
    
    else if(catId == 6)
        return @"Men's Accesories";
    else if(catId == 17)
        return @"Men's Bags";
    else if(catId == 18)
        return @"Men's Beauty & Make up";
    else if(catId == 19)
        return @"Men Celebrities";
    else if(catId == 16)
        return @"Men's Suits";
    else if(catId == 4)
        return @"Men's Jackets";
    else if(catId == 3)
        return @"Men's Pants & Denims";
    else if(catId == 5)
        return @"Men's Shoes";
    else if(catId == 15)
        return @"Men's Style Check";
    else if(catId == 1)
        return @"Men's Tops & Shirts";
    else if(catId == 7)
        return @"Men's Others";
    
    else if(catId == -1)
        return @"Women's All";
    else if(catId == 14)
        return @"Women's Accessories";
    else if(catId == 21)
        return @"Women's Bags";
    else if(catId == 23)
        return @"Women's Beauty & Make Up";
    else if(catId == 24)
        return @"Women Celebrities";
    else if(catId == 8)
        return @"Women's Dresses";
    else if(catId == 11)
        return @"Women's Jackets";
    else if(catId == 13)
        return @"Women's Pants & Denims";
    else if(catId == 22)
        return @"Women's Shoes";
    else if(catId == 12)
        return @"Women's Skirts";
    else if(catId == 20)
        return @"Women's Style Check";
    else if(catId == 9)
        return @"Women's Tops & Shirts";
    else if(catId == 25)
        return @"Women's Others";
    
    else
        return @"";
}



- (void) createCategoryDict{
    mensCatId = [[NSMutableDictionary alloc] init];
    womensCatId = [[NSMutableDictionary alloc] init];
    
    [mensCatId setObject:[NSNumber numberWithInt:-2] forKey:@" -SEE ALL"];
    [mensCatId setObject:[NSNumber numberWithInt:6] forKey:@"   \u2022ACCESSORIES"];
    [mensCatId setObject:[NSNumber numberWithInt:17] forKey:@"   \u2022BAGS"];
    [mensCatId setObject:[NSNumber numberWithInt:18] forKey:@"   \u2022BEAUTY & MAKE UP"];
    [mensCatId setObject:[NSNumber numberWithInt:19] forKey:@"   \u2022CELEBRITIES"];
    [mensCatId setObject:[NSNumber numberWithInt:16] forKey:@"   \u2022SUITS"];
    [mensCatId setObject:[NSNumber numberWithInt:4] forKey:@"   \u2022JACKETS"];
    [mensCatId setObject:[NSNumber numberWithInt:3] forKey:@"   \u2022PANTS & DENIMS"];
    [mensCatId setObject:[NSNumber numberWithInt:5] forKey:@"   \u2022SHOES"];
    [mensCatId setObject:[NSNumber numberWithInt:15] forKey:@"   \u2022STYLE CHECK"];
    [mensCatId setObject:[NSNumber numberWithInt:1] forKey:@"   \u2022TOPS & SHIRTS"];
    [mensCatId setObject:[NSNumber numberWithInt:7] forKey:@"   \u2022OTHERS"];
    
    [womensCatId setObject:[NSNumber numberWithInt:-1] forKey:@" -SEE ALL"];
    [womensCatId setObject:[NSNumber numberWithInt:14] forKey:@"   \u2022ACCESSORIES"];
    [womensCatId setObject:[NSNumber numberWithInt:21] forKey:@"   \u2022BAGS"];
    [womensCatId setObject:[NSNumber numberWithInt:23] forKey:@"   \u2022BEAUTY & MAKE UP"];
    [womensCatId setObject:[NSNumber numberWithInt:24] forKey:@"   \u2022CELEBRITIES"];
    [womensCatId setObject:[NSNumber numberWithInt:8] forKey:@"   \u2022DRESSES"];
    [womensCatId setObject:[NSNumber numberWithInt:11] forKey:@"   \u2022JACKETS"];
    [womensCatId setObject:[NSNumber numberWithInt:13] forKey:@"   \u2022PANTS & DENIMS"];
    [womensCatId setObject:[NSNumber numberWithInt:22] forKey:@"   \u2022SHOES"];
    [womensCatId setObject:[NSNumber numberWithInt:12] forKey:@"   \u2022SKIRTS"];
    [womensCatId setObject:[NSNumber numberWithInt:20] forKey:@"   \u2022STYLE CHECK"];
    [womensCatId setObject:[NSNumber numberWithInt:9] forKey:@"   \u2022TOPS & SHIRTS"];
    [womensCatId setObject:[NSNumber numberWithInt:25] forKey:@"   \u2022OTHERS"];
    
}



- (NSNumber*) getCategoryId:(NSString*)categoryName gender:(Gender)sex{

    if([categoryName isEqualToString:@"OTHERS"])
        return [NSNumber numberWithInt:26];
    
    return (sex==Male)?[mensCatId objectForKey:categoryName]:[womensCatId objectForKey:categoryName];

}

@end
