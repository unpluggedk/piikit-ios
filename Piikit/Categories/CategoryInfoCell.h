//
//  CategoryInfoCell.h
//  Piikit
//
//  Created by Jason Kim on 13. 5. 1..
//  Copyright (c) 2013년 yogurtapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryInfoCell : UITableViewCell

- (void)selectCell;
- (void)deselectCell;
@end
